.TH GEMINICLI 1 "October 2024" "geminicli" "User Commands"
.SH NAME
geminicli \- Terminal Chat Completion client for Google's Gemini AI models
.SH SYNOPSIS
.B geminicli
[\fIOPTIONS\fR] "\fIPROMPT\fR"
.SH DESCRIPTION
\fBgeminicli\fR is a command-line utility for interacting with Google's Gemini AI models. It allows users to send prompts, receive completions, generate embeddings, and perform various multimodal AI-related tasks.

.B The order of arguments matters: 
OPTIONS should be specified first, followed by the PROMPT. The PROMPT must always be enclosed in double quotes.
.SH OPTIONS
.SS Global Options:
.TP
.B \-\-defaults
Prints the program default values and exit.
.TP
.B \-l, \-\-list\-models
List names of all available models and exit.
.TP
.B \-m, \-\-model=\fIMODEL\fR
Select a specific model. Use '\-\-list\-models' to see available models.
.TP
.B \-\-model\-info
Return information about a given model.
.br
Can be used in conjunction with the '\-\-model' option.
.TP
.B \-p, \-\-preview
Previews the request payload that would be sent to the Gemini API and exits.
.br
This is useful for debugging and understanding how the different options affect the request.
.TP
.B \-\-response\-raw
Prints the full response body from the Gemini API in its raw format.
.TP
.B \-\-response\-json
Prints the full response body from the Gemini API formatted as JSON.
.TP
.B \-\-help
Prints help information.
.TP
.B \-\-version
Output version information and exit.

.SS Network Options:
.TP
.B \-\-retries=\fIRETRIES\fR
Specifies the number of connection retries to attempt if a network timeout occurs.
.br
Use the '\-\-retries-wait' option to insert a pause between retries.
.TP
.B \-\-retries\-wait=\fISECONDS\fR
In conjunction with the '\-\-retries RETRIES' option, inserts a pause of SECONDS seconds between retries.
.TP
.B \-\-timeout=\fISECONDS\fR
Sets the network connection timeout for a complete request/response session, in seconds. A value of 0 means no timeout.
.TP
.B \-\-timeout\-chunk=\fISECONDS\fR
Sets the network connection timeout for every streamed chunk of response in stream mode, in seconds.

.SS API Authentication:
.TP
.B \-\-env=\fINAME\fR
Specifies an alternative environment variable name to use instead of the default 'GOOGLE_API_KEY'. This variable should contain the API key for your Google Cloud Platform project.
.TP
.B \-f, \-\-file=\fIAPIKEY_FILE\fR
DEPRECATED - use option '-k, --key-file=FILENAME' instead.
.TP
.B \-k, \-\-key\-file=\fIFILENAME\fR
Specifies the path to a file containing the Google API key.
The file should contain a line in the format
.B GOOGLE_API_KEY=your_apikey.
.br
Obtaining and using a Google API key: <https://gitlab.com/ai-gimlab/geminicli#how-to-get-and-use-google-api-key>

.SS Chat/Completion/Media API Options:
.TP
.B \-a, \-\-assistant="\fIPROMPT\fR"
Specifies a message from the 'assistant' (model) with the given PROMPT.
.br
This option must be used with the '\-\-previous-prompt' option to simulate a single round of chat.
.P
.RS
It can also be used together with the '\-\-function-response' option to provide function call responses as JSON objects.
.br
For example:
.B --assistant '{JSON_FUNC_CALL_RESP_0},,{JSON_FUNC_CALL_RESP_1},,{JSON_FUNC_CALL_RESP_N}'
.br
Use the '\-\-function-examples' option to view how to compose a function call JSON object.
.RE
.TP
.B \-c, \-\-count=\fISELECT\fR
Prints the number of tokens used. SELECT can be one of the following:
.RS
.TP
.B in
Number of tokens in the user's prompt.
.TP
.B out
Number of tokens in the AI's response.
.TP
.B total
Total number of tokens used (in + out).
.TP
.B words
Word count of the AI's response.
.P
SELECT can also be any comma-separated combination of the above, without any spaces or trailing commas. For example:
.TP
.B in,out
.TP
.B in,out,total,words
.RE
.TP
.B \-\-cache=\fINAME\fR
Execute completion task using cached content identified by NAME.
.br
NAME is extracted as the 'name' field from the '--cache-create' response (e.g., 'cachedContents/abcde12345fg'). The 'cachedContents/' prefix is optional.
.RE
.TP
.B \-\-cfg=\fITOML_CFG_FILE\fR
Loads a basic configuration from the specified TOML configuration file.
.br
.B These configurations take precedence over the program's defaults, but command line options take precedence over both.
.TP
.B \-\-cfg\-save=\fITOML_CFG_FILE\fR
Save basic configuration to a TOML file using task session values.
.TP
.B \-\-citations
Prints the source attribution of the generated content,
.B if any.
.TP
.B \-\-code\-exec
Instructs the model to generate and run Python code from the provided PROMPT.
.br
The model will execute the Python code until it arrives at a final output.
.br
More information: <https://ai.google.dev/gemini-api/docs/code-execution> for more information on code execution with Gemini.
.TP
.B \-\-csv=\fICSV_FILE\fR
Exports the request/response data to the specified CSV file.
.br
If the file does not exist, the program will ask for permission to create it. If the file exists, the data will be appended.
.br
Not available in stream mode.
.TP
.B \-\-finish\-reason
Prints the reason why the model stopped generating tokens.
.TP
.B \-\-format=\fIFORMAT\fR
Specifies the format/mimetype for the model's output (Gemini 1.5 Flash/Pro and newer). FORMAT can be one of the following:
.RS
.TP
.B text/plain
or simply
.B text
(default).
.TP
.B application/json
or simply
.B json
.RE
.TP
.B \-\-format\-examples
Prints an example of JSON SCHEMA format requests and exits.
.TP
.B \-\-format\-jschema=\fI{SCHEMA}\fR
Specifies a JSON formatted schema for JSON formatted responses (Gemini 1.5 Pro and newer).
.br
Use the '\-\-format-examples' option to view how to compose a SCHEMA object.
.br
The '\-\-format=FORMAT' option is automatically set to 'application/json'.
.br
Refer to <https://ai.google.dev/api/rest/v1beta/cachedContents#schema> for information on the SCHEMA format
.br
Refer to <https://ai.google.dev/gemini-api/docs/api-overview#json> for supported models.
.TP
.B \-\-fp=\fIVALUE\fR
Frequency penalty: Sets the frequency penalty. \fIVALUE\fR must be in the range [-2.0, 2.0). Positive values decrease the model's likelihood of repeating the same content.
.TP
.B \-\-function='\fI{JSON}\fR'
Provides a JSON object containing a function definition. The JSON object must be enclosed in single quotes.
.br
Stream mode is available only if used with the '\-\-function-response={JSON}' option.
.br
Multiple function JSON object definitions can be provided, separated by double commas.
.br
For example:
.B '{JSON_FUNC_1},,{JSON_FUNC_2},,{JSON_FUNC_N}'
.br
Use the '\-\-function-examples' option to view how to compose a function definition JSON object.
.br
Refer to <https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#functions> for examples of using functions with
.B geminicli.
.TP
.B \-\-function\-allowed=\fINAMES\fR
Forces the model to call a function from the provided list of function NAMES. NAMES is a comma-separated list of function declaration names, enclosed in single or double quotes.
.br
For example:
.B "FNAME_A,FNAME_B,FNAME_X"
.br
Note that the function call MODE is automatically set to ANY.
.TP
.B \-\-function\-call="\fIMODE\fR"
Determines how the model responds to function calls. MODE can be one of the following:
.RS
.TP
.B AUTO
The model chooses whether to call a function.
.TP
.B NONE
The model does not call any functions.
.TP
.B ANY
The model is forced to call one or more functions.
.RE
.TP
.B \-\-function\-examples
Prints examples of function JSON objects.
.TP
.B \-\-function\-response=\fI{JSON}\fR
Provides a JSON object containing results from the application's actual function. This option must be used with the following options:
.RS
.TP
.BI \-\-assistant= {FUNC_CALL_JSON_RESP_0,,FUNC_CALL_JSON_RESP_1,,FUNC_CALL_JSON_RESP_N}
.TP
.BI \-\-previouse\-prompt= PROMPT
.TP
.BI \-\-function= {FUNC_DEF_0,,FUNC_DEF_1,,FUNC_DEF_N}
.RE
.P
.RS
Use the '\-\-function-examples' option to view how to compose a function response JSON object.
.RE
.P
.RS
Refer to <https://ai.google.dev/gemini-api/docs/function-calling#expandable-7> for more information on function calling with Gemini.
.RE
.TP
.B \-\-jrequest\-run=\fIJSON_FILE\fR
Runs a request payload from the specified JSON file. This option can only be used with the '\-\-model=MODEL' and '\-\-stream' options. It takes precedence over the default configurations and those defined in the TOML configuration file, except for the 'model' and 'stream' mode.
.TP
.B \-\-jrequest\-save=\fIJSON_FILE\fR
Saves the request payload to the specified JSON file using the current task session values.
.TP
.B \-\-list\-presets
Lists all available predefined tasks and exits.
.TP
.B \-\-logprobs
Return log probabilities of the chosen tokens in the response, formatted as a table. This is equivalent to '\-\-top\-logprobs=0'.
.br
When used with '\-\-response\-json' or '\-\-response\-raw', the log probabilities are included in the JSON formatted response.
.TP
.B \-\-media=\fIFILENAME\fR
Instructs the model to answer questions about the specified file. FILENAME can be any text, image, audio, or video file. It can be a local file and/or a FileAPI URI.
.br
For multiple files, enclose comma-separated filenames/URIs in double quotes, without any spaces or trailing commas.
.br
For example:
.B "img/img1.jpeg,https://generativelanguage.googleapis.com/v1beta/files/abc-123,audio.mp3,vid.mp4"
.P
.RS
Supported file formats: <https://ai.google.dev/gemini-api/docs/prompting_with_media?lang=go#supported_file_formats>
.RE
.P
.RS
.B Note:
for payloads greater than 20 MB, the media files must be first uploaded using the File API.
.RE
.P
.RS
Refer to <https://ai.google.dev/gemini-api/docs/vision?lang=node#upload-image> for more information.
.RE
.TP
.B \-\-model\-used
Which model was used at endpoint side.
.TP
.B \-\-no\-console
Suppresses console output. Must be used with the '--output=FILENAME' option. Stream mode is not supported.
.TP
.B \-\-no\-thinking
Disables the display of the "thinking..." message that is normally shown when a model that requires a significant processing time (a "thinking" model) is being used. This option is useful for scripting or other automated tasks where the message is not needed or desired.
.TP
.B \-o, \-\-output="\fIFILENAME\fR"
Save response to FILENAME
.TP
.B \-\-output\-full
Save full chat system/user/assistant to FILENAME (option '--output=FILENAME'), adding headings. For example:
.P
.RS
.B **SYSTEM**\fR: 'you are helpful assistant'
.RE
.RS
.B **USER**\fR: 'Write a poem'
.RE
.RS
.B **ASSISTANT**\fR: 'No more, I quit!'
.RE
.P
.RS
Must be used in conjunction with '--output=FILENAME' option.
.RE
.TP
.B \-\-pdf=\fIPDF_FILENAME\fR
Extracts text from the specified PDF file. When asking questions about the extracted text in the user prompt, focus on the context of the text itself. For example:
.RS
.TP
Incorrect:
.B geminicli --pdf document.pdf "From the following document {QUESTION}"
.TP
Correct:
.B geminicli --pdf document.pdf "How do I completely factory reset my phone?"
.RE
.P
.RS
If the '\-\-pdf-pages' option is not provided, the entire document's text will be extracted.
.RE
.TP
.B \-\-pdf\-pages=\fIPAGES\fR
Specifies the 'physical pages' of the PDF document from which to extract text. PAGES can be any of the following:
.RS
.TP
.B [page_number]
.TP
.B [page_number,page_number]
.TP
.B [page_number-page_number]
.RE
.P
.RS
Examples:
.P
.B 5
(only page 5)
.TP
.B 1,5,8
(pages 1, 5, and 8)
.TP
.B 14-18
(range of pages from page 14 to page 18)
.TP
.B 1,5,8,14-18,36,50-100
(pages 1, 5, 8, 36, and ranges from page 14 to page 18 and from page 50 to page 100)
.RE
.P
.RS
This option must be used with the '\-\-pdf=PDF_FILENAME' option.
.RE
.TP
.B \-\-pdf\-pagenumber
Prints a footer with 'physical page numbers'. This option must be used in conjunction with the '\-\-pdf-test' option.
.TP
.B \-\-pdf\-test
Prints the text extracted from the PDF file and exits. This is useful for testing the text extraction process.
.TP
.B \-\-pdf\-system
Prints default system message for PDF tasks.
.TP
.B \-\-pp=\fIVALUE\fR
Presence penalty: Sets the presence penalty. \fIVALUE\fR must be in the range [-2.0, 2.0). Positive values increase the model's likelihood of generating more diverse content.
.TP
.B \-\-preset=\fIPRESET_NAME\fR
Specifies a predefined task, such as summarization, sentiment analysis, etc. Use the '\-\-list-presets' option to list all available PRESET_NAMEs and their purpose. Note that preset configurations take precedence over TOML configuration files.
.TP
.B \-\-preset\-system
Prints the predefined system message for the specified '\-\-preset PRESET_NAME' and exits.
.TP
.B \-\-previous\-prompt=\fIPROMPT\fR
In conjunction with the '\-\-assistant' option, simulates a single round of chat by providing the previous prompt in the conversation.
.TP
.B \-r, \-\-response\-tokens=\fITOKENS\fR
Sets the maximum number of response tokens.
.br
Refer to <https://ai.google.dev/models/gemini> for information on model context sizes.
.TP
.B \-\-safety\-all=\fITHRESHOLD\fR
Sets the same safety threshold for all categories. This option can be used together with the '\-\-safety-{CATEGORY}' options, but the '\-\-safety-{CATEGORY}' options will override the configuration for the given category. THRESHOLD can be one of the following:
.RS
.TP
.B BLOCK_NONE
.TP
.B BLOCK_LOW_AND_ABOVE
.TP
.B BLOCK_MEDIUM_AND_ABOVE\fR (default)
.TP
.B BLOCK_ONLY_HIGH
.RE
.P
.RS
More information: <https://cloud.google.com/vertex-ai/docs/generative-ai/multimodal/configure-safety-attributes>
.RE
.TP
.B \-\-safety\-dcont=\fITHRESHOLD\fR
Sets the safety threshold for the HARM_CATEGORY_DANGEROUS_CONTENT category. Refer to the '\-\-safety-all' option for possible THRESHOLD values.
.TP
.B \-\-safety\-harass=\fITHRESHOLD\fR
Sets the safety threshold for the HARM_CATEGORY_HARASSMENT category. Refer to the '\-\-safety-all' option for possible THRESHOLD values.
.TP
.B \-\-safety\-hate=\fITHRESHOLD\fR
Sets the safety threshold for the HARM_CATEGORY_HATE_SPEECH category. Refer to the '\-\-safety-all' option for possible THRESHOLD values.
.TP
.B \-\-safety\-sex=\fITHRESHOLD\fR
Sets the safety threshold for the HARM_CATEGORY_SEXUALLY_EXPLICIT category. Refer to the '\-\-safety-all' option for possible THRESHOLD values.
.TP
.B \-\-stop="\fISTOP,...\fR"
Specifies a comma-separated list of stop sequences, up to 5 sequences, without a trailing comma, enclosed in single or double quotes.
.br
For example: \fB"STOP1,STOP2,..."\fR
.RE
.TP
.B \-\-stream
Start printing completion before it's fully finished.
.TP
.B \-s, \-\-system="\fIPROMPT\fR"
Set system message (Gemini 1.5 and newer).
.TP
.B \-t, \-\-temperature=\fIVALUE\fR
Controls the randomness of the generated text. VALUE ranges from 0.0 to 2.0 (the maximum value is model dependent).
.TP
.B \-\-test=\fIURL\fR
Inspect requests using an owned web debug server. URL must be \fIabsolute\fR. Does not send sensitive data.
.TP
.B \-\-test\-httpbin
Inspect requests using \fIhttpbin.org\fR as web debug server. Does not send sensitive data.
.TP
.B \-\-top\-k=\fIVALUE\fR
Sets the top_k parameter. VALUE ranges from 1 to the maximum value supported by the model.
.TP
.B \-\-top\-logprobs=\fINUMBER\fR
Return log probabilities for the top \fINUMBER\fR candidate tokens. \fINUMBER\fR must be an integer between 0 and 5, inclusive.
.br
'\-\-top\-logprobs=0' is equivalent to '\-\-logprobs'.
.br
When used with '\-\-response\-json' or '\-\-response\-raw', the top log probabilities are included in the JSON formatted response.
.TP
.B \-\-top\-p=\fIVALUE\fR
Sets the top_p parameter. VALUE ranges from 0.0 to 1.0.
.TP
.B \-\-web=\fIURL\fR
Specifies a web page URL. Note that dynamic web pages are not supported. The URL must always be enclosed in single or double quotes.
.br
When asking questions about the web page content in the user prompt, focus on the context of the page itself. For example:
.RS
.TP
Incorrect:
.B geminicli --web "https://www.example.com" "From the following web page {QUESTION}"
.TP
Correct:
.B geminicli --web "https://www.example.com" "Extract a list of cat names"
.P
When used in conjunction with the '\-\-preset' option, the URL content becomes the user prompt.
.TP
For example:
.B geminicli --preset ner --web "https://www.example.com"
.br
execute the 'ner' preset using content from the "https://www.example.com" web page
.RE
.TP
.B \-\-web\-select=\fISELECTOR\fR
Selects a section of the web page and strips HTML tags from the selected content. SELECTOR can be any HTML element, such as 'h2', 'p', 'body', 'article', etc. It can also be any class or ID attribute (e.g., '.myClass', '#myID'). Lastly, SELECTOR can be the 'NONE' keyword, which disables HTML tag stripping. Note that using 'NONE' can consume a large number of tokens. SELECTOR must always be enclosed in single or double quotes. The default selectors are 'main' (if it exists) or 'body'.
.TP
.B \-\-web\-test
Prints the text extracted from the web page and exits. This is useful for finding the right HTML selector (see the '\-\-web-select' option). Using the correct selector can reduce the number of tokens used.
.TP
.B \-\-web\-system
Prints the default system message for web tasks.

.SS Embeddings API Options:
.TP
.B \-e, \-\-embed
Returns the embedding of the provided PROMPT. The PROMPT must always be enclosed in double quotes. The output is a CSV table with the columns 'id, text, embedding', which is appended to the file 'data.csv' or a new file if it doesn't exist. To save to a different file, use the '\-\-csv FILENAME' option.
.P
.RS
When used in conjunction with the '\-\-response-json' or '\-\-response-raw' options, the response is output to stdout as JSON data instead of being saved to a file.
.RE
.P
.RS
Multiple PROMPTs can be sent in batch by separating them with double commas:
.br
.B \t  geminicli --embed "PROMPT_1,,PROMPT_2,,...,,PROMPT_N"
.RE
.TP
.B \-\-embed\-dimensions=\fIVALUE\fR
Changes the embedding vector size to the specified number of dimensions. This option must be used with the '\-\-embed' option.

.SS Cache API Options:
.TP
.B \-\-cache\-create
Creates cached resource. Must be used in conjunction with '--media=FILENAME' option
.TP
.B \-\-cache\-delete=\fINAME\fR
Deletes cached resource identified by NAME, which is the 'name' field from the '--cache-create' response (e.g., "cachedContents/abcde12345fg").
.br
NAME can optionally include the "cachedContents/" prefix.
.TP
.B \-\-cache\-dname=\fIDNAME\fR
Sets a human-readable name for the specified cache. Must be used in conjunction with the '--cache-create' option. Maximum length of 128 Unicode characters.
.TP
.B \-\-cache\-expire=\fITIMESTAMP\fR
Specifies when the cache should be considered expired. The TIMESTAMP must be in RFC3339 UTC "Zulu" format, including nanosecond resolution with up to nine fractional digits (e.g., "2024-08-18T15:01:23Z" or "2024-08-18T15:01:23.045123456Z"). The TIMESTAMP must always be enclosed in "double" quotes. This option must be used in conjunction with the '--cache-create' or '--cache-update' options. Cannot be used with the '--cache-ttl=SECONDS' option.
.TP
.B \-\-cache\-ttl=\fISECONDS\fR
Set the cache duration in SECONDS, allowing up to nine fractional digits (e.g., "3.5" for 3.5 seconds).
.br
This option must be used in conjunction with the '--cache-create' or '--cache-update' options. Cannot be used with the '--cache-expire=TIMESTAMP' option.
.TP
.B \-\-cache\-update=\fINAME\fR
Updates the cached resource NAME, modifying only its expiration. Must be used in conjunction with either '--cache-ttl=SECONDS' or '--cache-expire=TIMESTAMP'.
.br
NAME is the 'name' field from the '--cache-create' response (e.g., "cachedContents/abcde12345fg").
.TP
.B \-\-cache\-info=\fINAME\fR
Retrieve information about the cache specified by NAME.
.br
NAME corresponds to the '"name":' key from the '--cache-create' response. For example: "name": "cachedContents/abcde12345fg".
.br
NAME can be provided either with or without the "cachedContents/" prefix.
.TP
.B \-\-cache\-list
Lists cached contents.
.TP
.B \-\-cache\-listsize=\fISIZE\fR
Number of cached entries to return per page when using the '--cache-list' option. This option must be used in conjunction with '--cache-list'.
.TP
.B \-\-cache\-listtoken=\fITOKEN\fR
Page token obtained from a previous '--cache-list --cache-listsize=SIZE' call, representing the value of the "nextPageToken": key. This token must be used in conjunction with the '--cache-list' option.

.SS File API Options:
.TP
.B \-\-fdelete=\fINAME\fR
Deletes an uploaded file. NAME is the key '"name":' from the file upload/list/info response (e.g., "name": "files/123-abc-vwxyz"). NAME can be specified with or without the "files/" prefix.
.TP
.B \-\-fdname=\fIDNAME\fR
Specifies a human-readable display name for the uploaded file. This option must be used with the '\-\-fupload=FILENAME' option.
.TP
.B \-\-fname=\fINAME\fR
Specifies a resource identifier name for the uploaded file. This option must be used with the '\-\-fupload=FILENAME' option. If not provided, a unique name will be generated.
.TP
.B \-\-fupload=\fIFILENAME\fR
Uploads the specified file using the File API.
.TP
.B \-\-finfo=\fINAME\fR
Retrieves metadata for the specified file. NAME is the key '"name":' from the file upload response (e.g., "name": "files/123-abc-vwxyz"). NAME can be specified with or without the "files/" prefix.
.TP
.B \-\-flist
List uploaded files.
.TP
.B \-\-flist\-size=\fISIZE\fR
Specifies the number of files to return per page when using the '\-\-flist' option. This option must be used with the '\-\-flist' option.
.TP
.B \-\-flist\-token=\fITOKEN\fR
Provides a page token from a previous '\-\-flist \-\-flist-size=SIZE' call. TOKEN is the value of the '"nextPageToken":' key from the previous call. This option must be used with the '\-\-flist' option.

.SH EXAMPLES
.TP
Generate a response to a prompt using the default settings:
geminicli "What is the capital of France?"
.TP
Generate a response using a specific model:
geminicli -m gemini-1.5-pro-latest "What is the capital of France?"
.TP
Generate a response and display the token count:
geminicli -c total "Write a short poem."
.TP
Run a zero-shot classification using preset \fBsentiment\fP:
geminicli --preset sentiment "This is a positive review."
.TP
Use a JSON schema to format the output:
export JSON_SCHEMA='{
  "type": "array",
  "items": {
    "description": "blog post generator",
    "type": "object",
    "properties": {
      "title": {
        "description": "blog post title",
        "type": "string"
      },
      "content": {
        "description": "blog post content",
        "type": "string"
      }
    },
    "required": [
      "title",
      "content"
    ]
  }
.br
}'
.TP
then: geminicli --format-jschema="$JSON_SCHEMA" "Generate a title and content for a blog post about {TOPIC}."
.br
.TP
For more examples: <https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#geminicli---examples>

.SH "EXIT STATUS"
.B geminicli
Exits with a status of 0 on success or > 0 if an error occurs.

.SH ONLINE DOCUMENTATION
For more detailed information, visit: <https://gitlab.com/ai-gimlab/geminicli#geminicli---overview>

.SH AUTHOR
Written by gianluca emaldi.

.SH BUGS
Report bugs to <https://gitlab.com/ai-gimlab/geminicli/issues>

.SH COPYRIGHT
Copyright (C) 2024 gimlab. License AGPL-3.0: GNU Affero General Public License version 3 or later <https://www.gnu.org/licenses/agpl-3.0.html>.
.br
This is free software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law.


.SH "SEE ALSO"
.BR gptcli (1),
.BR mistralcli (1),
.BR eucos (1),
.BR jq (1),
.BR python (1),
.BR bash (1)
