/*  'geminicli' Terminal Chat Completion client for Google's Gemini AI models
    Copyright (C) 2024 gimlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/pborman/getopt/v2"
)

type flagRelated struct {
	excluded map[string][]struct{}
	admitted map[string][]struct{}
	name     string
}

type flagDependencies map[string]rune

var admittedCommon map[string][]struct{} = map[string][]struct{}{
	"p":             nil,
	"preview":       nil,
	"response-raw":  nil,
	"response-json": nil,
	"retries":       nil,
	"retries-wait":  nil,
	"timeout":       nil,
	"f":             nil,
	"file":          nil,
	"k":             nil,
	"key-file":      nil,
	"env":           nil,
	"test":          nil,
	"test-httpbin":  nil,
}

var flagNoPrompt flagDependencies = flagDependencies{
	flagPreviewLong:            flagPreviewShort,
	flagFileUploadLong:         fakeRune,
	flagFileUploadInfoLong:     fakeRune,
	flagFileUploadListLong:     fakeRune,
	flagFileUploadDeleteLong:   fakeRune,
	flagFunctionResponseLong:   fakeRune,
	flagWebTestLong:            fakeRune,
	flagPdfTestLong:            fakeRune,
	flagPayloadRunLong:         fakeRune,
	flagPresetSystemLong:       fakeRune,
	flagWebSystemLong:          fakeRune,
	flagPdfSystemLong:          fakeRune,
	flagCfgSaveLong:            fakeRune,
	flagCacheCreateLong:        fakeRune,
	flagCacheListLong:          fakeRune,
	flagCacheInfoLong:          fakeRune,
	flagCacheDeleteLong:        fakeRune,
	flagCacheUpdateLong:        fakeRune,
	flagTestPayloadLong:        fakeRune,
	flagTestPayloadHttpbinLong: fakeRune,
}

// how many flags are used
const flagsCount int = 92

// flag labels
const (
	fakeRune                              rune   = '#'
	flagTestPayloadShort                  rune   = fakeRune
	flagTestPayloadLong                   string = "test"
	flagTestPayloadHttpbinShort           rune   = fakeRune
	flagTestPayloadHttpbinLong            string = "test-httpbin"
	flagHelpShort                         rune   = fakeRune
	flagHelpLong                          string = "help"
	flagVersionShort                      rune   = fakeRune
	flagVersionLong                       string = "version"
	flagTemperatureShort                  rune   = 't'
	flagTemperatureLong                   string = "temperature"
	flagTopPShort                         rune   = fakeRune
	flagTopPLong                          string = "top-p"
	flagTopKShort                         rune   = fakeRune
	flagTopKLong                          string = "top-k"
	flagFileShort                         rune   = 'f'
	flagFileLong                          string = "file"
	flagKeyFileShort                      rune   = 'k'
	flagKeyFileLong                       string = "key-file"
	flagResTokShort                       rune   = 'r'
	flagResTokLong                        string = "response-tokens"
	flagStreamShort                       rune   = fakeRune
	flagStreamLong                        string = "stream"
	flagCountShort                        rune   = 'c'
	flagCountLong                         string = "count"
	flagAssistantShort                    rune   = 'a'
	flagAssistantLong                     string = "assistant"
	flagSystemShort                       rune   = 's'
	flagSystemLong                        string = "system"
	flagJsonShort                         rune   = fakeRune
	flagJsonLong                          string = "response-json"
	flagStopShort                         rune   = fakeRune
	flagStopLong                          string = "stop"
	flagRawShort                          rune   = fakeRune
	flagRawLong                           string = "response-raw"
	flagPreviewShort                      rune   = 'p'
	flagPreviewLong                       string = "preview"
	flagTimeoutShort                      rune   = fakeRune
	flagTimeoutLong                       string = "timeout"
	flagTimeoutChunckShort                rune   = fakeRune
	flagTimeoutChunkLong                  string = "timeout-chunk"
	flagRetriesshort                      rune   = fakeRune
	flagRetriesLong                       string = "retries"
	flagRetriesWaitShort                  rune   = fakeRune
	flagRetriesWaitLong                   string = "retries-wait"
	flagPreviousPromptShort               rune   = fakeRune
	flagPreviousPromptLong                string = "previous-prompt"
	flagFinishReasonShort                 rune   = fakeRune
	flagFinishReasonLong                  string = "finish-reason"
	flagSafetyAllShort                    rune   = fakeRune
	flagSafetyAllLong                     string = "safety-all"
	flagSafetyDangerousContentShort       rune   = fakeRune
	flagSafetyDangerousContentLong        string = "safety-dcont"
	flagSafetyHateSpeechShort             rune   = fakeRune
	flagSafetyHateSpeechLong              string = "safety-hate"
	flagSafetyHarassmentShort             rune   = fakeRune
	flagSafetyHarassmentLong              string = "safety-harass"
	flagSafetySexuallyExplicitShort       rune   = fakeRune
	flagSafetySexuallyExplicitLong        string = "safety-sex"
	flagCitationsShort                    rune   = fakeRune
	flagCitationsLong                     string = "citations"
	flagListModelsShort                   rune   = 'l'
	flagListModelsLong                    string = "list-models"
	flagModelShort                        rune   = 'm'
	flagModelLong                         string = "model"
	flagModelInfoShort                    rune   = fakeRune
	flagModelInfoLong                     string = "model-info"
	flagMediaShort                        rune   = fakeRune
	flagMediaLong                         string = "media"
	flagFileUploadShort                   rune   = fakeRune
	flagFileUploadLong                    string = "fupload"
	flagFileUploadNameShort               rune   = fakeRune
	flagFileUploadNameLong                string = "fname"
	flagFileUploadDisplayNameShort        rune   = fakeRune
	flagFileUploadDisplayNameLong         string = "fdname"
	flagFileUploadInfoShort               rune   = fakeRune
	flagFileUploadInfoLong                string = "finfo"
	flagFileUploadListShort               rune   = fakeRune
	flagFileUploadListLong                string = "flist"
	flagFileUploadDeleteShort             rune   = fakeRune
	flagFileUploadDeleteLong              string = "fdelete"
	flagFileUploadListSizeShort           rune   = fakeRune
	flagFileUploadListSizeLong            string = "flist-size"
	flagFileUploadListTokenShort          rune   = fakeRune
	flagFileUploadListTokenLong           string = "flist-token"
	flagResponseFormatShort               rune   = fakeRune
	flagResponseFormatLong                string = "format"
	flagResponseFormatSchemaJSONShort     rune   = fakeRune
	flagResponseFormatSchemaJSONLong      string = "format-jschema"
	flagResponseFormatSchemaExamplesShort rune   = fakeRune
	flagResponseFormatSchemaExamplesLong  string = "format-examples"
	flagFunctionShort                     rune   = fakeRune
	flagFunctionLong                      string = "function"
	flagFunctionExamplesShort             rune   = fakeRune
	flagFunctionExamplesLong              string = "function-examples"
	flagFunctionCallShort                 rune   = fakeRune
	flagFunctionCallLong                  string = "function-call"
	flagFunctionAllowedShort              rune   = fakeRune
	flagFunctionAllowedLong               string = "function-allowed"
	flagCodeExecutionShort                rune   = fakeRune
	flagCodeExecutionLong                 string = "code-exec"
	flagFunctionResponseShort             rune   = fakeRune
	flagFunctionResponseLong              string = "function-response"
	flagCsvShort                          rune   = fakeRune
	flagCsvLong                           string = "csv"
	flagEmbedShort                        rune   = 'e'
	flagEmbedLong                         string = "embed"
	flagEmbedDimensionsShort              rune   = fakeRune
	flagEmbedDimensionsLong               string = "embed-dimensions"
	flagWebShort                          rune   = fakeRune
	flagWebLong                           string = "web"
	flagWebSelectShort                    rune   = fakeRune
	flagWebSelectLong                     string = "web-select"
	flagWebTestShort                      rune   = fakeRune
	flagWebTestLong                       string = "web-test"
	flagWebSystemShort                    rune   = fakeRune
	flagWebSystemLong                     string = "web-system"
	flagPdfShort                          rune   = fakeRune
	flagPdfLong                           string = "pdf"
	flagPdfPagesShort                     rune   = fakeRune
	flagPdfPagesLong                      string = "pdf-pages"
	flagPdfTestShort                      rune   = fakeRune
	flagPdfTestLong                       string = "pdf-test"
	flagPdfPageNumberShort                rune   = fakeRune
	flagPdfPageNumberLong                 string = "pdf-pagenumber"
	flagPdfSystemShort                    rune   = fakeRune
	flagPdfSystemLong                     string = "pdf-system"
	flagPayloadRunShort                   rune   = fakeRune
	flagPayloadRunLong                    string = "jrequest-run"
	flagPayloadSaveShort                  rune   = fakeRune
	flagPayloadSaveLong                   string = "jrequest-save"
	flagCfgShort                          rune   = fakeRune
	flagCfgLong                           string = "cfg"
	flagCfgSaveShort                      rune   = fakeRune
	flagCfgSaveLong                       string = "cfg-save"
	flagDefaultsShort                     rune   = fakeRune
	flagDefaultsLong                      string = "defaults"
	flagListPresetsShort                  rune   = fakeRune
	flagListPresetsLong                   string = "list-presets"
	flagPresetShort                       rune   = fakeRune
	flagPresetLong                        string = "preset"
	flagPresetSystemShort                 rune   = fakeRune
	flagPresetSystemLong                  string = "preset-system"
	flagOutputShort                       rune   = 'o'
	flagOutputLong                        string = "output"
	flagOutputFullShort                   rune   = fakeRune
	flagOutputFullLong                    string = "output-full"
	flagNoConsoleShort                    rune   = fakeRune
	flagNoConsoleLong                     string = "no-console"
	flagCacheCreateShort                  rune   = fakeRune
	flagCacheCreateLong                   string = "cache-create"
	flagCacheTtlShort                     rune   = fakeRune
	flagCacheTtlLong                      string = "cache-ttl"
	flagCacheExpireShort                  rune   = fakeRune
	flagCacheExpireLong                   string = "cache-expire"
	flagCacheDisplayNameShort             rune   = fakeRune
	flagCacheDisplayNameLong              string = "cache-dname"
	flagCacheListShort                    rune   = fakeRune
	flagCacheListLong                     string = "cache-list"
	flagCacheInfoShort                    rune   = fakeRune
	flagCacheInfoLong                     string = "cache-info"
	flagCacheListSizeShort                rune   = fakeRune
	flagCacheListSizeLong                 string = "cache-listsize"
	flagCacheListTokenShort               rune   = fakeRune
	flagCacheListTokenLong                string = "cache-listtoken"
	flagCacheDeleteShort                  rune   = fakeRune
	flagCacheDeleteLong                   string = "cache-delete"
	flagCacheUpdateShort                  rune   = fakeRune
	flagCacheUpdateLong                   string = "cache-update"
	flagCacheShort                        rune   = fakeRune
	flagCacheLong                         string = "cache"
	flagEnvShort                          rune   = fakeRune
	flagEnvLong                           string = "env"
	flagModelUsedShort                    rune   = fakeRune
	flagModelUsedLong                     string = "model-used"
	flagLogProbsShort                     rune   = fakeRune
	flagLogProbsLong                      string = "logprobs"
	flagTopLogProbsShort                  rune   = fakeRune
	flagTopLogProbsLong                   string = "top-logprobs"
	flagPresencePenaltyShort              rune   = fakeRune
	flagPresencePenaltyLong               string = "pp"
	flagFrequencyPenaltyShort             rune   = fakeRune
	flagFrequencyPenaltyLong              string = "fp"
	/*
		flagNoThinkingShort                   rune   = fakeRune
		flagNoThinkingLong                    string = "no-thinking"
	*/
)

// Declare flags and have getopt return pointers to the values.
var (
	flagTestPayload                  string
	flagTestPayloadHttpbin           bool
	flagHelp                         bool
	flagVersion                      bool
	flagTemperature                  float64
	flagTopP                         float64
	flagTopK                         int
	flagFile                         string
	flagKeyFile                      string
	flagResTok                       int
	flagStream                       bool
	flagCount                        string
	flagAssistant                    string
	flagSystem                       string
	flagJson                         bool
	flagStop                         string
	flagRaw                          bool
	flagPreview                      bool
	flagTimeout                      int64
	flagTimeoutChunk                 int64
	flagRetries                      int
	flagRetriesWait                  int
	flagPreviousPrompt               string
	flagFinishReason                 bool
	flagSafetyAll                    string
	flagSafetyDangerousContent       string
	flagSafetyHateSpeech             string
	flagSafetyHarassment             string
	flagSafetySexuallyExplicit       string
	flagCitations                    bool
	flagListModels                   bool
	flagModel                        string
	flagModelInfo                    bool
	flagMedia                        string
	flagFileUpload                   string
	flagFileUploadName               string
	flagFileUploadDisplayName        string
	flagFileUploadInfo               string
	flagFileUploadList               bool
	flagFileUploadDelete             string
	flagFileUploadListSize           int
	flagFileUploadListToken          string
	flagResponseFormat               string
	flagResponseFormatSchemaJSON     string
	flagResponseFormatSchemaExamples bool
	flagFunction                     string
	flagFunctionExamples             bool
	flagFunctionCall                 string
	flagFunctionAllowed              string
	flagCodeExecution                bool
	flagFunctionResponse             string
	flagCsv                          string
	flagEmbed                        bool
	flagEmbedDimensions              int
	flagWeb                          string
	flagWebSelect                    string
	flagWebTest                      bool
	flagWebSystem                    bool
	flagPdf                          string
	flagPdfPages                     string
	flagPdfTest                      bool
	flagPdfPageNumber                bool
	flagPdfSystem                    bool
	flagPayloadRun                   string
	flagPayloadSave                  string
	flagCfg                          string
	flagCfgSave                      string
	flagDefaults                     bool
	flagListPresets                  bool
	flagPreset                       string
	flagPresetSystem                 bool
	flagOutput                       string
	flagOutputFull                   bool
	flagNoConsole                    bool
	flagCacheCreate                  bool
	flagCacheTtl                     float64
	flagCacheExpire                  string
	flagCacheDisplayName             string
	flagCacheList                    bool
	flagCacheInfo                    string
	flagCacheListSize                int
	flagCacheListToken               string
	flagCacheDelete                  string
	flagCacheUpdate                  string
	flagCache                        string
	flagEnv                          string
	flagModelUsed                    bool
	flagLogProbs                     bool
	flagTopLogProbs                  int
	flagPresencePenalty              float64
	flagFrequencyPenalty             float64
	//flagNoThinking                   bool
	flags        []getopt.Option = make([]getopt.Option, 0, flagsCount)
	flagsCounter map[string]int  = make(map[string]int)
	flagsTypes   []string        = make([]string, 0, flagsCount)
)

// init vars at program start with 'init()' internal function
func init() {
	flags = append(flags, getopt.FlagLong(&flagTestPayload, flagTestPayloadLong, '\U0001c595', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTestPayload))
	flags = append(flags, getopt.FlagLong(&flagTestPayloadHttpbin, flagTestPayloadHttpbinLong, '\U0001c596', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTestPayloadHttpbin))
	flags = append(flags, getopt.FlagLong(&flagHelp, flagHelpLong, '\U0001f595', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagHelp))
	flags = append(flags, getopt.FlagLong(&flagVersion, flagVersionLong, '\U0001f6bd', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagVersion))
	flags = append(flags, getopt.FlagLong(&flagFile, flagFileLong, flagFileShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFile))
	flags = append(flags, getopt.FlagLong(&flagKeyFile, flagKeyFileLong, flagKeyFileShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagKeyFile))
	flags = append(flags, getopt.FlagLong(&flagResTok, flagResTokLong, flagResTokShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagResTok))
	flags = append(flags, getopt.FlagLong(&flagTemperature, flagTemperatureLong, flagTemperatureShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTemperature))
	flags = append(flags, getopt.FlagLong(&flagTopP, flagTopPLong, '\U0001f601', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTopP))
	flags = append(flags, getopt.FlagLong(&flagTopK, flagTopKLong, '\U0001f602', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTopK))
	flags = append(flags, getopt.FlagLong(&flagStream, flagStreamLong, '\U0001f598', "").SetGroup("notStream"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagStream))
	flags = append(flags, getopt.FlagLong(&flagCount, flagCountLong, flagCountShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCount))
	flags = append(flags, getopt.FlagLong(&flagAssistant, flagAssistantLong, flagAssistantShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagAssistant))
	flags = append(flags, getopt.FlagLong(&flagSystem, flagSystemLong, flagSystemShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagSystem))
	flags = append(flags, getopt.FlagLong(&flagJson, flagJsonLong, '\U0001f002', "").SetGroup("outputFMT"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagJson))
	flags = append(flags, getopt.FlagLong(&flagStop, flagStopLong, '\U0001f504', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagStop))
	flags = append(flags, getopt.FlagLong(&flagRaw, flagRawLong, '\U0001f508', "").SetGroup("outputFMT"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagRaw))
	flags = append(flags, getopt.FlagLong(&flagPreview, flagPreviewLong, flagPreviewShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPreview))
	flags = append(flags, getopt.FlagLong(&flagTimeout, flagTimeoutLong, '\U0001f521', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTimeout))
	flags = append(flags, getopt.FlagLong(&flagTimeoutChunk, flagTimeoutChunkLong, '\U0001f522', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTimeoutChunk))
	flags = append(flags, getopt.FlagLong(&flagRetries, flagRetriesLong, '\U0001f523', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagRetries))
	flags = append(flags, getopt.FlagLong(&flagRetriesWait, flagRetriesWaitLong, '\U0001f000', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagRetriesWait))
	flags = append(flags, getopt.FlagLong(&flagPreviousPrompt, flagPreviousPromptLong, '\U0001f527', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPreviousPrompt))
	flags = append(flags, getopt.FlagLong(&flagFinishReason, flagFinishReasonLong, '\U0001f001', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFinishReason))
	flags = append(flags, getopt.FlagLong(&flagSafetyAll, flagSafetyAllLong, '\U0001f003', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagSafetyAll))
	flags = append(flags, getopt.FlagLong(&flagSafetyDangerousContent, flagSafetyDangerousContentLong, '\U0001f004', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagSafetyDangerousContent))
	flags = append(flags, getopt.FlagLong(&flagSafetyHateSpeech, flagSafetyHateSpeechLong, '\U0001f005', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagSafetyHateSpeech))
	flags = append(flags, getopt.FlagLong(&flagSafetyHarassment, flagSafetyHarassmentLong, '\U0001f006', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagSafetyHarassment))
	flags = append(flags, getopt.FlagLong(&flagSafetySexuallyExplicit, flagSafetySexuallyExplicitLong, '\U0001f007', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagSafetySexuallyExplicit))
	flags = append(flags, getopt.FlagLong(&flagCitations, flagCitationsLong, '\U0001f008', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCitations))
	flags = append(flags, getopt.FlagLong(&flagListModels, flagListModelsLong, flagListModelsShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagListModels))
	flags = append(flags, getopt.FlagLong(&flagModel, flagModelLong, flagModelShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagModel))
	flags = append(flags, getopt.FlagLong(&flagModelInfo, flagModelInfoLong, '\U0001f009', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagModelInfo))
	flags = append(flags, getopt.FlagLong(&flagMedia, flagMediaLong, '\U0001f010', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagMedia))
	flags = append(flags, getopt.FlagLong(&flagFileUpload, flagFileUploadLong, '\U0001f011', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFileUpload))
	flags = append(flags, getopt.FlagLong(&flagFileUploadName, flagFileUploadNameLong, '\U0001f012', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFileUploadName))
	flags = append(flags, getopt.FlagLong(&flagFileUploadDisplayName, flagFileUploadDisplayNameLong, '\U0001f013', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFileUploadDisplayName))
	flags = append(flags, getopt.FlagLong(&flagFileUploadInfo, flagFileUploadInfoLong, '\U0001f014', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFileUploadInfo))
	flags = append(flags, getopt.FlagLong(&flagFileUploadList, flagFileUploadListLong, '\U0001f015', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFileUploadList))
	flags = append(flags, getopt.FlagLong(&flagFileUploadDelete, flagFileUploadDeleteLong, '\U0001f016', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFileUploadDelete))
	flags = append(flags, getopt.FlagLong(&flagFileUploadListSize, flagFileUploadListSizeLong, '\U0001f017', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFileUploadListSize))
	flags = append(flags, getopt.FlagLong(&flagFileUploadListToken, flagFileUploadListTokenLong, '\U0001f018', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFileUploadListToken))
	flags = append(flags, getopt.FlagLong(&flagResponseFormat, flagResponseFormatLong, '\U0001f019', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagResponseFormat))
	flags = append(flags, getopt.FlagLong(&flagResponseFormatSchemaJSON, flagResponseFormatSchemaJSONLong, '\U0001f020', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagResponseFormatSchemaJSON))
	flags = append(flags, getopt.FlagLong(&flagResponseFormatSchemaExamples, flagResponseFormatSchemaExamplesLong, '\U0001e020', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagResponseFormatSchemaExamples))
	flags = append(flags, getopt.FlagLong(&flagFunction, flagFunctionLong, '\U0001f021', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFunction))
	flags = append(flags, getopt.FlagLong(&flagFunctionExamples, flagFunctionExamplesLong, '\U0001f022', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFunctionExamples))
	flags = append(flags, getopt.FlagLong(&flagFunctionCall, flagFunctionCallLong, '\U0001f023', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFunctionCall))
	flags = append(flags, getopt.FlagLong(&flagFunctionAllowed, flagFunctionAllowedLong, '\U0001f024', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFunctionAllowed))
	flags = append(flags, getopt.FlagLong(&flagCodeExecution, flagCodeExecutionLong, '\U0001f025', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCodeExecution))
	flags = append(flags, getopt.FlagLong(&flagFunctionResponse, flagFunctionResponseLong, '\U0001f026', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFunctionResponse))
	flags = append(flags, getopt.FlagLong(&flagCsv, flagCsvLong, '\U0001f027', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCsv))
	flags = append(flags, getopt.FlagLong(&flagEmbed, flagEmbedLong, flagEmbedShort, "").SetGroup("notStream"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagEmbed))
	flags = append(flags, getopt.FlagLong(&flagEmbedDimensions, flagEmbedDimensionsLong, '\U0001f028', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagEmbed))
	flags = append(flags, getopt.FlagLong(&flagWeb, flagWebLong, '\U0001f029', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagWeb))
	flags = append(flags, getopt.FlagLong(&flagWebSelect, flagWebSelectLong, '\U0001f030', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagWebSelect))
	flags = append(flags, getopt.FlagLong(&flagWebTest, flagWebTestLong, '\U0001f031', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagWebTest))
	flags = append(flags, getopt.FlagLong(&flagWebSystem, flagWebSystemLong, '\U0001e031', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagWebSystem))
	flags = append(flags, getopt.FlagLong(&flagPdf, flagPdfLong, '\U0001f032', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPdf))
	flags = append(flags, getopt.FlagLong(&flagPdfPages, flagPdfPagesLong, '\U0001f033', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPdfPages))
	flags = append(flags, getopt.FlagLong(&flagPdfTest, flagPdfTestLong, '\U0001f034', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPdfTest))
	flags = append(flags, getopt.FlagLong(&flagPdfPageNumber, flagPdfPageNumberLong, '\U0001f035', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPdfPageNumber))
	flags = append(flags, getopt.FlagLong(&flagPdfSystem, flagPdfSystemLong, '\U0001e034', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPdfSystem))
	flags = append(flags, getopt.FlagLong(&flagPayloadRun, flagPayloadRunLong, '\U0001f036', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPayloadRun))
	flags = append(flags, getopt.FlagLong(&flagPayloadSave, flagPayloadSaveLong, '\U0001f037', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPayloadSave))
	flags = append(flags, getopt.FlagLong(&flagCfg, flagCfgLong, '\U0001f038', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCfg))
	flags = append(flags, getopt.FlagLong(&flagCfgSave, flagCfgSaveLong, '\U0001f039', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCfgSave))
	flags = append(flags, getopt.FlagLong(&flagDefaults, flagDefaultsLong, '\U0001f040', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagDefaults))
	flags = append(flags, getopt.FlagLong(&flagListPresets, flagListPresetsLong, '\U0001f041', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagListPresets))
	flags = append(flags, getopt.FlagLong(&flagPreset, flagPresetLong, '\U0001f042', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPreset))
	flags = append(flags, getopt.FlagLong(&flagPresetSystem, flagPresetSystemLong, '\U0001f043', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPresetSystem))
	flags = append(flags, getopt.FlagLong(&flagOutput, flagOutputLong, flagOutputShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagOutput))
	flags = append(flags, getopt.FlagLong(&flagOutputFull, flagOutputFullLong, '\U0001f044', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagOutputFull))
	flags = append(flags, getopt.FlagLong(&flagNoConsole, flagNoConsoleLong, '\U0001f045', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagNoConsole))
	flags = append(flags, getopt.FlagLong(&flagCacheCreate, flagCacheCreateLong, '\U0001f046', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCacheCreate))
	flags = append(flags, getopt.FlagLong(&flagCacheTtl, flagCacheTtlLong, '\U0001f047', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCacheTtl))
	flags = append(flags, getopt.FlagLong(&flagCacheExpire, flagCacheExpireLong, '\U0001f048', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCacheExpire))
	flags = append(flags, getopt.FlagLong(&flagCacheDisplayName, flagCacheDisplayNameLong, '\U0001f049', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCacheDisplayName))
	flags = append(flags, getopt.FlagLong(&flagCacheList, flagCacheListLong, '\U0001f050', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCacheList))
	flags = append(flags, getopt.FlagLong(&flagCacheInfo, flagCacheInfoLong, '\U0001f051', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCacheInfo))
	flags = append(flags, getopt.FlagLong(&flagCacheListSize, flagCacheListSizeLong, '\U0001f052', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCacheListSize))
	flags = append(flags, getopt.FlagLong(&flagCacheListToken, flagCacheListTokenLong, '\U0001f053', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCacheListToken))
	flags = append(flags, getopt.FlagLong(&flagCacheDelete, flagCacheDeleteLong, '\U0001f054', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCacheDelete))
	flags = append(flags, getopt.FlagLong(&flagCacheUpdate, flagCacheUpdateLong, '\U0001f055', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCacheUpdate))
	flags = append(flags, getopt.FlagLong(&flagCache, flagCacheLong, '\U0001f056', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCache))
	flags = append(flags, getopt.FlagLong(&flagEnv, flagEnvLong, '\U0001f057', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagEnv))
	flags = append(flags, getopt.FlagLong(&flagModelUsed, flagModelUsedLong, '\U0001f058', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagModelUsed))
	flags = append(flags, getopt.FlagLong(&flagLogProbs, flagLogProbsLong, '\U0001f059', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagLogProbs))
	flags = append(flags, getopt.FlagLong(&flagTopLogProbs, flagTopLogProbsLong, '\U0001f060', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTopLogProbs))
	flags = append(flags, getopt.FlagLong(&flagPresencePenalty, flagPresencePenaltyLong, '\U0001f061', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPresencePenalty))
	flags = append(flags, getopt.FlagLong(&flagFrequencyPenalty, flagFrequencyPenaltyLong, '\U0001f062', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFrequencyPenalty))
	/*
		flags = append(flags, getopt.FlagLong(&flagNoThinking, flagNoThinkingLong, '\U0001f063', ""))
		flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagNoThinking))
	*/
}

// due 'getopt' library limitations need the following 'mutually exclusive options' config
func mutuallyExclusiveOptions() error {
	if flagMedia != "" {
		excluded := []string{flagFileUploadLong, flagFileUploadNameLong, flagFileUploadDisplayNameLong, flagWebLong, flagPdfLong}
		if err := relationsHandler(flagMediaLong, flagMediaLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if flagFileUpload != "" {
		admitted := []string{flagFileUploadNameLong, flagFileUploadDisplayNameLong}
		if err := relationsHandler(flagFileUploadLong, flagFileUploadLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagFileUploadInfo != "" {
		admitted := []string{}
		if err := relationsHandler(flagFileUploadInfoLong, flagFileUploadInfoLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagFileUploadList {
		admitted := []string{flagFileUploadListSizeLong, flagFileUploadListTokenLong}
		if err := relationsHandler(flagFileUploadListLong, flagFileUploadListLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagFileUploadDelete != "" {
		admitted := []string{}
		if err := relationsHandler(flagFileUploadDeleteLong, flagFileUploadDeleteLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagFunction != "" {
		excluded := []string{flagResponseFormatLong, flagCodeExecutionLong, flagPresetLong}
		if err := relationsHandler(flagFunctionLong, flagFunctionLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if flagFunction != "" && flagFunctionResponse == "" {
		// NOTE: when flagFunctionResponse is set, response can be streamed
		excluded := []string{flagStreamLong}
		if err := relationsHandler(flagFunctionLong, flagFunctionLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if flagFunctionAllowed != "" && (flagFunctionCall != "" && (flagFunctionCall != "any" && flagFunctionCall != "ANY")) {
		return fmt.Errorf("function call MODE (\"--%s\" option), must be set to %q if used with \"--%s\" option", flagFunctionCallLong, "any", flagFunctionAllowedLong)
	}
	if flagFunctionResponse != "" {
		excluded := []string{}
		if err := relationsHandler(flagFunctionResponseLong, flagFunctionResponseLong, nil, excluded, true); err != nil {
			return err
		}
	}
	if flagCsv != "" {
		excluded := []string{flagStreamLong, flagWebTestLong, flagPdfTestLong, string(flagOutputShort), flagOutputLong, flagCacheCreateLong}
		if err := relationsHandler(flagCsvLong, flagCsvLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if flagEmbed {
		admitted := []string{string(flagEmbedShort), string(flagModelShort), flagModelLong, flagCsvLong, flagEmbedDimensionsLong, string(flagOutputShort), flagOutputLong}
		if err := relationsHandler(flagEmbedLong, flagEmbedLong, admitted, nil, false); err != nil {
			return err
		}
	}
	if flagWeb != "" {
		excluded := []string{flagPdfLong}
		if err := relationsHandler(flagWebLong, flagWebLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if flagPreview {
		excluded := []string{flagWebTestLong, flagPdfTestLong}
		if err := relationsHandler(flagPreviewLong, flagPreviewLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if flagPdf != "" {
		excluded := []string{flagWebLong}
		if err := relationsHandler(flagPdfLong, flagPdfLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if flagPayloadRun != "" {
		admitted := []string{flagStreamLong, string(flagCountShort), flagCountLong, string(flagModelShort), flagModelLong, flagFinishReasonLong}
		if err := relationsHandler(flagPayloadRunLong, flagPayloadRunLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagPayloadSave != "" {
		excluded := []string{flagPayloadRunLong, flagRawLong, flagJsonLong, string(flagCountShort), flagCountLong, flagCsvLong, flagFinishReasonLong, flagPdfTestLong, flagWebTestLong, flagFileUploadDeleteLong, flagFileUploadLong, flagFileUploadListLong, flagFileUploadInfoLong, flagCacheCreateLong}
		if err := relationsHandler(flagPayloadSaveLong, flagPayloadSaveLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if flagCfg != "" {
		excluded := []string{flagPdfTestLong, flagWebTestLong, flagFileUploadDeleteLong, flagFileUploadLong, flagFileUploadListLong, flagFileUploadInfoLong, flagCacheCreateLong}
		if err := relationsHandler(flagCfgLong, flagCfgLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if flagCfgSave != "" {
		excluded := []string{flagPdfTestLong, flagWebTestLong, flagFileUploadDeleteLong, flagFileUploadLong, flagFileUploadListLong, flagFileUploadInfoLong, flagCacheCreateLong}
		if err := relationsHandler(flagCfgSaveLong, flagCfgSaveLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if flagNoConsole {
		excluded := []string{flagStreamLong, flagCacheCreateLong}
		if err := relationsHandler(flagNoConsoleLong, flagNoConsoleLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if flagCacheCreate {
		excluded := []string{flagCacheLong}
		if err := relationsHandler(flagCacheCreateLong, flagCacheCreateLong, nil, excluded, true); err != nil {
			return err
		}
	}
	if flagCacheExpire != "" {
		excluded := []string{flagCacheTtlLong}
		if err := relationsHandler(flagCacheExpireLong, flagCacheExpireLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if flagCacheList {
		admitted := []string{flagCacheListSizeLong, flagCacheListTokenLong}
		if err := relationsHandler(flagCacheListLong, flagCacheListLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagCacheInfo != "" {
		admitted := []string{}
		if err := relationsHandler(flagCacheInfoLong, flagCacheInfoLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagCacheDelete != "" {
		admitted := []string{}
		if err := relationsHandler(flagCacheDeleteLong, flagCacheDeleteLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagCacheUpdate != "" {
		admitted := []string{flagCacheTtlLong, flagCacheExpireLong}
		if err := relationsHandler(flagCacheUpdateLong, flagCacheUpdateLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagTestPayloadHttpbin {
		excluded := []string{flagTestPayloadLong, string(flagCountShort), flagCountLong, flagCfgSaveLong, flagCsvLong, flagPdfTestLong, flagWebTestLong}
		if err := relationsHandler(flagTestPayloadHttpbinLong, flagTestPayloadHttpbinLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if flagTestPayload != "" {
		excluded := []string{flagTestPayloadHttpbinLong, string(flagCountShort), flagCountLong, flagCfgSaveLong, flagCsvLong, flagPdfTestLong, flagWebTestLong}
		if err := relationsHandler(flagTestPayloadLong, flagTestPayloadLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if flagLogProbs {
		excluded := []string{flagStreamLong, flagFunctionLong}
		if err := relationsHandler(flagLogProbsLong, flagLogProbsLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if getopt.GetCount(flagTopLogProbsLong) > 0 {
		excluded := []string{flagStreamLong, flagFunctionLong}
		if err := relationsHandler(flagTopLogProbsLong, flagTopLogProbsLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if flagCitations {
		excluded := []string{flagStreamLong, flagFunctionLong, flagCsvLong}
		if err := relationsHandler(flagCitationsLong, flagCitationsLong, nil, excluded, false); err != nil {
			return err
		}
	}
	return nil
}

// check for flag dependencies (one flag depends from another)
func checkFlagDependencies() error {
	if flagFunctionCall != "" {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagFunctionLong] = fakeRune
		if err := dependencies(flagFunctionCallLong, dependOn); err != nil {
			return err
		}
	}
	if flagFunctionAllowed != "" {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagFunctionLong] = fakeRune
		if err := dependencies(flagFunctionAllowedLong, dependOn); err != nil {
			return err
		}
	}
	if flagFunctionResponse != "" {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagFunctionLong] = fakeRune
		dependOn[flagPreviousPromptLong] = fakeRune
		dependOn[flagAssistantLong] = flagAssistantShort
		if err := dependencies(flagFunctionResponseLong, dependOn); err != nil {
			return err
		}
	}
	if getopt.GetCount(flagEmbedDimensionsLong) > 0 {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagEmbedLong] = flagEmbedShort
		if err := dependencies(flagEmbedDimensionsLong, dependOn); err != nil {
			return err
		}
	}
	if flagWebSelect != "" {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagWebLong] = fakeRune
		if err := dependencies(flagWebSelectLong, dependOn); err != nil {
			return err
		}
	}
	if flagWebTest {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagWebLong] = fakeRune
		if err := dependencies(flagWebTestLong, dependOn); err != nil {
			return err
		}
	}
	if flagPdfPages != "" {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagPdfLong] = fakeRune
		if err := dependencies(flagPdfPagesLong, dependOn); err != nil {
			return err
		}
	}
	if flagPdfPageNumber {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagPdfLong] = fakeRune
		dependOn[flagPdfTestLong] = fakeRune
		if err := dependencies(flagPdfPageNumberLong, dependOn); err != nil {
			return err
		}
	}
	if flagPdfTest {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagPdfLong] = fakeRune
		if err := dependencies(flagPdfTestLong, dependOn); err != nil {
			return err
		}
	}
	if getopt.GetCount(flagTimeoutChunkLong) > 0 {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagStreamLong] = fakeRune
		if err := dependencies(flagTimeoutChunkLong, dependOn); err != nil {
			if flagCfg == "" {
				return err
			}
		}
	}
	if flagFileUploadName != "" {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagFileUploadLong] = fakeRune
		if err := dependencies(flagFileUploadNameLong, dependOn); err != nil {
			return err
		}
	}
	if flagFileUploadDisplayName != "" {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagFileUploadLong] = fakeRune
		if err := dependencies(flagFileUploadDisplayNameLong, dependOn); err != nil {
			return err
		}
	}
	if getopt.GetCount(flagFileUploadListSizeLong) > 0 {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagFileUploadListLong] = fakeRune
		if err := dependencies(flagFileUploadListSizeLong, dependOn); err != nil {
			return err
		}
	}
	if flagFileUploadListToken != "" {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagFileUploadListLong] = fakeRune
		if err := dependencies(flagFileUploadListTokenLong, dependOn); err != nil {
			return err
		}
	}
	if flagPresetSystem {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagPresetLong] = fakeRune
		if err := dependencies(flagPresetSystemLong, dependOn); err != nil {
			return err
		}
	}
	if flagPreviousPrompt != "" {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagAssistantLong] = flagAssistantShort
		if err := dependencies(flagPreviousPromptLong, dependOn); err != nil {
			return err
		}
	}
	if flagAssistant != "" {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagPreviousPromptLong] = fakeRune
		if err := dependencies(flagAssistantLong, dependOn); err != nil {
			return err
		}
	}
	if getopt.GetCount(flagRetriesWaitLong) > 0 {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagRetriesLong] = fakeRune
		if err := dependencies(flagRetriesWaitLong, dependOn); err != nil {
			return err
		}
	}
	if flagOutputFull {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagOutputLong] = flagOutputShort
		if err := dependencies(flagOutputFullLong, dependOn); err != nil {
			return err
		}
	}
	if flagNoConsole {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagOutputLong] = flagOutputShort
		if err := dependencies(flagNoConsoleLong, dependOn); err != nil {
			return err
		}
	}
	if flagCacheCreate {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagMediaLong] = fakeRune
		if err := dependencies(flagCacheCreateLong, dependOn); err != nil {
			return err
		}
	}
	if getopt.GetCount(flagCacheTtlLong) > 0 {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagCacheCreateLong] = fakeRune
		dependOn[flagCacheUpdateLong] = fakeRune
		flagDependencyMode = alternativeFlagDependencyMode
		if err := dependencies(flagCacheTtlLong, dependOn); err != nil {
			return err
		}
		flagDependencyMode = defaultFlagDependencyMode
	}
	if flagCacheExpire != "" {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagCacheCreateLong] = fakeRune
		dependOn[flagCacheUpdateLong] = fakeRune
		flagDependencyMode = alternativeFlagDependencyMode
		if err := dependencies(flagCacheExpireLong, dependOn); err != nil {
			return err
		}
		flagDependencyMode = defaultFlagDependencyMode
	}
	if flagCacheDisplayName != "" {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagCacheCreateLong] = fakeRune
		if err := dependencies(flagCacheDisplayNameLong, dependOn); err != nil {
			return err
		}
	}
	if getopt.GetCount(flagCacheListSizeLong) > 0 {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagCacheListLong] = fakeRune
		if err := dependencies(flagCacheListSizeLong, dependOn); err != nil {
			return err
		}
	}
	if flagCacheListToken != "" {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagCacheListLong] = fakeRune
		if err := dependencies(flagCacheListTokenLong, dependOn); err != nil {
			return err
		}
	}
	if flagCacheUpdate != "" {
		var dependOn flagDependencies = make(flagDependencies, 0)
		dependOn[flagCacheTtlLong] = fakeRune
		dependOn[flagCacheExpireLong] = fakeRune
		flagDependencyMode = alternativeFlagDependencyMode
		if err := dependencies(flagCacheUpdateLong, dependOn); err != nil {
			return err
		}
		flagDependencyMode = defaultFlagDependencyMode
	}
	return nil
}

// check if any string was provided to string options
func argProvided() error {
	for i := range flags {
		if flags[i].Seen() && !flags[i].IsFlag() && flags[i].Value().String() == "" {
			return fmt.Errorf("option '--%s' missing or wrong argument", flags[i].LongName())
		}
	}
	return nil
}

// check if long option are given with single or double dash
// without this check long option '--abcd', written with single dash, '-abcd', is parsed as a short option '-a'
// try with 'ls' command: 'ls -l -a' and 'ls -l --author' vs 'ls -l -author' (the latter behave as 'ls -l -a')
func checkLongOptionDashes(args []string) error {
	const dash byte = '-'
	const doubleDash string = "--"
	var wasBool bool = true

	// check if arg begin with dash (if begin with dash could be an option)
loopAllGivenArgs:
	for i, lenArgs := 0, len(args); i < lenArgs; i++ {
		var counter int = 0

		// check for empty strings, such as failed shell expansion - eg: "$(cat non-existant-file)"
		if args[i] == "" {
			continue
		}

		// if previous option was not bool, then next arg is (maybe) only the option argument
		if !wasBool {
			wasBool = true
			continue
		}

		// don't check arguments without dash prefix
		if len(args[i]) > 0 && args[i][0] != dash {
			continue
		}

		// don't check arguments with dash prefix + single char - eg: -a, -b, -c, etc...
		if len(args[i]) == 2 && args[i][0] == dash {
			continue
		}

		// remove single or double dash prefix
		s := strings.TrimPrefix(args[i], string(dash))
		s = strings.TrimPrefix(s, string(dash))

		// remove first '=' char, if any (eg. --my-opt=someData)
		s, _, _ = strings.Cut(s, "=")

		// check if string is a number
		if isNum(s) {
			continue
		}

		for _, v := range flags {
			if v.LongName() == s && (args[i][0] == dash && args[i][1] != dash) {
				// if 'longoption' exist, but was given without double dash (eg: --longoption OK, -longoption WRONG)
				return fmt.Errorf("long option with single dash: '-%s' must be '--%[1]s'", s)
			} else if v.LongName() == s {
				// if option is not boolean, then next arg is not an option
				if !v.IsFlag() {
					wasBool = false
				}
				// 'longoption' exist and was given correctly (eg: --longoption)
				continue loopAllGivenArgs
			} else {
				// 'longoption' not found in this inner loop cycle
				counter++
			}
		}

		// option not found
		if counter >= len(flags) {
			return fmt.Errorf("wrong option: '%s'", args[i])
		}
	}
	return nil
}

// used by presets - check if any parameter, such as temperature or model, was given.
// If yes it use command-line parameter setting, instead of preset parameter setting.
func countFlags(args []string, flag string) bool {
	for _, v := range args {
		flagsCounter[v]++
	}
	if _, ok := flagsCounter[flag]; ok {
		return true
	}
	return false
}

// check 'string type' option arguments for "-" char as first char of argument
func checkFlagTypeString() error {
	for i := range flags {
		if flags[i].Seen() && (flagsTypes[i] == "string" && strings.HasPrefix(flags[i].Value().String(), "-")) {
			return fmt.Errorf("\awrong parameter for %s: '%s'", flags[i].Name(), flags[i].Value().String())
		}
	}
	return nil
}

// check if string is a number, integer or float - false = NOT number, true = IS number
func isNum(s string) bool {
	if _, err := strconv.ParseInt(s, 10, 64); err == nil {
		return true
	}
	if _, err := strconv.ParseFloat(s, 64); err == nil {
		return true
	}
	if _, err := strconv.ParseComplex(s, 128); err == nil {
		return true
	}
	return false
}

// callback function for 'getopt.Visit()': ADMITTED
// ref: https://pkg.go.dev/github.com/pborman/getopt/v2#Visit
func checkFlagRelationsAdmitted(option getopt.Option, fr *flagRelated) {
	n := option.Name()
	s := strings.TrimPrefix(n, "-")
	s = strings.TrimPrefix(s, "-")
	if fr.admitted != nil {
		if _, ok := fr.admitted[s]; !ok {
			flagRelationsErr = fmt.Errorf("options '--%s' and '%s' are mutually exclusive", fr.name, n)
		}
	}
}

// callback function for 'getopt.Visit()': EXCLUDED
// ref: https://pkg.go.dev/github.com/pborman/getopt/v2#Visit
func checkFlagRelationsExcluded(option getopt.Option, fr *flagRelated) {
	n := option.Name()
	s := strings.TrimPrefix(n, "-")
	s = strings.TrimPrefix(s, "-")
	if fr.excluded != nil {
		if _, ok := fr.excluded[s]; ok {
			flagRelationsErr = fmt.Errorf("options '--%s' and '%s' are mutually exclusive", fr.name, n)
		}
	}
}

// wrapper for the 'getopt.Visit()' callback function
// ref: https://pkg.go.dev/github.com/pborman/getopt/v2#Visit
func wrapCheckFlagRelations(callback func(getopt.Option, *flagRelated), fr *flagRelated) func(getopt.Option) {
	return func(option getopt.Option) {
		callback(option, fr)
	}
}

// check admitted/excluded flags for a given flag name using 'getopt.Visit()'
// ref: https://pkg.go.dev/github.com/pborman/getopt/v2#Visit
func relationsHandler(name, flagLongName string, admitted, excluded []string, checkNonOptArgs bool) error {
	// non-option arguments
	if checkNonOptArgs {
		if len(getopt.Args()) > 0 {
			s := getopt.Args()
			return fmt.Errorf("option '--%s' and arg %q are mutually exclusive", flagLongName, s[0])
		}
	}
	// init flag data
	f := &flagRelated{
		admitted: map[string][]struct{}{
			name: nil,
		},
		excluded: map[string][]struct{}{},
		name:     name,
	}
	// admitted
	if admitted != nil {
		for _, v := range admitted {
			f.admitted[v] = nil
		}
		for k := range admittedCommon {
			f.admitted[k] = nil
		}
		// check relations
		relations := wrapCheckFlagRelations(checkFlagRelationsAdmitted, f)
		getopt.Visit(relations)
	}
	// excluded
	if excluded != nil && len(excluded) > 0 {
		for _, v := range excluded {
			f.excluded[v] = nil
		}
		// check relations
		relations := wrapCheckFlagRelations(checkFlagRelationsExcluded, f)
		getopt.Visit(relations)
	}
	// result
	if flagRelationsErr != nil {
		return flagRelationsErr
	}
	return nil
}

// check a given flag's dependencies
func dependencies(flagName string, dependOn flagDependencies) error {
	// vars for OR dependencies
	var notFound int = 1        // counter
	var max int = len(dependOn) // limit for counter

	for l, s := range dependOn {
		if getopt.GetCount(l) > 0 && flagDependencyMode == alternativeFlagDependencyMode {
			return nil
		}
		if getopt.GetCount(l) == 0 {
			switch {
			case s != fakeRune:
				if getopt.GetCount(s) > 0 && flagDependencyMode == alternativeFlagDependencyMode {
					return nil
				}
				if (getopt.GetCount(s) == 0 && flagDependencyMode == defaultFlagDependencyMode) || notFound == max {
					return fmt.Errorf("%s", dependenciesError(flagName, dependOn))
				}
				//return nil
			default:
				if flagDependencyMode == defaultFlagDependencyMode || notFound == max {
					return fmt.Errorf("%s", dependenciesError(flagName, dependOn))
				}
			}
		}
		notFound++
	}
	return nil
}

// print a given flag's dependencies
func dependenciesError(flagName string, dependOn flagDependencies) string {
	var b strings.Builder
	var i int = 0
	var l int = len(dependOn)
	if _, err := b.WriteString("\aoption '--" + flagName + "' depend from options "); err != nil {
		return err.Error()
	}
	for f := range dependOn {
		s := fmt.Sprintf("'--%s'", f)
		if _, err := b.WriteString(s); err != nil {
			return err.Error()
		}
		if l > 1 && (l-i) == 2 {
			if _, err := b.WriteString(flagDependencyMode); err != nil {
				return err.Error()
			}
			i++
			continue
		}
		if (l - i) > 2 {
			if _, err := b.WriteString(", "); err != nil {
				return err.Error()
			}
		}
		i++
	}
	return b.String()
}

// checks for flags that do not require any prompts
func checkNoPrompt() bool {
	for l, s := range flagNoPrompt {
		switch {
		case s != fakeRune:
			if getopt.GetCount(l) > 0 || getopt.GetCount(s) > 0 {
				return true
			}
		default:
			if getopt.GetCount(l) > 0 {
				return true
			}
		}
	}
	return false
}
