# ref: https://cloud.google.com/vertex-ai/generative-ai/docs/multimodal/list-token
import argparse
import csv
from vertexai.preview import tokenization

def main():
    # parse command line arguments
    parser = argparse.ArgumentParser(description="Compute tokens from text and save to CSV")
    parser.add_argument("--model_name", default="gemini-1.5-flash", help="Model name (default: gemini-1.5-flash)")
    parser.add_argument("contents", type=str, help="Text to tokenize")
    parser.add_argument("--csv", type=str, help="CSV filename to save output", required=False)

    args = parser.parse_args()

    if not args.contents:
        parser.error("PROMPT missing")

    tokenizer = tokenization.get_tokenizer_for_model(args.model_name)
    computed_tokens = tokenizer.compute_tokens(args.contents)
    count_tokens = tokenizer.count_tokens(args.contents)

    # Extract token_ids and tokens as strings
    token_ids = computed_tokens.token_info_list[0].token_ids
    tokens = [token.decode('utf-8') for token in computed_tokens.token_info_list[0].tokens]

    # Create CSV data
    if args.csv != None:
        csv_data = [tokens, token_ids]
        # Save to CSV
        with open(args.csv, 'w', newline='') as csvfile:
            csv_writer = csv.writer(csvfile)
            csv_writer.writerows(csv_data)
            print("CSV data saved to:", args.csv)
            print()

    # print results
    print("token_ids:", token_ids)
    print("tokens:", tokens)
    print("quantity:", count_tokens.total_tokens)

if __name__ == "__main__":
    main()

