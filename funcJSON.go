/*  'geminicli' Terminal Chat Completion client for Google's Gemini AI models
    Copyright (C) 2024 gimlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"sort"
	"strings"

	"github.com/pborman/getopt/v2"
)

// create payloads
func createPayloadJSON() (payloadJSON []byte, err error) {
	// function name to return if any error occur
	var funcName string = "createPayloadJSON"

	// configure model request
	switch {
	case flagEmbed:
		// embeddings

		// batch
		if len(inputs) > 1 {
			var payloadBatch requestEmbeddingBatch
			var requests []requestEmbedding
			for i, v := range inputs {
				var payload requestEmbedding
				var c content
				var p part
				var parts []part
				p.Text = v
				if flagTestPayload != "" || flagTestPayloadHttpbin {
					p.Text = fmt.Sprintf("%s_%d", fakeEmbedPrompt, i+1)
				}
				parts = append(parts, p)
				c.Parts = parts
				c.Role = user
				payload.Content = &c
				payload.Model = "models/" + model
				if flagEmbedDimensions > 0 {
					payload.OutputDimensionality = flagEmbedDimensions
				}
				requests = append(requests, payload)
			}
			payloadBatch.Requests = requests
			// create JSON payload
			payloadJSON, err = json.Marshal(payloadBatch)
		}

		// single prompt
		if len(inputs) == 1 {
			var payload requestEmbedding
			payload.Content = &contents[0]
			if flagEmbedDimensions > 0 {
				payload.OutputDimensionality = flagEmbedDimensions
			}
			// create JSON payload
			payloadJSON, err = json.Marshal(payload)
		}

		// check for errors
		if err != nil {
			return nil, fmt.Errorf("func %q - error creating embeddings JSON payload: %w\n", funcName, err)
		}
	case flagFileUpload != "":
		f := fileUpload{
			Name:        "files/" + mediaUploadName,
			DisplayName: mediaUploadDisplayName,
		}

		payload := &requestUpload{
			File: &f,
		}

		// create JSON payload
		payloadJSON, err = json.Marshal(payload)
		if err != nil {
			return nil, fmt.Errorf("func %q - error creating JSON payload: %w\n", funcName, err)
		}
	case flagCacheCreate:
		// cache API
		payload := cachedContent{
			Contents: contents,
			Model:    "models/" + model,
		}

		// TTL/Expire
		if getopt.GetCount(flagCacheExpireLong) == 0 {
			payload.TTL = cacheTTL
		}
		if flagCacheExpire != "" {
			payload.ExpireTime = cacheExpire
		}

		// display name
		if flagCacheDisplayName != "" {
			payload.DisplayName = flagCacheDisplayName
		}

		// system instruction
		if (flagSystem != "" || flagWeb != "" || flagPdf != "" || flagPreset != "") && len(systemContent.Parts) > 0 {
			payload.SystemInstruction = &systemContent
		}

		// create JSON payload
		payloadJSON, err = json.Marshal(payload)
		if err != nil {
			return nil, fmt.Errorf("func %q - error creating JSON payload: %w\n", funcName, err)
		}
	case flagCacheUpdate != "":
		var payload cachedContentUpdate

		// TTL/Expire
		if getopt.GetCount(flagCacheTtlLong) > 0 {
			payload.TTL = cacheTTL
		}
		if flagCacheExpire != "" {
			payload.ExpireTime = cacheExpire
		}

		// create JSON payload
		payloadJSON, err = json.Marshal(payload)
		if err != nil {
			return nil, fmt.Errorf("func %q - error creating JSON payload: %w\n", funcName, err)
		}
	default:
		// completion task, tool, function
		gc := generationConfig{
			Temperature:      temperature,
			TopP:             top_p,
			TopK:             top_k,
			CandidateCount:   numberOfResponses,
			MaxOutputTokens:  maxTokens,
			StopSequences:    stopSequences,
			Logprobs:         logProbs,
			TopLogprobs:      topLogProbs,
			PresencePenalty:  presencePenalty,
			FrequencyPenalty: frequencyPenalty,
		}
		if responseFormat != "text" && responseFormat != "text/plain" && responseFormat != "" {
			gc.ResponseMimeType = responseFormat
		}

		// flagResponseFormat can be a cmdline option or a toml config option, so must be verified again
		if flagResponseFormatSchemaJSON != "" {
			gc.ResponseMimeType = responseFormat
			err = json.Unmarshal([]byte(flagResponseFormatSchemaJSON), &gc.ResponseSchema)
			if err != nil {
				return nil, fmt.Errorf("func %q - error creating schema format: %w\n\nprovided schema:\n%s", funcName, err, flagResponseFormatSchemaJSON)
			}
		}

		// safety settings
		var s safetySetting
		var safetySettings []safetySetting
		for k, v := range safety {
			s.Category = k
			s.Threshold = v
			safetySettings = append(safetySettings, s)
		}

		// basic payload
		payload := &request{
			GenerationConfig: &gc,
			Contents:         contents,
			SafetySettings:   safetySettings,
		}

		// cached content
		if flagCache != "" {
			payload.CachedContent = cacheContent
			if flagTestPayload != "" || flagTestPayloadHttpbin {
				payload.CachedContent = "CACHED_CONTENT_HERE"
			}
		}

		// system instruction
		if (flagSystem != "" || flagWeb != "" || flagPdf != "" || flagPreset != "") && len(systemContent.Parts) > 0 {
			payload.SystemInstruction = &systemContent
		}

		// code execution
		if flagCodeExecution {
			var c codeExecution
			var t tool
			var tools []tool
			t.CodeExecution = &c
			tools = append(tools, t)
			// add code exec to payload
			payload.Tools = tools
		}

		// functions
		if flagFunction != "" {
			if flagFunctionResponse == "" {
				// add function MODE to payload
				var tcfg toolConfig
				var fcfg functionCallingConfig
				switch {
				case flagFunctionAllowed != "":
					fcfg.Mode = defaultFunctionCallAllowed
					fcfg.AllowedFunctionNames = strings.Split(flagFunctionAllowed, ",")
				case flagFunctionCall != "":
					fcfg.Mode = fmode[strings.ToUpper(flagFunctionCall)]
				default:
					fcfg.Mode = defaultFunctionCall
				}
				tcfg.FunctionCallingConfig = &fcfg
				payload.ToolConfig = &tcfg
			}

			// create objects to store functions
			var tools []tool
			var functionList []string = make([]string, 0)
			functionList = strings.Split(flagFunction, objectSplitChar)
			var t tool
			// parse functions list
			for i, f := range functionList {
				var declaration functionDeclaration
				switch {
				case flagTestPayload != "" || flagTestPayloadHttpbin:
					declaration.Name = fmt.Sprintf("%s_%d", "FUNCTION_NAME_HERE", i+1)
					declaration.Description = fmt.Sprintf("%s_%d", "FUNCTION_DESCRIPTION_HERE", i+1)
				default:
					// check if string is a valid json object
					if err := isValidJSON(f); err != nil {
						return nil, fmt.Errorf("func %q - invalid 'function' JSON format: %w\nJSON data: %s\n", funcName, err, f)
					}
					// unmarshal single function object
					if err := json.Unmarshal([]byte(f), &declaration); err != nil {
						return nil, fmt.Errorf("func %q - invalid 'function' data: %s\n", funcName, flagFunction)
					}
				}
				// append function object to 'functionDeclarations' (json) array
				t.FunctionDeclarations = append(t.FunctionDeclarations, declaration)
			}
			tools = append(tools, t)

			// add functions to payload
			payload.Tools = tools
		}

		// create JSON payload
		payloadJSON, err = json.Marshal(payload)
		if err != nil {
			return nil, fmt.Errorf("func %q - error creating JSON payload: %w\n", funcName, err)
		}

		// encode request data to csv
		if flagCsv != "" && payload != nil {
			requestCSV(payload)
		}
	}

	return payloadJSON, nil
}

// pretty print one line JSON
func prettyPrintJsonString(jsonString string) (formattedString string, err error) {
	// function name to return if any error occur
	var funcName string = "prettyPrintJsonString"

	var buf bytes.Buffer
	if err := json.Indent(&buf, []byte(jsonString), "", "  "); err != nil {
		return "", fmt.Errorf("func %q - error formatting JSON response: %w\n", funcName, err)
	}

	return buf.String(), nil
}

// check for valid JSON format
func isValidJSON(data string) error {
	var temp interface{}
	err := json.Unmarshal([]byte(data), &temp)
	return err
}

func listModels() error {
	// function name to return if any error occur
	var funcName string = "listModels"

	const prefixGemini string = "gemini-"
	const prefixEmbeddings string = "text-embedding"
	var modelsData modelsListData

	// request models list
	models, err := httpRequest("GET", true)
	if err != nil {
		return err
	}

	// validate response
	if err := isValidJSON(models); err != nil {
		return err
	}

	// decode response
	if err := json.Unmarshal([]byte(models), &modelsData); err != nil {
		return fmt.Errorf("func %q - error extracting JSON response: %w\n", funcName, err)
	}

	// extract and print data
	switch {
	case len(modelsData.Models) == 0:
		fmt.Printf("models data not available - retry later\n")
	default:
		// sort and filter models name
		for _, m := range modelsData.Models {
			name := strings.Split(m.Name, "/")[1]
			if strings.HasPrefix(name, prefixGemini) || strings.HasPrefix(name, prefixEmbeddings) {
				modelNames = append(modelNames, name)
			}
		}
		sort.SliceStable(modelNames, func(i, j int) bool { return modelNames[i] < modelNames[j] })

		/*
			// this function can also be used by 'checkModelName()' function
			if !flagListModels {
				return nil
			}
		*/

		fmt.Println("chat/completion models:")
		for _, m := range modelNames {
			if strings.HasPrefix(m, prefixGemini) {
				fmt.Printf("  %s\n", m)
			}
		}

		fmt.Println()

		fmt.Println("embedding models:")
		for _, m := range modelNames {
			if strings.HasPrefix(m, prefixEmbeddings) {
				fmt.Printf("  %s\n", m)
			}
		}

		fmt.Printf("\n%s\n", modelsTechRef)
	}

	return nil
}

func modelInfo() error {
	// function name to return if any error occur
	var funcName string = "modelInfo"

	// request models list
	m, err := httpRequest("GET", true)
	if err != nil {
		return fmt.Errorf("func %q: %w\n", funcName, err)
	}

	// validate response
	if err := isValidJSON(m); err != nil {
		return err
	}

	// print model information
	ppJSON, err := prettyPrintJsonString(m)
	if err != nil {
		return fmt.Errorf("func %q - error extracting JSON response: %w\n", funcName, err)
	}
	fmt.Println(ppJSON)

	return nil
}
