# CODE CHECK RESULTS

---

## // --- GOSEC --- //

```text
[gosec] 2024/09/02 15:06:33 Including rules: default
[gosec] 2024/09/02 15:06:33 Excluding rules: default
[gosec] 2024/09/02 15:06:33 Import directory: /home/gianluca/Documents/Development/go/src/aiGemini
[gosec] 2024/09/02 15:06:34 Checking package: main
[gosec] 2024/09/02 15:06:34 Checking file: /home/gianluca/Documents/Development/go/src/aiGemini/funcCompute.go
[gosec] 2024/09/02 15:06:38 Checking file: /home/gianluca/Documents/Development/go/src/aiGemini/funcConfig.go
[gosec] 2024/09/02 15:06:40 Checking file: /home/gianluca/Documents/Development/go/src/aiGemini/funcExtract.go
[gosec] 2024/09/02 15:06:41 Checking file: /home/gianluca/Documents/Development/go/src/aiGemini/funcJSON.go
[gosec] 2024/09/02 15:06:42 Checking file: /home/gianluca/Documents/Development/go/src/aiGemini/funcSelector.go
[gosec] 2024/09/02 15:06:43 Checking file: /home/gianluca/Documents/Development/go/src/aiGemini/funcWeb.go
[gosec] 2024/09/02 15:06:45 Checking file: /home/gianluca/Documents/Development/go/src/aiGemini/hdrConfig.go
[gosec] 2024/09/02 15:06:45 Checking file: /home/gianluca/Documents/Development/go/src/aiGemini/hdrData.go
[gosec] 2024/09/02 15:06:45 Checking file: /home/gianluca/Documents/Development/go/src/aiGemini/hdrMessages.go
[gosec] 2024/09/02 15:06:45 Checking file: /home/gianluca/Documents/Development/go/src/aiGemini/hdrOpt.go
[gosec] 2024/09/02 15:06:52 Checking file: /home/gianluca/Documents/Development/go/src/aiGemini/hdrPresets.go
[gosec] 2024/09/02 15:06:52 Checking file: /home/gianluca/Documents/Development/go/src/aiGemini/main.go
Results:


Summary:
  Gosec  : dev
  Files  : 12
  Lines  : 7530
  Nosec  : 0
  Issues : 0
```

---

## // --- GO VET --- //

```text
go vet: OK
```

---

## // --- GOVULNCHECK --- //

```text
No vulnerabilities found.
```

---

## // --- MEMORY ALIGNMENT --- //

```text
fieldalignment: OK
```

---
