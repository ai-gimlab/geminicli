/*  'geminicli' Terminal Chat Completion client for Google's Gemini AI models
    Copyright (C) 2024 gimlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/fs"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/pborman/getopt/v2"
	"gitlab.com/sysUtils/go-libraries/librandom"
)

func configFlags() (err error) {
	// function name to return if any error occur
	var funcName string = "configFlags"

	// model selection
	if flagResponseFormatSchemaJSON != "" {
		model = defaultModelFormatJSchema
	}
	if flagCache != "" || flagCacheCreate {
		model = defaultModelCache
	}
	if flagEmbed {
		model = defaultModelEmbed
	}
	if flagModel != "" {
		model = flagModel
	}

	// method selection
	if flagStream {
		method = methodGenerateStream + keyStream + "&"
		if flagCfg == "" {
			timeout = timeoutCompletionStream
		}
	}
	if flagCount != "" && ((flagPreview || flagWebTest || flagPdfTest) && strings.Contains(flagCount, tokenInput)) {
		method = methodCountTokens
		timeout = timeoutEstimatedTokens
	}
	if flagEmbed && !strings.Contains(prompt, objectSplitChar) {
		method = methodEmbedContent
	}
	if flagEmbed && strings.Contains(prompt, objectSplitChar) {
		method = methodEmbedContentBatch
	}

	// config network
	if getopt.GetCount(flagTimeoutLong) > 0 {
		timeout = time.Second * time.Duration(flagTimeout)
	}
	if getopt.GetCount(flagRetriesLong) > 0 {
		retries = flagRetries
	}
	if getopt.GetCount(flagRetriesWaitLong) > 0 {
		retriesWait = flagRetriesWait
	}
	if flagTimeoutChunk != 0 {
		timeoutChunk = time.Second * time.Duration(flagTimeoutChunk)
	}

	// ------ embeddings ------ //
	if flagEmbed {
		inputs = strings.Split(prompt, objectSplitChar)
	}
	if getopt.GetCount(flagEmbedDimensionsLong) > 0 {
		if flagEmbedDimensions < minEmbeddingDimensions {
			return fmt.Errorf("wrong value for \"--%s\" option: %d\nminimum embedding dimensions value: %d", flagEmbedDimensionsLong, flagEmbedDimensions, minEmbeddingDimensions)
		}
	}

	// ------ functions ------ //
	if flagFunction != "" && flagModel == "" {
		model = defaultModelFunction
		functionCallMode = defaultFunctionCall
	}
	if flagFunctionCall != "" {
		s := strings.ToUpper(flagFunctionCall)
		if _, ok := fmode[s]; !ok {
			return fmt.Errorf("func %q - wrong argument for option \"--%s\": %q", funcName, flagFunctionCallLong, flagFunctionCall)
		}
		functionCallMode = flagFunctionCall
	}

	// media
	if flagMedia != "" {
		switch {
		case strings.Contains(flagMedia, ","):
			// multiple comma separated image filenames
			mediaArgs = strings.Split(flagMedia, ",")
		default:
			// single image filename
			mediaArgs = append(mediaArgs, flagMedia)
		}
		for _, v := range mediaArgs {
			if strings.HasPrefix(v, uriPrefix) {
				if _, err := url.ParseRequestURI(v); err == nil {
					continue
				}
			}
			mediaArgsLocal = append(mediaArgsLocal, v)
		}
		if err := validateMediaLocal(); err != nil {
			return err
		}
		if flagPreset == "" {
			maxTokens = maxTokensMedia
		}
	}
	if flagFileUpload != "" {
		if flagFileUploadName != "" {
			mediaUploadName = flagFileUploadName
		}
		if flagFileUploadName == "" {
			rnd, err := librandom.GenRandIntS(len(nameChars), 12)
			if err != nil {
				return err
			}
			for _, v := range rnd {
				mediaUploadName = mediaUploadName + string(nameChars[v])
			}
		}
		if flagFileUploadDisplayName != "" {
			mediaUploadDisplayName = flagFileUploadDisplayName
		}
	}

	// ------ configs ------ //
	// temperature
	if (getopt.GetCount(flagTemperatureLong) > 0 || getopt.GetCount(flagTemperatureShort) > 0) && (flagTemperature >= minLimitDefault && flagTemperature <= maxTemperatureLimit) {
		temperature = flagTemperature
	}
	if (getopt.GetCount(flagTemperatureLong) > 0 || getopt.GetCount(flagTemperatureShort) > 0) && (flagTemperature < minLimitDefault || flagTemperature > maxTemperatureLimit) {
		return fmt.Errorf("func %q - wrong parameter for -%c or --%s: %.2f (less than %.2f or greater than %.2f - max is model dependent)", funcName, flagTemperatureShort, flagTemperatureLong, flagTemperature, minLimitDefault, maxTemperatureLimit)
	}
	// top_P
	if getopt.GetCount(flagTopPLong) > 0 && (flagTopP >= minLimitDefault && flagTopP <= maxTopPLimit) {
		top_p = flagTopP
	}
	if getopt.GetCount(flagTopPLong) > 0 && (flagTopP < minLimitDefault || flagTopP > maxTopPLimit) {
		return fmt.Errorf("func %q - wrong parameter for --%s: %.2f (less than %.2f or greater than %.2f)", funcName, flagTopPLong, flagTopP, minLimitDefault, maxTopPLimit)
	}
	// top_K
	if getopt.GetCount(flagTopKLong) > 0 && flagTopK >= minTopKLimit {
		top_k = flagTopK
	}
	if getopt.GetCount(flagTopKLong) > 0 && flagTopK < minTopKLimit {
		return fmt.Errorf("func %q - wrong parameter for --%s: %d (less than %d)", funcName, flagTopKLong, flagTopK, minTopKLimit)
	}
	// logprobs
	if flagLogProbs || getopt.GetCount(flagTopLogProbsLong) > 0 {
		logProbs = true
	}
	if getopt.GetCount(flagTopLogProbsLong) > 0 {
		if flagTopLogProbs < 0 || flagTopLogProbs > maxTopLogProbsLimit {
			return fmt.Errorf("func %q - option --%s must be in the range [%d, %d]", funcName, flagTopLogProbsLong, int(minLimitDefault), maxTopLogProbsLimit)
		}
		topLogProbs = flagTopLogProbs
	}
	// presence penalty
	if flagPresencePenalty != 0 && (flagPresencePenalty >= minPenaltyLimit && flagPresencePenalty < maxPenaltyLimit) {
		presencePenalty = flagPresencePenalty
	}
	if flagPresencePenalty != 0 && (flagPresencePenalty < minPenaltyLimit || flagPresencePenalty >= maxPenaltyLimit) {
		return fmt.Errorf("func %q - wrong parameter for --%s: %.2f (less than %.2f or equal/greater than %.2f)", funcName, flagPresencePenaltyLong, flagPresencePenalty, minPenaltyLimit, maxPenaltyLimit)
	}
	// frequency penalty
	if flagFrequencyPenalty != 0 && (flagFrequencyPenalty >= minPenaltyLimit && flagFrequencyPenalty < maxPenaltyLimit) {
		frequencyPenalty = flagFrequencyPenalty
	}
	if flagFrequencyPenalty != 0 && (flagFrequencyPenalty < minPenaltyLimit || flagFrequencyPenalty >= maxPenaltyLimit) {
		return fmt.Errorf("func %q - wrong parameter for --%s: %.2f (less than %.2f or equal/greater than %.2f)", funcName, flagFrequencyPenaltyLong, flagFrequencyPenalty, minPenaltyLimit, maxPenaltyLimit)
	}
	// stop sequence
	if flagStop != "" {
		s := flagStop
		// sanitize string
		s = strings.TrimSpace(s)
		s = strings.TrimSuffix(s, ",")
		// check string for valid format
		if !regexCompileCommaSepList.MatchString(s) {
			return fmt.Errorf("func %q - invalid 'stop' sequence or more than %d sequences: %s\n", funcName, maxStopSequences, flagStop)
		}
		stopSequences = strings.SplitN(s, ",", -1)
	}

	// --- format --- //
	if flagResponseFormat != "" {
		s := strings.ToLower(flagResponseFormat)
		switch {
		case s != "text" && s != "json" && s != "text/plain" && s != "application/json":
			return fmt.Errorf("wrong argument for option '--%s': %q", flagResponseFormatLong, flagResponseFormat)
		default:
			if s == "json" || s == "application/json" {
				responseFormat = responseMimeTypeJSON
			}
		}
	}
	if flagResponseFormatSchemaJSON != "" {
		responseFormat = responseMimeTypeJSON
	}

	// tokens count
	if flagCount != "" {
		if !regexCompileCountOption.MatchString(flagCount) {
			return fmt.Errorf("wrong argument for option '-%c' or option '--%s': %q", flagCountShort, flagCountLong, flagCount)
		}
	}

	// --- safety settings --- //
	// safety default for all categories
	if flagSafetyAll != "" {
		// tolerate some mistyping
		s := strings.ToUpper(flagSafetyAll) // tolerate lowercase (convert all chars to uppercase)
		s = strings.ReplaceAll(s, "-", "_") // tolerate '-' (replace '-' char with underscore)
		// check if provided threshold is supported
		if _, ok := thresholds[s]; !ok {
			return fmt.Errorf("wrong THRESHOLD specified in option --%s: %q", flagSafetyAllLong, flagSafetyAll)
		}
		safetyThreshold = flagSafetyAll
		for k := range safety {
			safety[k] = s
		}
	}
	// safety by category
	if flagSafetyDangerousContent != "" {
		// tolerate some mistyping
		s := strings.ToUpper(flagSafetyDangerousContent) // tolerate lowercase (convert all chars to uppercase)
		s = strings.ReplaceAll(s, "-", "_")              // tolerate '-' (replace '-' char with underscore)
		// check if provided threshold is supported
		if _, ok := thresholds[s]; !ok {
			return fmt.Errorf("wrong THRESHOLD specified in option --%s: %q", flagSafetyDangerousContentLong, flagSafetyDangerousContent)
		}
		safety[categoryDangerousContent] = s
	}
	if flagSafetyHateSpeech != "" {
		// tolerate some mistyping
		s := strings.ToUpper(flagSafetyHateSpeech) // tolerate lowercase (convert all chars to uppercase)
		s = strings.ReplaceAll(s, "-", "_")        // tolerate '-' (replace '-' char with underscore)
		// check if provided threshold is supported
		if _, ok := thresholds[s]; !ok {
			return fmt.Errorf("wrong THRESHOLD specified in option --%s: %q", flagSafetyHateSpeechLong, flagSafetyHateSpeech)
		}
		safety[categoryHateSpeech] = s
	}
	if flagSafetyHarassment != "" {
		// tolerate some mistyping
		s := strings.ToUpper(flagSafetyHarassment) // tolerate lowercase (convert all chars to uppercase)
		s = strings.ReplaceAll(s, "-", "_")        // tolerate '-' (replace '-' char with underscore)
		// check if provided threshold is supported
		if _, ok := thresholds[s]; !ok {
			return fmt.Errorf("wrong THRESHOLD specified in option --%s: %q", flagSafetyHarassmentLong, flagSafetyHarassment)
		}
		safety[categoryHarassment] = s
	}
	if flagSafetySexuallyExplicit != "" {
		// tolerate some mistyping
		s := strings.ToUpper(flagSafetySexuallyExplicit) // tolerate lowercase (convert all chars to uppercase)
		s = strings.ReplaceAll(s, "-", "_")              // tolerate '-' (replace '-' char with underscore)
		// check if provided threshold is supported
		if _, ok := thresholds[s]; !ok {
			return fmt.Errorf("wrong THRESHOLD specified in option --%s: %q", flagSafetySexuallyExplicitLong, flagSafetySexuallyExplicit)
		}
		safety[categorySexuallyExplicit] = s
	}
	// --- END safety settings --- //

	/*
		// ------ presets ------ //
		if flagPreset != "" {
			var p presetSettings
			if err := p.set(flagPreset); err != nil {
				return err
			}
		}
	*/

	// ------ web ------ //
	if flagWeb != "" && (flagTestPayload == "" && !flagTestPayloadHttpbin) {
		if selected, err := selectHTML(flagWeb, flagWebSelect); err != nil {
			return err
		} else {
			webPageContent = selected
		}
	}

	// ------ pdf ------ //
	if flagPdf != "" && (flagTestPayload == "" && !flagTestPayloadHttpbin) {
		if flagPdfPages != "" {
			if ok := regexCompilePdfPages.MatchString(flagPdfPages); !ok {
				return fmt.Errorf("func %q - invalid '--%s' syntax: %s\n", funcName, flagPdfPagesLong, flagPdfPages)
			}
		}
		mimeType, err := checkMIME(flagPdf)
		if err != nil {
			return err
		}
		if mimeType != PDF {
			return fmt.Errorf("func %q - wrong MIME type for option %s: file %q, mime %q", funcName, flagPdfLong, flagPdf, mimeType)
		}
		var e Extractor
		e = Pdf{}
		if pdfContent, err = e.ToText(flagPdf); err != nil {
			return err
		}
	}

	// ------ csv ------ //
	if flagCsv != "" && !flagPreview {
		// if csv file does not exist ask permission to create a new one
		var r rune
		_, errFileNotExist := os.Open(flagCsv)
		if newFileCSV = errors.Is(errFileNotExist, fs.ErrNotExist); newFileCSV {
			// ask to confirm new file creation
			fmt.Printf("File %q does not exist. Create new one (%q to quit and exit)? [y/n/q]: ", flagCsv, "q")
			if _, err := fmt.Scanf("%c", &r); err != nil {
				return fmt.Errorf("func %q - error creating CSV file %q: %w", funcName, flagCsv, err)
			}
			switch {
			case r == 'y' || r == 'Y':
				newFileCSV = true
			case r == 'n' || r == 'N':
				flagCsv = ""
			case r == 'q' || r == 'Q':
				return (fmt.Errorf("EXIT"))
			default:
				return fmt.Errorf("wrong option: %c\nexit program", r)
			}
		}
	}

	// ------ save JSON payload config ------ //
	if flagPayloadSave != "" {
		// if json file already exist ask permission to overwrite
		var r rune
		if _, err := os.Open(flagPayloadSave); err == nil {
			// ask to confirm overwrite
			fmt.Printf("File %q already exist. Overwrite (%q to quit and exit)? [y/n/q]: ", flagPayloadSave, "q")
			if _, err := fmt.Scanf("%c", &r); err != nil {
				return fmt.Errorf("func %q - error writing JSON payload file %q: %w", funcName, flagPayloadSave, err)
			}
			switch {
			case r == 'y' || r == 'Y':
			case r == 'n' || r == 'N':
				return (fmt.Errorf("EXIT"))
			case r == 'q' || r == 'Q':
				return (fmt.Errorf("EXIT"))
			default:
				return fmt.Errorf("wrong option: %c\nexit program", r)
			}
		}
	}

	// ------ save TOML config file ------ //
	if flagCfgSave != "" {
		// if toml file already exist ask permission to overwrite
		var r rune
		if _, err := os.Open(flagCfgSave); err == nil {
			// ask to confirm overwrite
			fmt.Printf("File %q already exist. Overwrite (%q to quit and exit)? [y/n/q]: ", flagCfgSave, "q")
			if _, err := fmt.Scanf("%c", &r); err != nil {
				return fmt.Errorf("func %q - error writing TOML config file %q: %w", funcName, flagCfgSave, err)
			}
			switch {
			case r == 'y' || r == 'Y':
			case r == 'n' || r == 'N':
				return (fmt.Errorf("EXIT"))
			case r == 'q' || r == 'Q':
				return (fmt.Errorf("EXIT"))
			default:
				return fmt.Errorf("wrong option: %c\nexit program", r)
			}
		}
	}

	// ------ cache ------ //
	// completion
	if flagCache != "" {
		c := strings.TrimPrefix(flagCache, "cachedContents/")
		cacheContent = cacheContent + c
	}
	// create
	if flagCacheCreate || flagCacheUpdate != "" {
		// cache TTL
		if getopt.GetCount(flagCacheTtlLong) > 0 {
			s := strconv.FormatFloat(flagCacheTtl, 'f', -1, 64)
			if strings.Contains(s, ".") {
				s = strings.TrimRight(s, "0")
				s = strings.TrimRight(s, ".")
			}
			cacheTTL = fmt.Sprintf("%ss", s)
		}
		if flagCacheExpire != "" {
			t, err := time.Parse(time.RFC3339Nano, flagCacheExpire)
			if err != nil {
				return fmt.Errorf("func %q - error parsing timestamp %q: %w", funcName, flagCacheExpire, err)
			}
			cacheExpire = t.Format(time.RFC3339Nano)
		}
		if flagCacheDisplayName != "" {
			if len([]rune(flagCacheDisplayName)) > maxCacheDnameUnicodeChars {
				return fmt.Errorf("func %q - option '--%s' exceed 128 Unicode chars: %q", funcName, flagCacheDisplayNameLong, flagCacheDisplayName)
			}
		}
	}

	// ------ response tokens ------ //
	if getopt.GetCount(flagResTokLong) > 0 || getopt.GetCount(flagResTokShort) > 0 {
		maxTokens = flagResTok
	}

	// api-key name
	if flagEnv != "" {
		apikeyName = flagEnv
	}

	// validate owned dump server address
	if flagTestPayload != "" {
		// check URL syntax
		u, err := url.ParseRequestURI(flagTestPayload)
		if err != nil {
			return fmt.Errorf("func %q - invalid URL %q in option '--%s': %w", funcName, flagTestPayload, flagTestPayloadLong, err)
		}
		// check if URL is absolute
		if !u.IsAbs() {
			return fmt.Errorf("func %q - invalid URL %q in option '--%s'", funcName, flagTestPayload, flagTestPayloadLong)
		}
		// check for valid scheme
		if u.Scheme != "http" && u.Scheme != "https" {
			return fmt.Errorf("func %q - invalid scheme %q in option '--%s': %q", funcName, u.Scheme, flagTestPayloadLong, flagTestPayload)
		}
		// parse hostname
		if u.Host == "" {
			return fmt.Errorf("func %q - invalid hostname in option '--%s': %q", funcName, flagTestPayloadLong, flagTestPayload)
		}
	}

	// check if output filepath did exist
	if flagOutput != "" {
		// extract dir and check if did exists
		var dir string = filepath.Dir(flagOutput)
		if _, err := os.Stat(dir); err != nil {
			return fmt.Errorf("func %q - option '--%s' invalid path name or i/o error: %w", funcName, flagOutputLong, err)
		}
	}

	// Jan 2024 - not used in Gemini API - numberOfResponses (API param 'candidateCount') always = 1
	//if getopt.GetCount(flagResponsesLong) > 0 {
	//numberOfResponses = flagResponses
	//}

	return nil
}

func configRoles() error {
	// function name to return if any error occur
	var funcName string = "configRoles"

	// ROLE: system
	if flagSystem != "" || flagPreset != "" || flagWeb != "" || flagPdf != "" {
		var p part
		var parts []part
		switch {
		case flagTestPayload != "" || flagTestPayloadHttpbin:
			p.Text = fakeSystemPrompt
		case flagPreset != "" && flagSystem == "":
			p.Text = presets[flagPreset].systemMessage
		case flagWeb != "" && flagSystem == "":
			p.Text = systemMessageWeb
		case flagPdf != "" && flagSystem == "":
			p.Text = systemMessagePdf
		default:
			p.Text = strings.ReplaceAll(flagSystem, "\\n", "\n")
		}
		parts = append(parts, p)
		systemContent.Parts = parts
	}

	// ROLE: assistant
	if flagAssistant != "" {
		var parts []part
		// first user PROMPT
		var userPreviousPrompt content
		var previousPrompt part
		previousPrompt.Text = flagPreviousPrompt
		if flagTestPayload != "" || flagTestPayloadHttpbin {
			previousPrompt.Text = fakeUserPrompt
		}
		parts = append(parts, previousPrompt)
		userPreviousPrompt.Role = user
		userPreviousPrompt.Parts = parts
		contents = append(contents, userPreviousPrompt)
		switch {
		case flagFunctionResponse != "":
			// model response to function call
			assistantPrompts := strings.Split(flagAssistant, ",,")
			var AssistantParts []part
			var assistantContent content
			if len(assistantPrompts) > 0 {
				for _, p := range assistantPrompts {
					var assistantPart part
					var assistantResponse functionCall
					if err := json.Unmarshal([]byte(p), &assistantResponse); err != nil {
						return fmt.Errorf("func %q - invalid 'json' data: %s\nerror: %w", funcName, flagAssistantLong, err)
					}
					assistantPart.FunctionCall = &assistantResponse
					if flagTestPayload != "" || flagTestPayloadHttpbin {
						assistantPart.FunctionCall.Name = fakeFunctionCall
						assistantPart.FunctionCall.Args = nil
					}
					AssistantParts = append(AssistantParts, assistantPart)
				}
				assistantContent.Role = assistant
				assistantContent.Parts = AssistantParts
				contents = append(contents, assistantContent)
			}
			// function response from actual function
			assistantResponses := strings.Split(flagFunctionResponse, ",,")
			var funcRespParts []part
			var funcRespContent content
			if len(assistantResponses) > 0 {
				for i, r := range assistantResponses {
					var funcRespPart part
					if flagTestPayload != "" || flagTestPayloadHttpbin {
						r = fmt.Sprintf(fakeFunctionResponse, i, i)
					}
					if err := json.Unmarshal([]byte(r), &funcRespPart.FunctionResponse); err != nil {
						return fmt.Errorf("func %q - invalid 'json' data: %s\nerror: %w", funcName, flagFunctionResponseLong, err)
					}
					funcRespParts = append(funcRespParts, funcRespPart)
				}
			}
			funcRespContent.Role = function
			funcRespContent.Parts = funcRespParts
			contents = append(contents, funcRespContent)
			return nil
		default:
			var assistantContent content
			var p part
			var parts []part
			p.Text = flagAssistant
			if flagTestPayload != "" || flagTestPayloadHttpbin {
				p.Text = fakeAssistantPrompt
			}
			parts = append(parts, p)
			assistantContent.Parts = parts
			assistantContent.Role = assistant
			contents = append(contents, assistantContent)
		}
	}
	// ROLE: user
	if flagEmbed {
		if len(inputs) > 1 {
			return nil
		}
	}
	var userContent content
	userContent.Role = user
	var parts []part

	// media task requested
	if flagMedia != "" {
		if len(mediaArgs) == 0 {
			return fmt.Errorf("func %q - invalid 'media' data: %s", funcName, flagMedia)
		}
		for i, m := range mediaArgs {
			if _, ok := mediaPayload[m]; ok {
				var p part
				var data inlineData
				data.Data = mediaPayload[m].Data
				data.MimeType = mediaPayload[m].MimeType
				if flagTestPayload != "" || flagTestPayloadHttpbin {
					data.Data = fmt.Sprintf("%s_%d", fakeMediaAsset, i+1)
				}
				p.InlineData = &data
				parts = append(parts, p)
				continue
			}
			var p part
			var data fileData
			data.FileUri = m
			if flagTestPayload != "" || flagTestPayloadHttpbin {
				data.FileUri = fmt.Sprintf("URI_%s_%d", fakeMediaAsset, i+1)
			}
			p.FileData = &data
			parts = append(parts, p)
		}
	}

	// normal language task
	if !flagCacheCreate {
		var p part
		p.Text = prompt
		if flagTestPayload != "" || flagTestPayloadHttpbin {
			p.Text = fakeUserPrompt
		}
		parts = append(parts, p)
	}
	userContent.Parts = parts
	contents = append(contents, userContent)

	return nil
}

func userPromptExceptions() error {
	// function name to return if any error occur
	var funcName string = "userPromptExceptions"

	var b strings.Builder
	var writeErr []error = make([]error, 0)
	switch {
	case flagWebTest:
		prompt = webPageContent
		return nil
	case flagPdfTest:
		prompt = pdfContent
		return nil
	case flagPreset != "" && flagWeb != "":
		_, err := b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(webPageContent)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		prompt = b.String()
	case flagPreset != "" && flagPdf != "":
		_, err := b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(pdfContent)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		prompt = b.String()
	case flagPreset != "":
		_, err := b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(prompt)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		prompt = b.String()
	case flagWeb != "":
		_, err := b.WriteString(delimiterWeb)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(webPageContent)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiterWeb)
		writeErr = append(writeErr, err)
		_, err = b.WriteString("\n\n")
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(prompt)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		prompt = b.String()
	case flagPdf != "":
		_, err := b.WriteString(delimiterPdf)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(pdfContent)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiterPdf)
		writeErr = append(writeErr, err)
		_, err = b.WriteString("\n\n")
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(prompt)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		prompt = b.String()
	}
	if len(writeErr) > 0 {
		for _, e := range writeErr {
			if e != nil {
				return fmt.Errorf("func %q - error composing web prompt: %w", funcName, e)
			}
		}
	}
	return nil
}

func composeEndpoint() (err error) {
	switch {
	case flagCacheCreate:
		endpoint = urlCache + "?" + keyKey
	case flagCacheInfo != "":
		c := strings.TrimPrefix(flagCacheInfo, "cachedContents/")
		endpoint = urlCache + "/" + c + "?" + keyKey
	case flagCacheUpdate != "":
		c := strings.TrimPrefix(flagCacheUpdate, "cachedContents/")
		endpoint = urlCache + "/" + c + "?" + keyKey
	case flagCacheDelete != "":
		c := strings.TrimPrefix(flagCacheDelete, "cachedContents/")
		endpoint = urlCache + "/" + c + "?" + keyKey
	case flagCacheList:
		endpoint = urlCache + "?"
		if getopt.GetCount(flagCacheListSizeLong) > 0 {
			endpoint = fmt.Sprintf("%spageSize=%d&", endpoint, flagCacheListSize)
		}
		if flagCacheListToken != "" {
			endpoint = endpoint + "pageToken=" + flagCacheListToken + "&"
		}
		endpoint = endpoint + keyKey
	case flagFileUpload != "":
		endpoint = urlMediaUpload + "?" + keyKey
		mediaUploadData, err = os.ReadFile(flagFileUpload)
		if err != nil {
			return fmt.Errorf("error reading media file: %w", err)
		}
	case flagFileUploadInfo != "":
		f := strings.TrimPrefix(flagFileUploadInfo, "files/")
		endpoint = urlMediaMetadata + "/" + f + "?" + keyKey
	case flagFileUploadDelete != "":
		f := strings.TrimPrefix(flagFileUploadDelete, "files/")
		endpoint = urlMediaMetadata + "/" + f + "?" + keyKey
	case flagFileUploadList:
		endpoint = urlMediaMetadata + "?"
		if getopt.GetCount(flagFileUploadListSizeLong) > 0 {
			endpoint = fmt.Sprintf("%spageSize=%d&", endpoint, flagFileUploadListSize)
		}
		if flagFileUploadListToken != "" {
			endpoint = endpoint + "pageToken=" + flagFileUploadListToken + "&"
		}
		endpoint = endpoint + keyKey
	case flagListModels:
		endpoint = strings.TrimSuffix(baseUrl, "/") + "?" + keyKey
	case flagModelInfo:
		endpoint = baseUrl + model + "?" + keyKey
	default:
		endpoint = baseUrl + model + method + keyKey
	}
	return nil
}

// configFromFile load a basic configuration from TOML file
func configFromFile(filename string) error {
	// function name to return if any error occur
	var funcName string = "configFromFile"

	// load permitted keys
	cfgKeys := strings.Split(tomlKeys, ",")

	// check if config file exist
	f, err := os.Open(filepath.Clean(filename))
	if err != nil {
		return fmt.Errorf("func %q - error opening config file %q: %w", funcName, filename, err)
	}
	if err := f.Close(); err != nil {
		return err
	}

	// decode to struct for strict type checking
	var cfg tomlConfig
	_, err = toml.DecodeFile(filename, &cfg)
	if err != nil {
		return fmt.Errorf("func %q - error decoding config file %q: %w", funcName, filename, err)
	}

	// decode to map to simplify key/value mapping
	_, err = toml.DecodeFile(filename, &tomlMap)
	if err != nil {
		return fmt.Errorf("func %q - error mapping config file %q: %w", funcName, filename, err)
	}

	// extract only valid keys
	for _, k := range cfgKeys {
		if _, ok := tomlMap[k]; ok {
			cfgMap[k] = tomlMap[k]
		}
	}

	// --- apply configs --- //
	// setting flags because the various controls are already implemented in "configFlags()" code
	if _, ok := cfgMap["model"]; ok && flagModel == "" {
		flagModel = cfg.Model
	}
	if _, ok := cfgMap["stream"]; ok && !flagStream {
		flagStream = cfg.Stream
	}
	if _, ok := cfgMap["retries"]; ok {
		retries = cfg.Retries
	}
	if _, ok := cfgMap["retries_wait"]; ok {
		retriesWait = cfg.RetriesWait
	}
	if _, ok := cfgMap["timeout"]; ok {
		timeout = time.Second * time.Duration(cfg.Timeout)
	}
	if _, ok := cfgMap["timeout_chunk"]; ok {
		timeoutChunk = time.Second * time.Duration(cfg.TimeoutChunk)
	}
	if _, ok := cfgMap["citations"]; ok && !flagCitations {
		flagCitations = cfg.Citations
	}
	if _, ok := cfgMap["finishReason"]; ok && !flagFinishReason {
		flagFinishReason = cfg.FinishReason
	}
	if _, ok := cfgMap["format"]; ok && flagResponseFormat == "" {
		flagResponseFormat = cfg.Format
	}
	if _, ok := cfgMap["function_call_mode"]; ok && flagFunctionCall == "" {
		flagFunctionCall = cfg.FunctionCall
	}
	if _, ok := cfgMap["response_tokens"]; ok {
		maxTokens = cfg.ResponseTokens
	}
	if _, ok := cfgMap["safety_all_categories"]; ok && flagSafetyAll == "" {
		flagSafetyAll = cfg.SafetyAll
	}
	if _, ok := cfgMap["stop"]; ok && flagStop == "" {
		flagStop = cfg.Stop
	}
	if _, ok := cfgMap["system"]; ok && (flagSystem == "") {
		flagSystem = cfg.System
	}
	if _, ok := cfgMap["temperature"]; ok && (getopt.GetCount(flagTemperatureLong) == 0 || getopt.GetCount(flagTemperatureShort) == 0) {
		if cfg.Temperature < minLimitDefault || cfg.Temperature > maxTemperatureLimit {
			return fmt.Errorf("func %q - wrong config for %q: %.2f (less than %.2f or greater than %.2f - max is model dependent)", funcName, "temperature", cfg.Temperature, minLimitDefault, maxTemperatureLimit)
		}
		temperature = cfg.Temperature
	}
	if _, ok := cfgMap["top_P"]; ok && getopt.GetCount(flagTopPLong) == 0 {
		if cfg.TopP < minLimitDefault || cfg.TopP > maxTopPLimit {
			return fmt.Errorf("func %q - wrong config for %q: %.2f (less than %.2f or greater than %.2f)", funcName, "top_P", cfg.TopP, minLimitDefault, maxTopPLimit)
		}
		top_p = cfg.TopP
	}
	if _, ok := cfgMap["top_K"]; ok && getopt.GetCount(flagTopKLong) == 0 {
		if cfg.TopK < minTopKLimit {
			return fmt.Errorf("func %q - wrong config for %q: %d (less than %d)", funcName, "top_K", cfg.TopK, minTopKLimit)
		}
		top_k = cfg.TopK
	}
	if _, ok := cfgMap["presence_penalty"]; ok && getopt.GetCount(flagPresencePenaltyLong) == 0 {
		if cfg.PresencePenalty < minPenaltyLimit || cfg.PresencePenalty >= maxPenaltyLimit {
			return fmt.Errorf("func %q - wrong config for %q: %.2f (less than %.2f or equal/greater than %.2f)", funcName, "presence_penalty", cfg.PresencePenalty, minPenaltyLimit, maxPenaltyLimit)
		}
		presencePenalty = cfg.PresencePenalty
	}
	if _, ok := cfgMap["frequency_penalty"]; ok && getopt.GetCount(flagFrequencyPenaltyLong) == 0 {
		if cfg.FrequencyPenalty < minPenaltyLimit || cfg.FrequencyPenalty >= maxPenaltyLimit {
			return fmt.Errorf("func %q - wrong config for %q: %.2f (less than %.2f or equal/greater than %.2f)", funcName, "frequency_penalty", cfg.FrequencyPenalty, minPenaltyLimit, maxPenaltyLimit)
		}
		frequencyPenalty = cfg.FrequencyPenalty
	}
	if _, ok := cfgMap["output_file"]; ok && flagOutput == "" {
		flagOutput = cfg.OutputFile
	}
	if _, ok := cfgMap["no_console"]; ok && !flagNoConsole {
		flagNoConsole = cfg.NoConsole
	}

	return nil
}

// configToFile save a basic configuration to TOML file
func configToFile(filename string) error {
	// function name to return if any error occur
	var funcName string = "configToFile"

	f, err := os.OpenFile(filepath.Clean(filename), os.O_RDWR|os.O_TRUNC|os.O_CREATE, 0600)
	if err != nil {
		return fmt.Errorf("func %q - error opening configuration file %q: %w", funcName, filename, err)
	}
	defer f.Close()

	var cfg tomlConfig
	cfg.Citations = flagCitations
	cfg.FinishReason = flagFinishReason
	if flagResponseFormat == "" {
		flagResponseFormat = "text"
	}
	cfg.Format = flagResponseFormat
	if flagFunctionCall == "" {
		cfg.DashFCall = string(fakeRune)
	}
	cfg.FunctionCall = functionCallMode
	cfg.Model = model
	cfg.ResponseTokens = maxTokens
	cfg.Retries = retries
	cfg.RetriesWait = retriesWait
	cfg.SafetyAll = safetyThreshold
	if flagStop == "" {
		cfg.DashStop = string(fakeRune)
	}
	cfg.Stop = flagStop
	cfg.Stream = flagStream
	switch {
	case len(systemContent.Parts) > 0:
		cfg.System = systemContent.Parts[0].Text
	default:
		cfg.System = ""
		cfg.DashSystem = string(fakeRune)
	}
	cfg.Temperature = temperature
	cfg.TopK = top_k
	cfg.TopP = top_p
	cfg.PresencePenalty = presencePenalty
	cfg.FrequencyPenalty = frequencyPenalty
	cfg.Timeout = int(timeout) / nanosecToSec
	cfg.TimeoutChunk = int(timeoutChunk) / nanosecToSec
	if flagOutput == "" {
		cfg.DashOutFile = string(fakeRune)
	}
	cfg.OutputFile = flagOutput
	if !flagNoConsole {
		cfg.DashNoConsole = string(fakeRune)
	}
	cfg.NoConsole = flagNoConsole

	t := template.Must(template.New("config").Parse(configTemplate))
	if err := t.Execute(f, cfg); err != nil {
		return fmt.Errorf("func %q - error writing configuration to file %q: %w", funcName, filename, err)
	}

	return nil
}
