/*  'geminicli' Terminal Chat Completion client for Google's Gemini AI models
    Copyright (C) 2024 gimlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

const helpMsgUsage = "Usage: %s [OPTIONS] \"PROMPT\"\n"

const helpMsgBody = `
Terminal Chat Completion client for Google's Gemini AI models

Defaults:
  model: gemini-1.5-flash-latest
  model embeddings: text-embedding-004
  model cached content: gemini-1.5-flash-001
  model json schema: gemini-1.5-pro
  function call if functions are present: auto
  code exec: false
  temperature: 0.0
  top_p: 0.95
  top_k: 40
  presence penalty: 0.0
  frequency penalty: 0.0
  safety threshold: all categories BLOCK_MEDIUM_AND_ABOVE
  max response tokens: 1000
  max text response tokens for media: 300
  number of responses: 1 (from API docs 'This value must be 1')
  response format: text
  stream response mode: false
  thinking message: no supported in API
  connection timeout: 120 sec
  connection timeout stream mode (chunk, total): 5 sec, 180 sec
  number of retries: 0
  wait before retry: 10 sec
  embeddings csv filename: data.csv
  web HTML element selection: main/body
  PDF pages selection: all
  cache TTL: 300 sec
  api-key name: GOOGLE_API_KEY

Order matter:
  first OPTIONS
  then PROMPT

Notes:
  . PROMPT must always be enclosed in "double" quotes.

Online Documentation:
  <https://gitlab.com/ai-gimlab/geminicli#geminicli---overview>

OPTIONS:
Global:
      --defaults                  prints the program default values and exit
  -l, --list-models               lists names of all available models and exit
  -m, --model=MODEL               select model
                                  use '--list-models' to lists all available models
      --model-info                return information about a given model
                                  can be used in conjunction with the '--model' option
  -p, --preview                   preview request payload and exit
      --response-raw              prints full response body raw
      --response-json             prints full response body json formatted
      --help                      prints this help
      --version                   output version information and exit

Network:
      --retries=RETRIES           number of connection retries if network timeout occur
                                  use the '--retries-wait' option to insert a pause between retries
      --retries-wait=SECONDS      in conjunction with '--retries RETRIES' option insert a pause between retries
      --timeout=SECONDS           network connection timeout, in seconds
                                  apply to a complete request/response session
                                  SECONDS = 0 means no timeout
      --timeout-chunk=SECONDS     network connection timeout, in seconds
                                  used only in stream mode
			          apply to every streamed chunk of response

API Auth:                         ref: <https://gitlab.com/ai-gimlab/geminicli#how-to-get-and-use-google-api-key>
      --env=NAME                  uses NAME as environment variable name instead of 'GOOGLE_API_KEY'
                                  such as 'GOOGLE_SEARCH_API_KEY', 'PROJECT_1269', etc...
  -f, --file=APIKEY_FILE          NOTE: DEPRECATED - use option '-k, --key-file=FILENAME' instead
  -k, --key-file=FILENAME         file containing the Google api key (GOOGLE_API_KEY=your_apikey)

Chat/Completion/Media API:        ref: <https://ai.google.dev/api/rest/v1beta/models/generateContent>
  -a, --assistant="PROMPT"        [role]: 'assistant' (model) message PROMPT
                                  must be used with '--previous-prompt' option
				  can be used together with the '--function-response' option:
				   - function call responses must be passed as JSON objects, double comma separated
				     eg: --assistant '{JSON_FUNC_CALL_RESP_0},,{JSON_FUNC_CALL_RESP_1},,{JSON_FUNC_CALL_RESP_N}'
			           - use '--function-examples' to view how to compose function call json object
  -c, --count=SELECT              prints how many tokens have been used
                                  can also prints the word count of the AI response
                                  SELECT can be 'in', 'out', 'cache', 'total' (in + out) or 'words'
			          can also be any comma separated combination of above,
			          without any space, without trailing comma:
 			            in,out
				    in,cache,total
			            in,out,total,words
			            etc...
      --cache=NAME                execute completion task using cached content NAME
                                  NAME is the key '"name":' from '--cache-create' response
				  eg: "name": "cachedContents/abcde12345fg"
				  NAME can be with or without "cachedContents/" prefix
      --cfg=TOML_CFG_FILE         load a basic configuration from TOML_CFG_FILE
                                  take precedence over defaults
                                  command line options take precedence over TOML_CFG_FILE configurations
      --cfg-save=TOML_CFG_FILE    save a basic configuration to TOML_CFG_FILE using task session values
                                  can be used with completion tasks or preview mode ('--preview' option)
      --citations                 prints source attribution of the generated content (if any)
			          no stream mode
      --code-exec                 the model generate and run PROMPT required Python code
                                  it run Python code until it arrives at a final output
				  ref: <https://ai.google.dev/gemini-api/docs/code-execution>
      --csv=CSV_FILE              export request/response data, csv formatted, to CSV_FILE
			          no stream mode
                                   - if CSV_FILE does not exist: ask permission to create a new one
			           - if CSV_FILE exist: append data
      --finish-reason             prints the reason why the model stopped generating tokens
      --format=FORMAT             the format/mimetype that the model must output (Gemini 1.5 Flash/Pro and newer)
				  FORMAT can be either:
				   - 'text/plain' or simply 'text' (default)
				   - 'application/json' or simply 'json'
      --format-examples           prints examples of 'JSON SCHEMA format' request and exit
      --format-jschema={SCHEMA}   JSON schema for JSON formatted response (Gemini 1.5 Pro and newer)
				  SCHEMA ref: <https://ai.google.dev/api/rest/v1beta/cachedContents#schema>
				  supported models: <https://ai.google.dev/gemini-api/docs/api-overview#json>
                                  '--format=FORMAT' option is automatically set to 'application/json'
			          use '--format-examples' to view how to compose a SCHEMA object
      --fp=VALUE                  frequency penalty: VALUE range from -2.0 up to, but not including, 2.0
                                  positive values decrease the probability of repeating content
      --function='{JSON}'         json object containing function definition
			          must be enclosed in single quotes
			          stream mode available only if used with option '--function-response={JSON}'
				  support multiple functions json object definitions, separated by double commas
				    eg: '{JSON_FUNC_1},,{JSON_FUNC_2},,{JSON_FUNC_N}'
			          use '--function-examples' to view how to compose function definition json object
				  ref: <https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#functions>
      --function-allowed=NAMES    model is forced to call a function from the list of function NAMES
                                  NAMES is a comma separated list of function declaration names, enclosed in single/double quotes
				    eg: "FNAME_A,FNAME_B,FNAME_X"
				  NOTE: function call MODE is automatically set to required/any
      --function-call="MODE"      how the model responds to function calls
                                  MODE can be:
			           - "auto" (model choose if calling function)
		         	   - "none" (model does not call a function)
				   - "required" or "any" (model is forced to call one or more functions)
      --function-examples         prints some examples of function json object and exit
      --function-response={JSON}  json object containing results from application's actual function
                                  must be used with options:
				   --assistant={FUNC_CALL_JSON_RESP_0,,FUNC_CALL_JSON_RESP_1,,FUNC_CALL_JSON_RESP_N}
				   --previouse-prompt=PROMPT
				   --function={FUNC_DEF_0,,FUNC_DEF_1,,FUNC_DEF_N}
			          use '--function-examples' to view how to compose function response json object
                                  ref: <https://ai.google.dev/gemini-api/docs/function-calling#expandable-7>
      --jrequest-run=JSON_FILE    run request payload from JSON_FILE
				  can be used only with options '--model=MODEL' and '--stream'
                                   - take precedence over the default configurations
				     and those defined in the TOML_CFG_FILE file,
				     except the 'model' and 'stream' mode
      --jrequest-save=JSON_FILE   save request payload to JSON_FILE using a task session values
      --list-presets              lists all available predefined tasks and exit
      --logprobs                  return log probabilities of the response choosed tokens, in tabular format
                                  is equivalent to '--top-logprobs=0'
                                   - in conjunction with the '--response-json' or '--response-raw' options
				     logprobs are included in the response, json formatted
      --media=FILENAME            answer questions about FILENAME
                                  FILENAME can be any text, image, audio or video file
				  FILENAME can be a local file and/or a FileAPI URI
				  for multiple FILENAMEs enclose comma separated filenames/uri in double quotes,
				  without any space, without trailing comma, such as:
				    "img/img1.jpeg,https://generativelanguage.googleapis.com/v1beta/files/abc-123,audio.mp3,vid.mp4"
				  NOTE:
				    for payloads greater than 20 MB the media files must be first uploaded with 'File API'
				    ref: <https://ai.google.dev/gemini-api/docs/vision?lang=node#upload-image>
      --model-used                which model was used at endpoint side
      --no-console                suppresses console output
                                  must be used in conjunction with '--output=FILENAME' option
			          no stream mode
  -o, --output=FILENAME           save response to FILENAME
      --output-full               save full chat system/user/assistant to FILENAME, adding headings. Eg.:
                                    **SYSTEM**: you are helpful assistant
				    **USER**: Write a poem
				    **ASSISTANT**: No more, I quit!
                                  must be used in conjunction with '--output=FILENAME' option
      --pdf=PDF_FILENAME          text only extraction from local PDF_FILENAME
			          in user PROMPT ask only for the context of the extracted text. Eg.:
			           - WRONG: geminicli --pdf document.pdf "From the following document {QUESTION}"
			           - OK:    geminicli --pdf document.pdf "How do I completely factory reset my phone?"
				  if '--pdf-pages' option not provided, extracts all document's text
				  Note: full PDF documents, text + images, can also be used as is with '--media FILENAME' option
      --pdf-pages=PAGES           'physical pages' of PDF document from which to extract text
                                  PAGES can be any of [page_number], [page_number,page_number], [page_number-page_number]
				  examples:
				  - 5 (only page 5)
				  - 1,5,8 (pages 1, 5 and 8)
				  - 14-18 (range of pages from page 14 to page 18)
				  - 1,5,8,14-18,36,50-100
				    (pages 1, 5, 8, 36 and ranges from page 14 to page 18, from page 50 to page 100)
				  must be used with option '--pdf=PDF_FILENAME'
      --pdf-pagenumber            prints a footer with 'physical page numbers'
                                  must be used in conjunction with the '--pdf-test' option
      --pdf-test                  prints an output of text extracted from PDF file and exit
      --pdf-system                prints default system message for pdf tasks
      --pp=VALUE                  presence penalty: VALUE range from -2.0 up to, but not including, 2.0
                                  positive values increase the probability of generating more diverse content
      --preset=PRESET_NAME        predefined tasks, such as summarization, sentiment analisys, etc...
                                  use '--list-presets' to lists all available PRESET_NAMEs and their purpose
				  NOTE: preset configuration take precedence over TOML configuration file
      --preset-system             prints PRESET_NAME predefined system message and exit
      --previous-prompt=PROMPT    in conjunction with the '--assistant' option simulates a single round of chat
  -r, --response-tokens=TOKENS    maximun number of response tokens
				  ref: <https://ai.google.dev/models/gemini> for models context size
      --safety-all=THRESHOLD      set the same safety threshold THRESHOLD for all categories
				  can be used together with the options '--safety-{CATEGORY}'
				  option '--safety-{CATEGORY}' overwrite configuration for the given category
                                  THRESHOLD can be any of:
				   - BLOCK_NONE
				   - BLOCK_LOW_AND_ABOVE
				   - BLOCK_MEDIUM_AND_ABOVE (default)
				   - BLOCK_ONLY_HIGH
				  ref.: <https://cloud.google.com/vertex-ai/docs/generative-ai/multimodal/configure-safety-attributes>
      --safety-dcont=THRESHOLD    category: HARM_CATEGORY_DANGEROUS_CONTENT
                                  for THRESHOLDs refer to '--safety-all' option
      --safety-harass=THRESHOLD   category: HARM_CATEGORY_HARASSMENT
                                  for THRESHOLDs refer to '--safety-all' option
      --safety-hate=THRESHOLD     category: HARM_CATEGORY_HATE_SPEECH
                                  for THRESHOLDs refer to '--safety-all' option
      --safety-sex=THRESHOLD      category: HARM_CATEGORY_SEXUALLY_EXPLICIT
                                  for THRESHOLDs refer to '--safety-all' option
      --stop="STOP,..."           comma separated list of stop sequences, up to 5 sequences,
			          without trailing comma, enclosed in single or double quotes
			            eg: "STOP1,STOP2,..."
      --stream		          start printing completion before the full completion is finished
  -s, --system="PROMPT"           system message PROMPT (Gemini 1.5 and newer)
  -t, --temperature=VALUE         VALUE range from 0.0 to 2.0 (the maximum value is model dependent)
      --test=URL                  inspect requests using an owned web debug server on URL
                                  URL must be 'absolute'
                                  NOTE: does not send sensitive data
      --test-httpbin              inspect requests using httpbin.org as web debug server
                                  NOTE: does not send sensitive data
      --top-k=VALUE               top_k: VALUE range from 1 to (the maximum value is model dependent)
      --top-logprobs=NUMBER       number of top candidates tokens to return
                                  NUMBER is an integer between 0 and 5
				  '--top-logprobs=0' equivalent to '--logprobs' option
                                   - in conjunction with the '--response-json' or '--response-raw' options
				     top logprobs are included in the response, json formatted
      --top-p=VALUE               top_p: VALUE range from 0.0 to 1.0
      --web=URL                   any web page URL (dynamic web pages are not supported)
			          URL must always be encloded in single or double quotes
			          in user PROMPT ask only for the context of the page. Eg.:
			           - WRONG: geminicli --web "https://www.example.com" "From the following web page {QUESTION}"
			           - OK:    geminicli --web "https://www.example.com" "Extract a list of cat names"
			          used in conjunction with option '--preset', URL content becomes the user PROMPT. Eg:
			           - geminicli --preset ner --web "https://www.example.com"
			             execute NER preset using content from "https://www.example.com" web page
      --web-select=SELECTOR       select a section of web page, then strip html tags
                                  SELECTOR can be any HTML element, such as 'h2', 'p', 'body', 'article', etc...
                                  SELECTOR can also be any class or id attribute (eg.: '.myClass', '#myID')
			          lastly SELECTOR can be 'NONE' keyword: no html tag stripping
			           - Note: NONE can use a lot of tokens
			          SELECTOR must always be encloded in single or double quotes
			          default selectors are 'main', if exist, or 'body'
      --web-test                  prints an output of text extracted from web page and exit
                                  useful to find the right HTML selector (check --web-select option)
			          with the right SELECTOR less tokens are used
      --web-system                prints default system message for web tasks

Embeddings API:                   ref: <https://ai.google.dev/api/rest/v1beta/models/embedContent>
  -e, --embed                     return PROMPT embedding
                                  PROMPT must always be enclosed in "double" quotes
				  return a cvs table with columns 'id, text, embedding'
				  append table to file 'data.csv' or create new one
				   - to save in a different file use '--csv FILENAME' option
				   - in conjunction with the '--response-json' or '--response-raw' options
				     response is not saved to file, instead output json data to stdout
				  BATCH: multiple PROMPTs can be sent in batch
				   - multiple PROMPTs must be separated by double commas
				     eg: geminicli --embed "PROMPT_1,,PROMPT_2,,...,,PROMPT_N"
      --embed-dimensions=VALUE    change embedding vector size to VALUE dimensions
                                  must be used in conjunction with '--embed' option

Cache API:                        ref: <https://ai.google.dev/api/caching>
  CREATE/UPDATE/DELETE CACHE:     ref: <https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#caching>
      --cache-create              creates cached resource
                                  must be used in conjunction with '--media=FILENAME' option
      --cache-delete=NAME         deletes cached resource
                                  NAME is the key '"name":' from '--cache-create' response
				  eg: "name": "cachedContents/abcde12345fg"
				  NAME can be with or without "cachedContents/" prefix
      --cache-dname=DNAME         a human-readable display name for a given cache
                                  must be used with '--cache-create' option
				  max DNAME length: 128 Unicode characters
      --cache-expire=TIMESTAMP    when cache is considered expired
                                  TIMESTAMP must be in RFC3339 UTC "Zulu" format,
				  with nanosecond resolution and up to nine fractional digits
				  eg.: "2024-08-18T15:01:23Z" and "2024-08-18T15:01:23.045123456Z"
				  TIMESTAMP must always be enclosed in "double" quotes
                                  must be used in conjunction with '--cache-create' or '--cache-update' options
				  it is mutually exclusive with '--cache-ttl=SECONDS' option
      --cache-ttl=SECONDS         cache duration in SECONDS with up to nine fractional digits, such as "3.5" (3.5 seconds)
                                  must be used in conjunction with '--cache-create' or '--cache-update' options
				  it is mutually exclusive with '--cache-expire=TIMESTAMP' option
      --cache-update=NAME         updates cached resource NAME (only expiration)
                                  must be used with either '--cache-ttl=SECONDS' or '--cache-expire=TIMESTAMP'
  INFO:                           ref: <https://ai.google.dev/api/rest/v1beta/files/list>
      --cache-info=NAME           show cache NAME information
                                  NAME is the key '"name":' from '--cache-create' response
				  eg: "name": "cachedContents/abcde12345fg"
				  NAME can be with or without "cachedContents/" prefix
      --cache-list                lists cached contents
      --cache-listsize=SIZE       number of cached contents to return per page with '--cache-list' option
                                  must be used with '--cache-list' option
      --cache-listtoken=TOKEN     page token from a previous '--cache-list --cache-listsize=SIZE' call
                                  TOKEN is value of '"nextPageToken":' key from previous '--cache-list --cache-listsize=SIZE' call
                                  must be used with '--cache-list' option

File API:                         ref: <https://ai.google.dev/gemini-api/docs/prompting_with_media>
  UPLOAD/DELETE FILE:             ref: <https://ai.google.dev/api/rest/v1beta/media/upload>
      --fdelete=NAME              delete an uploaded file
                                  NAME is the key '"name":' from file upload/list/info response
				  eg: "name": "files/123-abc-vwxyz"
				  NAME can be with or without "files/" prefix
      --fdname=DNAME              a human-readable display name for uploaded FILENAME
                                  must be used with '--fupload=FILENAME' option
      --fname=NAME                uploaded FILENAME resource identifier name
                                  must be used with '--fupload=FILENAME' option
                                  if not used a unique NAME will be generated
      --fupload=FILENAME          upload FILENAME with 'File API'

  INFO:                           ref: <https://ai.google.dev/api/rest/v1beta/files/list>
      --finfo=NAME                gets the metadata for the given NAME
                                  NAME is the key '"name":' from file upload response
				  eg: "name": "files/123-abc-vwxyz"
				  NAME can be with or without "files/" prefix
      --flist                     lists uploaded files
      --flist-size=SIZE           number of files to return per page with '--flist' option
                                  must be used with '--flist' option
      --flist-token=TOKEN         page token from a previous '--flist --flist-size=SIZE' call
                                  TOKEN is value of '"nextPageToken":' key from previous '--flist --flist-size=SIZE' call
                                  must be used with '--flist' option
`

const helpMsgTips = "  💡                              %s --count in,out,total \"Summarize the following text: {TEXT}\"\n                                  geminicli --jrequest-run PAYLOAD_FILE.json\n"

// befor this var, always print cmd name, without newline char (os.Args[0]). eg: fmt.Fprintf(os.Stdout, "%s", os.Args[0]); fmt.Println(verMsg)
const author = "(gimlab)"
const license = `Copyright (C) 2025 gimlab.
License AGPL-3.0: GNU Affero General Public License version 3 or later <https://www.gnu.org/licenses/agpl-3.0.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.`

const jsonPayloadExamples = `[FUNCTION DEFINITION JSON OBJECTS EXAMPLES]
option: '--function={JSON}'

// --- one line JSON FUNCTION DEFINITION --- //
{"name":"get_current_weather","description":"Get the current weather","parameters":{"type":"object","properties":{"location":{"type":"string","description":"The city and state, e.g. San Francisco, CA"},"unit":{"type":"string","format":"enum","enum":["celsius","fahrenheit"],"description":"The temperature unit to use. Infer this from the users location."}},"required":["location","unit"]}}

// --- pretty print JSON FUNCTION DEFINITION --- //
{
  "name": "get_current_weather",
  "description": "Get the current weather",
  "parameters": {
    "type": "object",
    "properties": {
      "location": {
        "type": "string",
        "description": "The city and state, e.g. San Francisco, CA"
      },
      "unit": {
        "type": "string",
        "format": "enum",
        "enum": [
          "celsius",
          "fahrenheit"
        ],
        "description": "The temperature unit to use. Infer this from the users location."
      }
    },
    "required": [
      "location",
      "unit"
    ]
  }
}

[FUNCTION RESPONSE ASSISTANT JSON OBJECTS EXAMPLE]
option: '--assistant={JSON}'

// --- JSON FUNCTION RESPONSE ASSISTANT --- //
{
  "name": "get_current_weather",
  "args": {
    "unit": "celsius",
    "location": "Milan, Italy"
  }
}

[FUNCTION RESPONSE JSON OBJECTS EXAMPLE]
option: '--function-response={JSON}'

// --- JSON FUNCTION RESPONSE --- //
{
  "name": "get_current_weather",
  "response": {
    "name": "get_current_weather",
    "content": {
      "location": "Milan, Italy",
      "temperature": "22",
      "unit": "celsius"
    }
  }
}

// --- tips --- //
[PRESET 'functionJSchema']
Preset 'functionJSchema' can be used to help create a JSON function call definition.
The prompt should be clear, specific, and outline clear goals.
If you’re working with numbers, be sure to specify the data type.

The following is an example of a good prompt:

<PROMPT_EXAMPLE>
"I need a JSON object for a function.
Function name is "helpdesk_request", description "send a support request email to a specific HelpDesk department".

This function takes 3 arguments:

1. arg name "email", description "email address for specific helpdesk departments, you must choose the right email address based on user request. For booking request output 'Date:' and 'Time:' or 'From:'/'To:' fields", permitted values "network@helpdesk.local", "hardware@helpdesk.local", "email@helpdesk.local", "booking@helpdesk.local", "other@helpdesk.local".
2. arg name "subject", description "short email subject, based on user request".
3. arg name "body", description "a bulleted list of user requests".

All arguments are mandatory."
</PROMPT_EXAMPLE>

Command: exec preset 'functionJSchema' and save output to file 'fcall.json'
geminicli --preset functionJSchema --output fcall.json PROMPT_EXAMPLE_HERE

Result:
<JSON>
{
  "name": "helpdesk_request",
  "description": "send a support request email to a specific HelpDesk department",
  "parameters": {
    "type": "OBJECT",
    "properties": {
      "email": {
        "type": "STRING",
        "description": "email address for specific helpdesk departments, you must choose the right email address based on user request. For booking request output 'Date:' and 'Time:' or 'From:'/'To:' fields",
        "enum": [
          "network@helpdesk.local",
          "hardware@helpdesk.local",
          "email@helpdesk.local",
          "booking@helpdesk.local",
          "other@helpdesk.local"
        ]
      },
      "subject": {
        "type": "STRING",
        "description": "short email subject, based on user request"
      },
      "body": {
        "type": "STRING",
        "description": "a bulleted list of user requests"
      }
    },
    "required": [
      "email",
      "subject",
      "body"
    ]
  }
}
</JSON>

Then:
geminicli --function '{JSON_HERE}' USER_PROMPT

[LINUX]
If you are using a Linux terminal with Bash, follow these steps:

1. create JSON file, eg. function.json, with your data, formatted as the examples above.
2. create a var to hold json data. Eg. if var name is "FUNC" and filename is "function.json": 
   export FUNC=$(cat function.json)
3. use your var with command:
   geminicli --function "$FUNC" "PROMPT"

If you prefer, create json file, as in step 1, and skip steps 2, 3 and 4. Then:
   geminicli --function "$(cat function.json)" "PROMPT"

Whichever method you choose, always remember the double quotes:
"$FUNC", "$(cat function.json)", "PROMPT", etc...

[WINDOWS]
If you are using Windows with CMD shell, follow these steps:

1. create JSON file, eg. function.json, with your data, formatted as the examples above.
2. all double quotes in your json data must be escaped with backslash (\). Eg:

{\"name\":\"get_current_weather\",\"description\":\"Get the current weather\",\"parameters\":{\"type\":\"object\",\"properties\":{\"location\":{\"type\":\"string\",\"description\":\"The city and state, e.g. San Francisco, CA\"},\"unit\":{\"type\":\"string\",\"format\":\"enum\",\"enum\":[\"celsius\",\"fahrenheit\"],\"description\":\"The temperature unit to use. Infer this from the users location.\"}},\"required\":[\"location\",\"unit\"]}}

3. create a var to hold json data. Eg. if var name is "FUNC" and filename is "function.json": 
   FOR /F "tokens=*" %g IN ('type function.json') do (SET FUNC=%g)
4. use your var with command:
   geminicli.exe --function "%FUNC%" "PROMPT"

Whichever method you choose, always remember the single or double quotes:
"tokens=*", 'type function.json', "%FUNC%" "PROMPT", etc...

// --- notes --- //
- The same tips can be applied for all PROMPTs (user, system, etc...).
- For more advanced functions usage, such as parallel function calling, see the online documentation:
  https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#functions
  and
  https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#parallel-function-calling
`

const jsonSchemaExample = `[EXAMPLE PROMPT]
"Return the current population of Rome, Milan, Florence, Venice, Bologna"

[JSON SCHEMA REQUESTED]
{
  "type": "array",
  "items": {
    "description": "The current population of the given geographic entities",
    "type": "object",
    "properties": {
      "name": {
        "description": "Name of the geographic entity",
        "type": "string"
      },
      "population": {
        "description": "Current population of the geographic entity",
        "type": "integer"
      }
    },
    "required": [
      "name",
      "population"
    ]
  }
}

[MODEL RESPONSE]
[
  {
    "name": "Rome",
    "population": 2860009
  },
  {
    "name": "Milan",
    "population": 1395274
  },
  {
    "name": "Florence",
    "population": 382238
  },
  {
    "name": "Venice",
    "population": 261905
  },
  {
    "name": "Bologna",
    "population": 392523
  }
] 

// --- tips --- //
[PRESET 'formatJSchema']
Preset 'formatJSchema' can be used to help create a JSON schema for JSON formatted response.
The prompt should be clear, specific, and outline clear goals.
If you’re working with numbers, be sure to specify the data type.

The following is an example of a good prompt:

<PROMPT_EXAMPLE>
Create the following JSON schema:
description "The current population of the given geographic entities".
items: an array of keys

2 keys:

1. "name":, description "Name of the geographic entity".
2. "population":, description "Current population of the geographic entity", type integer.

All keys are required.
</PROMPT_EXAMPLE>

Command exec preset 'formatJSchema' and save output to file 'schema.json':
geminicli --preset formatJSchema --output schema.json PROMPT_EXAMPLE_HERE

Result:
<JSON>
{
  "description": "The current population of the given geographic entities",
  "type": "array",
  "items": {
    "type": "object",
    "properties": {
      "name": {
        "type": "string",
        "description": "Name of the geographic entity"
      },
      "population": {
        "type": "integer",
        "description": "Current population of the geographic entity"
      }
    },
    "required": [
      "name",
      "population"
    ]
  }
}
</JSON>

Then:
geminicli --format-jschema '{JSON_SCHEMA}' USER_PROMPT

[REFERENCE]
<https://ai.google.dev/api/rest/v1beta/cachedContents#schema>`

const defaultValues string = `Defaults:
  model: gemini-1.5-flash-latest
  model embeddings: text-embedding-004
  model cached content: gemini-1.5-flash-001
  model json schema: gemini-1.5-pro
  function call if functions are present: auto
  code exec: false
  temperature: 0.0
  top_p: 0.95
  top_k: 40
  presence penalty: 0.0
  frequency penalty: 0.0
  safety threshold: all categories BLOCK_MEDIUM_AND_ABOVE
  max response tokens: 1000
  max text response tokens for media: 300
  number of responses: 1 (from API docs 'This value must be 1')
  response format: text
  stream response mode: false
  thinking message: printed
  connection timeout: 120 sec
  connection timeout stream mode (chunk, total): 5 sec, 180 sec
  number of retries: 0
  wait before retry: 10 sec
  embeddings csv filename: data.csv
  web HTML element selection: main/body
  PDF pages selection: all
  cache TTL: 300 sec
  api-key name: GOOGLE_API_KEY`
