# geminicli - overview

## Table of Contents

- [CHANGELOG](https://gitlab.com/ai-gimlab/geminicli/-/blob/main/CHANGELOG.md#changelog)
- [EXAMPLES](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#geminicli---examples)
- [Introduction](#introduction)
  - [Presets](#presets)
  - [Models](#models)
- [Disclaimer](#disclaimer)
- [Getting started](#getting-started)
  - [Binary](#binary)
  - [Compile from source](#compile-from-source)
  - [Default settings](#default-settings)
  - [Usage](#usage)
    - [Informational options](#informational-options)
    - [Data analysis options](#data-analysis-options)
- [Token count](#token-count)
- [How to get and use Google Api Key](#how-to-get-and-use-google-api-key)
- [Credits](#credits)

---

## Introduction

geminicli: Terminal Chat Completion client for [Google Gemini](https://ai.google/gemini-ecosystem/) LLM models family written in [Go](https://go.dev/).

This utility is a kind of Swiss Army knife for AI developers used to working in a text-based terminal.

It has no chat features. It use chat endpoints as *completion* endpoint. Its sole purpose is to quickly craft prompts, check payload and/or response format, tokens usage, collect and analyze data, try the same prompt with different parameter settings, and so on.

Take advantage of Gemini's **multimodality**: text, images, video, and audio ([technical details](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#media)).

Prompts can be content from **web** sites ([technical details](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#web-contents)). **Note**: [dynamic web pages](https://en.wikipedia.org/wiki/Dynamic_web_page) are not supported.

Prompts can also be text content from **PDF** documents ([technical details](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#pdf-documents)). **Note**: option `--pdf FILENAME` extracts only text. For full PDF content extraction you can use `--media FILENAME` option.

Includes support for the **Cache API** ([technical details](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#caching)).

Can get **embedding** of a prompt or batch of prompts ([technical details](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#embedding)).

Embeddings can be used with companion utility [eucos](https://gitlab.com/ai-gimlab/eucos#eucos).

Request/Response session can be exported in a **CSV** file ([technical details](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#export-to-csv)).

The man page can be found [here](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/man).

All of the [examples](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#geminicli---examples) in this guide use the Bash shell on Linux systems, but can run on other platforms as well.

To follow this guide a basic knowledge of [Google Gemini APIs](https://ai.google.dev/gemini-api) is required.

More **usage examples** [here](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#geminicli---examples).

---

### Presets

There are some basic [presets](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#presets), such as summarization or sentiment analysis, that can be used as a reference or starting point.

[**Presets**](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#presets) imply that all parameters are presetted, but they can be changed ([technical details](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#customize-presets)).

**Any preset is executed against a given prompt**. For example, preset *summary* summarize user prompt, preset *ner* execute a [Named Entity Recognition](https://en.wikipedia.org/wiki/Named-entity_recognition) task on user text prompt, etc...

Presets can be used with web pages. If you use `--web URL` option with a preset, **web page text content becomes the prompt** ([technical details](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#using-presets-with-web-content)).

Presets can also be used with PDF documents. If you use `--pdf PDF_FILENAME` option with a preset, **PDF document text content becomes the prompt** ([technical details](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#using-presets-with-pdf-content)).

#### Presets list

To list all presets use `--list-presets` option:

```bash
geminicli --list-presets
```

Output:

```text
presets:
   absurdity                    (write nonsense, using PROMPT as starting point)
   baby                         (write stories for kids using user PROMPT as inspiration)
   contrarian                   (try to find any flaw in PROMPT, eg. a marketing plan, a poem, source code, etc...)
   description                  (try to describe PROMPT context, such as a place, an object, etc...)
   elegant                      (write a story inspired by PROMPT, trying to use an elegant style)
   formatJSchema                (helps create a JSON schema for JSON formatted response ('--format-examples' option for more))
   functionJSchema              (creates 'function call' JSON object for function calling tasks ('--function-examples' option for more))
   headline                     (an headline of user PROMPT)
   manpage                      (create 'troff' man page code from a given command help output as PROMPT)
   mantranslate                 (translate man page from English to colon separated LANGUAGE at beginning of PROMPT, such as 'Italian: [MAN_PAGE_CONTENT.1_HERE]')
   ner                          (Named Entity Recognition of user PROMPT)
   offensive                    (user PROMPT analysis for offenses directed 'at': ignore bad words used as break-in)
   poem                         (write a poem inspired by PROMPT)
   presentationbare             (converts PROMPT to barebone presentation - can be used as PROMPT for 'presentationcode' preset)
   presentationcode             (converts PROMPT from 'presentationbare' preset to Python code needed to create a PowerPoint basic presentation)
   role                         (find a Role for a given PROMPT - useful for 'act as' or 'you are' LLMs role attribution)
   semiotic                     (try to analyze PROMPT with Umberto Eco's semiotic rules)
   sentiment                    (sentiment analysis of user PROMPT: positive/negative)
   sentimentneutral             (sentiment analysis of user PROMPT: positive/negative/neutral)
   stylecasual                  (rewrites user PROMPT in a more casual style)
   styleformal                  (rewrites user PROMPT in a more formal style)
   stylenews                    (rewrites user PROMPT in the style of a newscaster)
   summary                      (summary of user PROMPT)
   summarybrief                 (brief summary of user PROMPT)
   summarybullet                (bulleted summary of user PROMPT)
   table                        (try to find a data pattern from PROMPT, to output as table)
   tablecsv                     (try to find a data pattern from PROMPT, to output as table, CSV formatted)
   terzarima                    (try to use Dante Alighieri's 'Divina Commedia' style using user PROMPT as inspiration)
   tutor                        (give a big picture about PROMPT topic)
   visiondescription            (describe an image uploaded with '--media IMAGEFILE' option - in PROMPT specify only language (Italian, English, etc...))

  💡                            geminicli --preset poem --stream "Little white cat"
                                geminicli --preset summarybullet --stream --web "https://www.examples.com"
                                geminicli --preset mantranslate "Spanish: $(zcat /usr/share/man/man1/ls.1.gz)" | gzip -c - > ls-es.1.gz
```

More **presets usage examples** [here](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#presets).

---

### Models

To list all supported Google Gemini models, use `--list-models` option:

```bash
# actual query to AI model
geminicli --list-models
```

Output:

```txt
chat/completion models:
  gemini-1.0-pro
  gemini-1.0-pro-001
  gemini-1.0-pro-latest
  gemini-1.0-pro-vision-latest
  gemini-1.5-flash
  gemini-1.5-flash-001
  gemini-1.5-flash-001-tuning
  gemini-1.5-flash-002
  gemini-1.5-flash-8b
  gemini-1.5-flash-8b-001
  gemini-1.5-flash-8b-exp-0827
  gemini-1.5-flash-8b-exp-0924
  gemini-1.5-flash-8b-latest
  gemini-1.5-flash-exp-0827
  gemini-1.5-flash-latest
  gemini-1.5-pro
  gemini-1.5-pro-001
  gemini-1.5-pro-002
  gemini-1.5-pro-exp-0801
  gemini-1.5-pro-exp-0827
  gemini-1.5-pro-latest
  gemini-pro
  gemini-pro-vision

embedding models:
  text-embedding-004

ref: https://ai.google.dev/gemini-api/docs/models/gemini
```

With option `-m, --model` it's possible to choose which model to use.

**Note**: your models list may be different.

---

[top](#table-of-contents)

---

## Disclaimer

I enjoy to develop this utilities just for my personal use. So, use them at your own risk.

If you want to develop your own solution written in [Go](https://go.dev/) and based on Google Gemini models family, I suggest you use the [Google's Gemini API Go SDK](https://ai.google.dev/api?lang=go#install-sdk). For other languages check the [Google Gemini API documentation](https://ai.google.dev/api).

---

[top](#table-of-contents)

---

## Getting started

### Binary

Prebuilt binary package for Linux/WSL can be downloaded from [here](https://gitlab.com/ai-gimlab/geminicli/-/releases).

---

### Compile from source

If you prefer, clone this repo and compile from sources.

Prerequisite:
- [Go Development Environment](https://go.dev/dl/)
- [GCC 10 or higher](https://gcc.gnu.org/)

Clone this repo and build:

```bash
git clone https://gitlab.com/ai-gimlab/geminicli.git
cd geminicli
go mod init geminicli && go mod tidy
go build -ldflags="-s -w -X main.version=$(git tag --points-at HEAD | cut -d "v" -f2)" .
```

---

### Default settings

- **model**: gemini-1.5-flash-latest
- **model embeddings**: text-embedding-004
- **model cached content**: gemini-1.5-flash-001
- **model json schema**: gemini-1.5-pro
- **function call if functions are present**: auto
- **code exec**: false
- **temperature**: 0.0
- **top_p**: 0.95
- **top_k**: 40
- **presence penalty**: 0.0
- **frequency penalty**: 0.0
- **safety threshold**: all categories BLOCK_MEDIUM_AND_ABOVE
- **max response tokens**: 1000
- **max text response tokens for media**: 300
- **number of responses**: 1 (from API docs 'This value must be 1')
- **response format**: text
- **stream response mode**: false
- **thinking message**: printed
- **connection timeout**: 120 sec
- **connection timeout stream mode (chunk, total)**: 5 sec, 180 sec
- **number of retries**: 0
- **wait before retry**: 10 sec
- **embeddings csv filename**: data.csv
- **web HTML element selection**: main/body
- **PDF pages selection**: all
- **cache TTL**: 300 sec
- **api-key name**: GOOGLE_API_KEY

[Presets](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#presets) have their own default settings. Use option `--preview` to view settings for a given preset ([technical details](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#preset-settings)).

---

### Usage

Basically, a [prompt is enough](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#quick-start):

```bash
geminicli "PROMPT"
```

More **usage examples** [here](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#geminicli---examples).

---

#### Help

Use `--help` to view all options:

```bash
geminicli --help
```

Output:

```txt
Usage: geminicli [OPTIONS] "PROMPT"

Terminal Chat Completion client for Google's Gemini AI models

Defaults:
  model: gemini-1.5-flash-latest
  model embeddings: text-embedding-004
  model cached content: gemini-1.5-flash-001
  model json schema: gemini-1.5-pro
  function call if functions are present: auto
  code exec: false
  temperature: 0.0
  top_p: 0.95
  top_k: 40
  presence penalty: 0.0
  frequency penalty: 0.0
  safety threshold: all categories BLOCK_MEDIUM_AND_ABOVE
  max response tokens: 1000
  max text response tokens for media: 300
  number of responses: 1 (from API docs 'This value must be 1')
  response format: text
  stream response mode: false
  thinking message: printed
  connection timeout: 120 sec
  connection timeout stream mode (chunk, total): 5 sec, 180 sec
  number of retries: 0
  wait before retry: 10 sec
  embeddings csv filename: data.csv
  web HTML element selection: main/body
  PDF pages selection: all
  cache TTL: 300 sec
  api-key name: GOOGLE_API_KEY

Order matter:
  first OPTIONS
  then PROMPT

Notes:
  . PROMPT must always be enclosed in "double" quotes.

Online Documentation:
  <https://gitlab.com/ai-gimlab/geminicli#geminicli---overview>

OPTIONS:
Global:
      --defaults                  prints the program default values and exit
  -l, --list-models               lists names of all available models and exit
  -m, --model=MODEL               select model
                                  use '--list-models' to lists all available models
      --model-info                return information about a given model
                                  can be used in conjunction with the '--model' option
  -p, --preview                   preview request payload and exit
      --response-raw              prints full response body raw
      --response-json             prints full response body json formatted
      --help                      prints this help
      --version                   output version information and exit

Network:
      --retries=RETRIES           number of connection retries if network timeout occur
                                  use the '--retries-wait' option to insert a pause between retries
      --retries-wait=SECONDS      in conjunction with '--retries RETRIES' option insert a pause between retries
      --timeout=SECONDS           network connection timeout, in seconds
                                  apply to a complete request/response session
                                  SECONDS = 0 means no timeout
      --timeout-chunk=SECONDS     network connection timeout, in seconds
                                  used only in stream mode
                                  apply to every streamed chunk of response

API Auth:                         ref: <https://gitlab.com/ai-gimlab/geminicli#how-to-get-and-use-google-api-key>
      --env=NAME                  uses NAME as environment variable name instead of 'GOOGLE_API_KEY'
                                  such as 'GOOGLE_SEARCH_API_KEY', 'PROJECT_1269', etc...
  -f, --file=APIKEY_FILE          NOTE: DEPRECATED - use option '-k, --key-file=FILENAME' instead
  -k, --key-file=FILENAME         file containing the Google api key (GOOGLE_API_KEY=your_apikey)

Chat/Completion/Media API:        ref: <https://ai.google.dev/api/rest/v1beta/models/generateContent>
  -a, --assistant="PROMPT"        [role]: 'assistant' (model) message PROMPT
                                  must be used with '--previous-prompt' option
                                  can be used together with the '--function-response' option:
                                   - function call responses must be passed as JSON objects, double comma separated
                                     eg: --assistant '{JSON_FUNC_CALL_RESP_0},,{JSON_FUNC_CALL_RESP_1},,{JSON_FUNC_CALL_RESP_N}'
                                   - use '--function-examples' to view how to compose function call json object
  -c, --count=SELECT              prints how many tokens have been used
                                  can also prints the word count of the AI response
                                  SELECT can be 'in', 'out', 'cache', 'total' (in + out) or 'words'
                                  can also be any comma separated combination of above,
                                  without any space, without trailing comma:
                                    in,out
                                    in,cache,total
                                    in,out,total,words
                                    etc...
      --cache=NAME                execute completion task using cached content NAME
                                  NAME is the key '"name":' from '--cache-create' response
                                  eg: "name": "cachedContents/abcde12345fg"
                                  NAME can be with or without "cachedContents/" prefix
      --cfg=TOML_CFG_FILE         load a basic configuration from TOML_CFG_FILE
                                  take precedence over defaults
                                  command line options take precedence over TOML_CFG_FILE configurations
      --cfg-save=TOML_CFG_FILE    save a basic configuration to TOML_CFG_FILE using task session values
                                  can be used with completion tasks or preview mode ('--preview' option)
      --citations                 prints source attribution of the generated content (if any)
      --code-exec                 the model generate and run PROMPT required Python code
                                  it run Python code until it arrives at a final output
                                  ref: <https://ai.google.dev/gemini-api/docs/code-execution>
      --csv=CSV_FILE              export request/response data, csv formatted, to CSV_FILE
                                  no stream mode
                                   - if CSV_FILE does not exist: ask permission to create a new one
                                   - if CSV_FILE exist: append data
      --finish-reason             prints the reason why the model stopped generating tokens
      --format=FORMAT             the format/mimetype that the model must output (Gemini 1.5 Flash/Pro and newer)
                                  FORMAT can be either:
                                   - 'text/plain' or simply 'text' (default)
                                   - 'application/json' or simply 'json'
      --format-examples           prints examples of 'JSON SCHEMA format' request and exit
      --format-jschema={SCHEMA}   JSON schema for JSON formatted response (Gemini 1.5 Pro and newer)
                                  SCHEMA ref: <https://ai.google.dev/api/rest/v1beta/cachedContents#schema>
                                  supported models: <https://ai.google.dev/gemini-api/docs/api-overview#json>
                                  '--format=FORMAT' option is automatically set to 'application/json'
                                  use '--format-examples' to view how to compose a SCHEMA object
      --fp=VALUE                  frequency penalty: VALUE range from -2.0 up to, but not including, 2.0
                                  positive values decrease the probability of repeating content
      --function='{JSON}'         json object containing function definition
                                  must be enclosed in single quotes
                                  stream mode available only if used with option '--function-response={JSON}'
                                  support multiple functions json object definitions, separated by double commas
                                    eg: '{JSON_FUNC_1},,{JSON_FUNC_2},,{JSON_FUNC_N}'
                                  use '--function-examples' to view how to compose function definition json object
                                  ref: <https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#functions>
      --function-allowed=NAMES    model is forced to call a function from the list of function NAMES
                                  NAMES is a comma separated list of function declaration names, enclosed in single/double quotes
                                    eg: "FNAME_A,FNAME_B,FNAME_X"
                                  NOTE: function call MODE is automatically set to required/any
      --function-call="MODE"      how the model responds to function calls
                                  MODE can be:
                                   - "auto" (model choose if calling function)
                                   - "none" (model does not call a function)
                                   - "required" or "any" (model is forced to call one or more functions)
      --function-examples         prints some examples of function json object and exit
      --function-response={JSON}  json object containing results from application's actual function
                                  must be used with options:
                                   --assistant={FUNC_CALL_JSON_RESP_0,,FUNC_CALL_JSON_RESP_1,,FUNC_CALL_JSON_RESP_N}
                                   --previouse-prompt=PROMPT
                                   --function={FUNC_DEF_0,,FUNC_DEF_1,,FUNC_DEF_N}
                                  use '--function-examples' to view how to compose function response json object
                                  ref: <https://ai.google.dev/gemini-api/docs/function-calling#expandable-7>
      --jrequest-run=JSON_FILE    run request payload from JSON_FILE
                                  can be used only with options '--model=MODEL' and '--stream'
                                   - take precedence over the default configurations
                                     and those defined in the TOML_CFG_FILE file,
                                     except the 'model' and 'stream' mode
      --jrequest-save=JSON_FILE   save request payload to JSON_FILE using a task session values
      --list-presets              lists all available predefined tasks and exit
      --logprobs                  return log probabilities of the response choosed tokens, in tabular format
                                  is equivalent to '--top-logprobs=0'
                                   - in conjunction with the '--response-json' or '--response-raw' options
                                     logprobs are included in the response, json formatted
      --media=FILENAME            answer questions about FILENAME
                                  FILENAME can be any text, image, audio or video file
                                  FILENAME can be a local file and/or a FileAPI URI
                                  for multiple FILENAMEs enclose comma separated filenames/uri in double quotes,
                                  without any space, without trailing comma, such as:
                                    "img/img1.jpeg,https://generativelanguage.googleapis.com/v1beta/files/abc-123,audio.mp3,vid.mp4"
                                  NOTE:
                                    for payloads greater than 20 MB the media files must be first uploaded with 'File API'
                                    ref: <https://ai.google.dev/gemini-api/docs/vision?lang=node#upload-image>
      --model-used                which model was used at endpoint side
      --no-console                suppresses console output
                                  must be used in conjunction with '--output=FILENAME' option
                                  no stream mode
      --no-thinking               does not print the 'thinking' message if a 'thinking' model is used
  -o, --output=FILENAME           save response to FILENAME
      --output-full               save full chat system/user/assistant to FILENAME, adding headings. Eg.:
                                    **SYSTEM**: you are helpful assistant
                                    **USER**: Write a poem
                                    **ASSISTANT**: No more, I quit!
                                  must be used in conjunction with '--output=FILENAME' option
      --pdf=PDF_FILENAME          text only extraction from local PDF_FILENAME
                                  in user PROMPT ask only for the context of the extracted text. Eg.:
                                   - WRONG: geminicli --pdf document.pdf "From the following document {QUESTION}"
                                   - OK:    geminicli --pdf document.pdf "How do I completely factory reset my phone?"
                                  if '--pdf-pages' option not provided, extracts all document's text
                                  Note: full PDF documents, text + images, can also be used as is with '--media FILENAME' option
      --pdf-pages=PAGES           'physical pages' of PDF document from which to extract text
                                  PAGES can be any of [page_number], [page_number,page_number], [page_number-page_number]
                                  examples:
                                  - 5 (only page 5)
                                  - 1,5,8 (pages 1, 5 and 8)
                                  - 14-18 (range of pages from page 14 to page 18)
                                  - 1,5,8,14-18,36,50-100
                                    (pages 1, 5, 8, 36 and ranges from page 14 to page 18, from page 50 to page 100)
                                  must be used with option '--pdf=PDF_FILENAME'
      --pdf-pagenumber            prints a footer with 'physical page numbers'
                                  must be used in conjunction with the '--pdf-test' option
      --pdf-test                  prints an output of text extracted from PDF file and exit
      --pdf-system                prints default system message for pdf tasks
      --pp=VALUE                  presence penalty: VALUE range from -2.0 up to, but not including, 2.0
                                  positive values increase the probability of generating more diverse content
      --preset=PRESET_NAME        predefined tasks, such as summarization, sentiment analysis, etc...
                                  use '--list-presets' to lists all available PRESET_NAMEs and their purpose
                                  NOTE: preset configuration take precedence over TOML configuration file
      --preset-system             prints PRESET_NAME predefined system message and exit
      --previous-prompt=PROMPT    in conjunction with the '--assistant' option simulates a single round of chat
  -r, --response-tokens=TOKENS    maximun number of response tokens
                                  ref: <https://ai.google.dev/models/gemini> for models context size
      --safety-all=THRESHOLD      set the same safety threshold THRESHOLD for all categories
                                  can be used together with the options '--safety-{CATEGORY}'
                                  option '--safety-{CATEGORY}' overwrite configuration for the given category
                                  THRESHOLD can be any of:
                                   - BLOCK_NONE
                                   - BLOCK_LOW_AND_ABOVE
                                   - BLOCK_MEDIUM_AND_ABOVE (default)
                                   - BLOCK_ONLY_HIGH
                                  ref.: <https://cloud.google.com/vertex-ai/docs/generative-ai/multimodal/configure-safety-attributes>
      --safety-dcont=THRESHOLD    category: HARM_CATEGORY_DANGEROUS_CONTENT
                                  for THRESHOLDs refer to '--safety-all' option
      --safety-harass=THRESHOLD   category: HARM_CATEGORY_HARASSMENT
                                  for THRESHOLDs refer to '--safety-all' option
      --safety-hate=THRESHOLD     category: HARM_CATEGORY_HATE_SPEECH
                                  for THRESHOLDs refer to '--safety-all' option
      --safety-sex=THRESHOLD      category: HARM_CATEGORY_SEXUALLY_EXPLICIT
                                  for THRESHOLDs refer to '--safety-all' option
      --stop="STOP,..."           comma separated list of stop sequences, up to 5 sequences,
                                  without trailing comma, enclosed in single or double quotes
                                    eg: "STOP1,STOP2,..."
      --stream                    start printing completion before the full completion is finished
  -s, --system="PROMPT"           system message PROMPT (Gemini 1.5 and newer)
  -t, --temperature=VALUE         VALUE range from 0.0 to 2.0 (the maximum value is model dependent)
      --test=URL                  inspect requests using an owned web debug server on URL
                                  URL must be 'absolute'
                                  NOTE: does not send sensitive data
      --test-httpbin              inspect requests using httpbin.org as web debug server
                                  NOTE: does not send sensitive data
      --top-k=VALUE               top_k: VALUE range from 1 to (the maximum value is model dependent)
      --top-logprobs=NUMBER       number of top candidates tokens to return
                                  NUMBER is an integer between 0 and 5
                                  '--top-logprobs=0' equivalent to '--logprobs' option
                                   - in conjunction with the '--response-json' or '--response-raw' options
                                     top logprobs are included in the response, json formatted
      --top-p=VALUE               top_p: VALUE range from 0.0 to 1.0
      --web=URL                   any web page URL (dynamic web pages are not supported)
                                  URL must always be encloded in single or double quotes
                                  in user PROMPT ask only for the context of the page. Eg.:
                                   - WRONG: geminicli --web "https://www.example.com" "From the following web page {QUESTION}"
                                   - OK:    geminicli --web "https://www.example.com" "Extract a list of cat names"
                                  used in conjunction with option '--preset', URL content becomes the user PROMPT. Eg:
                                   - geminicli --preset ner --web "https://www.example.com"
                                     execute NER preset using content from "https://www.example.com" web page
      --web-select=SELECTOR       select a section of web page, then strip html tags
                                  SELECTOR can be any HTML element, such as 'h2', 'p', 'body', 'article', etc...
                                  SELECTOR can also be any class or id attribute (eg.: '.myClass', '#myID')
                                  lastly SELECTOR can be 'NONE' keyword: no html tag stripping
                                   - Note: NONE can use a lot of tokens
                                  SELECTOR must always be encloded in single or double quotes
                                  default selectors are 'main', if exist, or 'body'
      --web-test                  prints an output of text extracted from web page and exit
                                  useful to find the right HTML selector (check --web-select option)
                                  with the right SELECTOR less tokens are used
      --web-system                prints default system message for web tasks

Embeddings API:                   ref: <https://ai.google.dev/api/rest/v1beta/models/embedContent>
  -e, --embed                     return PROMPT embedding
                                  PROMPT must always be enclosed in "double" quotes
                                  return a cvs table with columns 'id, text, embedding'
                                  append table to file 'data.csv' or create new one
                                   - to save in a different file use '--csv FILENAME' option
                                   - in conjunction with the '--response-json' or '--response-raw' options
                                     response is not saved to file, instead output json data to stdout
                                  BATCH: multiple PROMPTs can be sent in batch
                                   - multiple PROMPTs must be separated by double commas
                                     eg: geminicli --embed "PROMPT_1,,PROMPT_2,,...,,PROMPT_N"
      --embed-dimensions=VALUE    change embedding vector size to VALUE dimensions
                                  must be used in conjunction with '--embed' option

Cache API:                        ref: <https://ai.google.dev/api/caching>
  CREATE/UPDATE/DELETE CACHE:     ref: <https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#caching>
      --cache-create              creates cached resource
                                  must be used in conjunction with '--media=FILENAME' option
      --cache-delete=NAME         deletes cached resource
                                  NAME is the key '"name":' from '--cache-create' response
                                  eg: "name": "cachedContents/abcde12345fg"
                                  NAME can be with or without "cachedContents/" prefix
      --cache-dname=DNAME         a human-readable display name for a given cache
                                  must be used with '--cache-create' option
                                  max DNAME length: 128 Unicode characters
      --cache-expire=TIMESTAMP    when cache is considered expired
                                  TIMESTAMP must be in RFC3339 UTC "Zulu" format,
                                  with nanosecond resolution and up to nine fractional digits
                                  eg.: "2024-08-18T15:01:23Z" and "2024-08-18T15:01:23.045123456Z"
                                  TIMESTAMP must always be enclosed in "double" quotes
                                  must be used in conjunction with '--cache-create' or '--cache-update' options
                                  it is mutually exclusive with '--cache-ttl=SECONDS' option
      --cache-ttl=SECONDS         cache duration in SECONDS with up to nine fractional digits, such as "3.5" (3.5 seconds)
                                  must be used in conjunction with '--cache-create' or '--cache-update' options
                                  it is mutually exclusive with '--cache-expire=TIMESTAMP' option
      --cache-update=NAME         updates cached resource NAME (only expiration)
                                  must be used with either '--cache-ttl=SECONDS' or '--cache-expire=TIMESTAMP'
  INFO:                           ref: <https://ai.google.dev/api/rest/v1beta/files/list>
      --cache-info=NAME           show cache NAME information
                                  NAME is the key '"name":' from '--cache-create' response
                                  eg: "name": "cachedContents/abcde12345fg"
                                  NAME can be with or without "cachedContents/" prefix
      --cache-list                lists cached contents
      --cache-listsize=SIZE       number of cached contents to return per page with '--cache-list' option
                                  must be used with '--cache-list' option
      --cache-listtoken=TOKEN     page token from a previous '--cache-list --cache-listsize=SIZE' call
                                  TOKEN is value of '"nextPageToken":' key from previous '--cache-list --cache-listsize=SIZE' call
                                  must be used with '--cache-list' option

File API:                         ref: <https://ai.google.dev/gemini-api/docs/prompting_with_media>
  UPLOAD/DELETE FILE:             ref: <https://ai.google.dev/api/rest/v1beta/media/upload>
      --fdelete=NAME              delete an uploaded file
                                  NAME is the key '"name":' from file upload/list/info response
                                  eg: "name": "files/123-abc-vwxyz"
                                  NAME can be with or without "files/" prefix
      --fdname=DNAME              a human-readable display name for uploaded FILENAME
                                  must be used with '--fupload=FILENAME' option
      --fname=NAME                uploaded FILENAME resource identifier name
                                  must be used with '--fupload=FILENAME' option
                                  if not used a unique NAME will be generated
      --fupload=FILENAME          upload FILENAME with 'File API'

  INFO:                           ref: <https://ai.google.dev/api/rest/v1beta/files/list>
      --finfo=NAME                gets the metadata for the given NAME
                                  NAME is the key '"name":' from file upload response
                                  eg: "name": "files/123-abc-vwxyz"
                                  NAME can be with or without "files/" prefix
      --flist                     lists uploaded files
      --flist-size=SIZE           number of files to return per page with '--flist' option
                                  must be used with '--flist' option
      --flist-token=TOKEN         page token from a previous '--flist --flist-size=SIZE' call
                                  TOKEN is value of '"nextPageToken":' key from previous '--flist --flist-size=SIZE' call
                                  must be used with '--flist' option

  💡                              geminicli --count in,out,total "Summarize the following text: {TEXT}"
                                  geminicli --jrequest-run PAYLOAD_FILE.json
```

---

#### Informational options

Some options output informations only, without making any requests:

- `--function-examples`: prints an example of function json object
- `--format-examples`: prints an example of json schema for JSON format requests
- `-l, --list-models`: list names of all available models
- `--list-presets`: list all available predefined tasks
- `--preset-system`: prints predefined system message for a given preset - must be used with `--preset PRESET_NAME` option
- `-p, --preview`: preview request payload, json formatted
- `--web-test`: prints an output of text extracted from web page - must be used with `--web URL` option
- `--pdf-test`: prints an output of text extracted from PDF document - must be used with `--pdf PDF_FILENAME` option
- `--test=URL` and `--test-httpbin` prints a complete dump (headers, URL field's query parameters, body) of the request
- `--help`: prints help
- `--version`: prints version information

In the following example some [parameters](https://ai.google.dev/api/generate-content) are set, such as topK (`--top-k 25`), temperature (`--temperature 0.8`) and system message (`--system "be polite"`). Using `-p, --preview` option, you can verify that the resulting payload is the one you want, but without performing any requests:

```bash
# preview payload
geminicli --preview --top-k 25 --temperature 0.8 --system "be polite" "In about 15 words write a poem about a little white cat"
```

Output:

```json
{
  "generationConfig": {
    "temperature": 0.8,
    "topP": 0.95,
    "topK": 25,
    "candidateCount": 1,
    "maxOutputTokens": 1000
  },
  "systemInstruction": {
    "parts": [
      {
        "text": "be polite"
      }
    ]
  },
  "contents": [
    {
      "role": "user",
      "parts": [
        {
          "text": "In about 15 words write a poem about a little white cat"
        }
      ]
    }
  ],
  "safetySettings": [
    {
      "category": "HARM_CATEGORY_DANGEROUS_CONTENT",
      "threshold": "BLOCK_MEDIUM_AND_ABOVE"
    },
    {
      "category": "HARM_CATEGORY_HATE_SPEECH",
      "threshold": "BLOCK_MEDIUM_AND_ABOVE"
    },
    {
      "category": "HARM_CATEGORY_HARASSMENT",
      "threshold": "BLOCK_MEDIUM_AND_ABOVE"
    },
    {
      "category": "HARM_CATEGORY_SEXUALLY_EXPLICIT",
      "threshold": "BLOCK_MEDIUM_AND_ABOVE"
    }
  ]
}
```

```text
ENDPOINT: https://generativelanguage.googleapis.com/v1beta/models/gemini-1.5-flash:generateContent?key=API_KEY
```

---

#### Data analysis options

Some options give more verbose glimpse of what's going on during requests:

- `-c, --count`: prints how many tokens have been used - check [Token count](#token-count) section for more
- `--csv`: export request and response data, csv formatted, to file - useful for data analysis ([technical details](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#export-to-csv))
- `--response-json`: prints full response, json formatted - useful to find the right key to unmarshal
- `--response-raw`: prints the unformatted raw response - useful to understand how stream behave
- `--citations`: prints source attribution of the generated content (if any)
- `--finish-reason`: prints the reason why the model stopped generating tokens

The same example as above, but without `-p, --preview` option, because we want actually execute request. This time we use `--response-json` option, to view all response details:

```bash
# actual query to AI model
geminicli --response-json --top-k 25 --temperature 0.8 --system "be polite" "In about 15 words write a poem about a little white cat"
```

Response:

```json
{
  "candidates": [
    {
      "content": {
        "parts": [
          {
            "text": "A tiny white fluff, \nWith eyes of emerald green,\nA playful, purring dream. \n"
          }
        ],
        "role": "model"
      },
      "finishReason": "STOP",
      "index": 0,
      "safetyRatings": [
        {
          "category": "HARM_CATEGORY_SEXUALLY_EXPLICIT",
          "probability": "NEGLIGIBLE"
        },
        {
          "category": "HARM_CATEGORY_HATE_SPEECH",
          "probability": "NEGLIGIBLE"
        },
        {
          "category": "HARM_CATEGORY_HARASSMENT",
          "probability": "NEGLIGIBLE"
        },
        {
          "category": "HARM_CATEGORY_DANGEROUS_CONTENT",
          "probability": "NEGLIGIBLE"
        }
      ]
    }
  ],
  "usageMetadata": {
    "promptTokenCount": 17,
    "candidatesTokenCount": 21,
    "totalTokenCount": 38
  }
}
```

The AI answer contains all the information you need to understand what's going on under the hood:

- `"usageMetadata":` contain actual tokens usage
- `"content":` contain response
- `"finishReason":` why the model stopped generating tokens
- etc...

More **details on** [Gemini API documentation](https://ai.google.dev/api) page.

More **usage examples** [here](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#geminicli---examples).

---

[top](#table-of-contents)

---

## Token count

Option `-c, --count=SELECT` return count of used tokens and AI response words. SELECT can be *in*, *out*, *cache*, *total* (in + out), *words*(output words) or any combination, such as `in,out,words`, `in,total`, etc...

**Preview Mode**: `--preview` option don't actually execute any request, therefore the information on cached tokens, output tokens, total tokens and output words cannot be available. Only input tokens, the user prompt, are available.

**Web Test Mode** and **PDF Test Mode**: in conjunction with `--web-test` or `--PDF-test` options, `-c, --count=SELECT` option return the actual number of tokens that would be used for a given web page or pdf document. The count refers to the raw web page or the raw pdf document text, without the addition of messages, such as the *system* message, and other payload stuff. it represents the real web page or pdf document usage.

More **tokens/words usage examples** [here](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#tokens-and-words-usage).

---

[top](#table-of-contents)

---

## How to get and use Google Api Key

Get your api key from [Google Gemini](https://aistudio.google.com/app/apikey) site ([pricing](https://ai.google.dev/pricing)).

**Note**: the api key default name is *GOOGLE_API_KEY*. Can be changed with option `--env=NAME`.

There are three way to supply api key:

**[1] default**

Create file `.env` and insert the following line:

```bash
GOOGLE_API_KEY='YOUR-API-KEY'
```

File `.env` content example:

```bash
GOOGLE_API_KEY='1234567890qwertyuiopasdfghjklzxcvbnm123'
ANOTHER_REST_API_KEY='qwertyuiopasdfghjklzxcvbnm1234567890'
HAL9000_AGI_KEY='m0_50_C4z'
```

Copy file `.env` to `$HOME/.local/etc/` folder.

**[2] environment variable**

If you prefer, export GOOGLE_API_KEY as environment variable:

```bash
export GOOGLE_API_KEY='YOUR-API-KEY'
```

**[3] use '-f, --file' option**

You can also supply your own key file, containing the statement `GOOGLE_API_KEY='YOUR-API-KEY'`, and pass its path as argument to `-f, --file` option.

---

[top](#table-of-contents)

---

## Credits

This project is made possible thanks to the use of the following libraries and the precious work of those who create and maintain them.
Of course thanks also to all those who create and maintain the AI models.

- [css](https://github.com/ericchiang/css)
- [getopt](https://github.com/pborman/getopt/)
- [go-fitz](https://github.com/gen2brain/go-fitz) (Go wrapper for [MuPDF](http://mupdf.com/) fitz library)
- [godotenv](https://github.com/joho/godotenv)
- [mimetype](https://github.com/gabriel-vasile/mimetype)
- [toml](https://github.com/BurntSushi/toml)
- [Google Gemini™](https://deepmind.google/technologies/gemini/#introduction)

---

[top](#table-of-contents)

---

[More **usage examples**](https://gitlab.com/ai-gimlab/geminicli/-/tree/main/examples?ref_type=heads#geminicli---examples).

---

[Others gimlab repos](https://gitlab.com/users/gimaldi/groups)

---
