/*  'geminicli' Terminal Chat Completion client for Google's Gemini AI models
    Copyright (C) 2024 gimlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"context"
	"encoding/base64"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"math"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/gabriel-vasile/mimetype"
)

// check if user given model name is a valid model name
func checkModelName(name string) error {
	// function name to return if any error occur
	var funcName string = "checkModelName"

	if err := listModels(); err != nil {
		return err
	}

	// check against registered names
	for _, modelName := range modelNames {
		if modelName == name {
			return nil
		}
	}

	// if not found, return error
	return fmt.Errorf("func %q - wrong model name: %q", funcName, name)
}

// count input tokens in stream mode, preview mode and web-test mode
func countEstimatedInputTokens() {
	// function name to return if any error occur
	var funcName string = "countEstimatedInputTokens"

	var rct requestCountToken
	var g generateContentRequest
	var r request

	// create JSON payload of request
	payloadRequestJSON, err := createPayloadJSON()
	if err != nil {
		fmt.Printf("func %q: %s\n", funcName, err.Error())
		return
	}

	// create JSON payload for token counting using original request
	if err := json.Unmarshal([]byte(payloadRequestJSON), &r); err != nil {
		fmt.Printf("func %q: %s\n", funcName, err.Error())
		return
	}
	g.Model = "models/" + model
	g.GenerationConfig = r.GenerationConfig
	g.SystemInstruction = r.SystemInstruction
	g.ToolConfig = r.ToolConfig
	g.SafetySettings = r.SafetySettings[:]
	g.Tools = r.Tools[:]
	if len(r.Contents) > 0 {
		for _, c := range r.Contents {
			g.Contents = append(g.Contents, c)
		}
	}
	rct.GenerateContentRequest = &g
	payloadJSON, err := json.Marshal(rct)
	if err != nil {
		fmt.Printf("func %q: %s\n", funcName, err.Error())
		return
	}

	// send request
	resp, err := httpRequestPOST(payloadJSON)
	if err != nil {
		fmt.Printf("func %q: %s\n", funcName, err.Error())
		return
	}
	countChannel <- true // main() is waiting for this channel

	// decode JSON response data
	var estimated responseCountToken
	if err := json.Unmarshal([]byte(resp), &estimated); err != nil {
		fmt.Printf("func %q: %s\n", funcName, err.Error())
		return
	}

	// result
	estimatedInputTokens = estimated.TotalTokens
}

// count input tokens in preview mode + load JSON request mode
func countEstimatedInputTokensPayload(payloadRequestJSON []byte) {
	// function name to return if any error occur
	var funcName string = "countEstimatedInputTokensPayload"

	var rct requestCountToken
	var g generateContentRequest
	var r request

	// create JSON payload for token counting using original request
	if err := json.Unmarshal([]byte(payloadRequestJSON), &r); err != nil {
		fmt.Printf("func %q: %s\n", funcName, err.Error())
		return
	}
	g.Model = "models/" + model
	g.GenerationConfig = r.GenerationConfig
	g.SystemInstruction = r.SystemInstruction
	g.ToolConfig = r.ToolConfig
	g.SafetySettings = r.SafetySettings[:]
	g.Tools = r.Tools[:]
	if len(r.Contents) > 0 {
		for _, c := range r.Contents {
			g.Contents = append(g.Contents, c)
		}
	}
	rct.GenerateContentRequest = &g
	payloadJSON, err := json.Marshal(rct)
	if err != nil {
		fmt.Printf("func %q: %s\n", funcName, err.Error())
		return
	}

	// send request
	resp, err := httpRequestPOST(payloadJSON)
	if err != nil {
		fmt.Printf("func %q: %s\n", funcName, err.Error())
		return
	}
	countChannel <- true // main() is waiting for this channel

	// decode JSON response data
	var estimated responseCountToken
	if err := json.Unmarshal([]byte(resp), &estimated); err != nil {
		fmt.Printf("func %q: %s\n", funcName, err.Error())
		return
	}

	// result
	estimatedInputTokens = estimated.TotalTokens
}

// tokens usage
func printTokensUsage(u *usageMetadata) {
	// function name to return if any error occur
	var funcName string = "printTokensUsage"

	switch {
	case flagPreview || flagWebTest || flagPdfTest:
		// --- estimated input tokens --- //
		if !strings.Contains(flagCount, tokenInput) {
			fmt.Printf("\a\n'--%s', %s or '--%s' and '-%c' or '--%s' provided: %q tokens not available\n", flagPreviewLong, flagWebTestLong, flagPdfTestLong, flagCountShort, flagCountLong, flagCount)
			return
		}

		// Set a read timeout for 'countTokens' endpoint
		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		defer cancel()

		// loop until the results are available
		for ctxDone := true; ctxDone; {
			select {
			case <-ctx.Done():
				fmt.Printf("func %q - timeout counting input tokens\n", funcName)
				ctxDone = false
			default:
				if estimatedInputTokens > 0 {
					if flagWebTest {
						fmt.Printf("\nestimated web page tokens: %d\n", estimatedInputTokens)
						return
					}
					if flagPdfTest {
						fmt.Printf("\nestimated pdf document tokens: %d\n", estimatedInputTokens)
						return
					}
					fmt.Printf("\nestimated input tokens: %d\n", estimatedInputTokens)
					ctxDone = false
				}
			}
		}
	case flagCount == tokenInput:
		// --- single argument (any of 'in', 'out', 'cache', 'total', 'words') --- //
		fmt.Printf("%d\n", u.PromptTokenCount)
	case flagCount == tokenOutput:
		fmt.Printf("%d\n", u.CandidatesTokenCount)
	case flagCount == tokenCache:
		fmt.Printf("%d\n", u.CachedContentTokenCount)
	case flagCount == tokenSumInOut:
		fmt.Printf("%d\n", u.TotalTokenCount)
	case flagCount == outputWords:
		fmt.Printf("%d\n", outputWordsCount)
	case strings.Contains(flagCount, ","):
		// --- multiple arguments (a combination of 'in', 'out', 'cache', 'total', 'words') --- //
		if strings.Contains(flagCount, tokenInput) {
			fmt.Printf("input tokens:\t%d\n", u.PromptTokenCount)
		}
		if strings.Contains(flagCount, tokenOutput) {
			fmt.Printf("output tokens:\t%d\n", u.CandidatesTokenCount)
		}
		if strings.Contains(flagCount, tokenCache) {
			fmt.Printf("cached tokens:\t%d\n", u.CachedContentTokenCount)
		}
		if strings.Contains(flagCount, tokenSumInOut) {
			fmt.Printf("total tokens:\t%d\n", u.TotalTokenCount)
		}
		if strings.Contains(flagCount, outputWords) {
			fmt.Printf("output words:\t%d\n", outputWordsCount)
		}
	}
}

// export csv data
func writeFileCSV(newFile bool) error {
	// function name to return if any error occur
	var funcName string = "writeFileCSV"

	// create or open file (append)
	f, err := os.OpenFile(flagCsv, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		return fmt.Errorf("func %q - file %s: %w\n", funcName, flagCsv, err)
	}
	defer f.Close()

	// check file size
	fileInfo, err := f.Stat()
	if err != nil {
		return fmt.Errorf("func %q - error checking csv file size: %w\n", funcName, err)
	}

	// prepare data
	var b strings.Builder
	if newFile || fileInfo.Size() == 0 {
		// if csv file does not exist, or is empty, write headers
		if _, err := b.WriteString(csvFieldsName); err != nil {
			return fmt.Errorf("func %q - error composing csv [header]: %w\n", funcName, err)
		}
	}

	// append row
	for i, lenCsvFields := 0, len(csvFields); i < lenCsvFields; i++ {
		if _, err := b.WriteString(csvFields[i]); err != nil {
			return fmt.Errorf("func %q - error composing csv [data]: %w\n", funcName, err)
		}
		if i < len(csvFields)-1 {
			if _, err := b.WriteString(","); err != nil {
				return fmt.Errorf("func %q - error composing csv [data]: %w\n", funcName, err)
			}
		}
	}

	// end row with newline
	if _, err := b.WriteString("\n"); err != nil {
		return fmt.Errorf("func %q - error composing csv [data]: %w\n", funcName, err)
	}

	// write data to csv file
	if _, err := f.Write([]byte(b.String())); err != nil {
		return fmt.Errorf("func %q - file %s: %w\n", funcName, flagCsv, err)
	}

	return nil
}

// encode request data to csv
func requestCSV(req *request) {
	// user, system, assistant PROMPTs
	if len(req.Contents) > 0 {
		for _, c := range req.Contents {
			if c.Role == user {
				// sanitize message: all in a single line
				s := strings.ReplaceAll(c.Parts[0].Text, "\n", "\\n")
				csvFields[requestContentUser] = "\"" + s + "\""
			}
		}
	}
	if flagSystem != "" {
		s := strings.ReplaceAll(req.SystemInstruction.Parts[0].Text, "\n", "\\n")
		csvFields[requestContentSystem] = "\"" + s + "\""
	}

	// request parameters
	csvFields[requestModel] = model
	csvFields[requestResponseFormat] = req.GenerationConfig.ResponseMimeType
	csvFields[requestTemperature] = fmt.Sprintf("%.2f", req.GenerationConfig.Temperature)
	csvFields[requestTop_p] = fmt.Sprintf("%.2f", req.GenerationConfig.TopP)
	csvFields[requestTop_k] = fmt.Sprintf("%d", req.GenerationConfig.TopK)
	csvFields[requestPresencePenalty] = fmt.Sprintf("%.2f", req.GenerationConfig.PresencePenalty)
	csvFields[requestFrequencyPenalty] = fmt.Sprintf("%.2f", req.GenerationConfig.FrequencyPenalty)

	if flagFunction != "" {
		csvFields[requestFunction] = "true"
		csvFields[requestFunctionCall] = fmode[strings.ToUpper(functionCallMode)]
	}
}

// encode response data to csv
func responseCSV(resp response) {
	csvFields[responseInputTokens] = fmt.Sprintf("%d", resp.UsageMetadata.PromptTokenCount)
	csvFields[responseOutputTokens] = fmt.Sprintf("%d", resp.UsageMetadata.CandidatesTokenCount)
	csvFields[responseTotalTokens] = fmt.Sprintf("%d", resp.UsageMetadata.TotalTokenCount)
	csvFields[responseOutputWords] = fmt.Sprintf("%d", len(resp.Candidates[0].Content.Parts[0].Text))

	if flagFunction != "" {
		fname := make([]string, 0)
		fargs := make([]string, 0)
		if len(resp.Candidates) > 0 {
			if resp.Candidates[0].Content.Parts[0].FunctionCall != nil {
				// function call YES
				for _, c := range resp.Candidates {
					for i, p := range c.Content.Parts {
						if p.FunctionCall != nil {
							item := fmt.Sprintf("[%d]:", i)
							// this field contain commas and double quotes, but quotes are needed to delimit field, so they are removed
							args := strings.ReplaceAll(string(*p.FunctionCall.Args), "\"", "")
							fname = append(fname, item+"("+p.FunctionCall.Name+")")
							fargs = append(fargs, item+args)
						}
					}
					csvFields[responseFunctionCall] = "true"
					csvFields[responseFunctionName] = strings.Join(fname, "; ")
					csvFields[responseFunctionArgs] = "\"" + strings.Join(fargs, "; ") + "\""
				}
			}
			if resp.Candidates[0].Content.Parts[0].FunctionCall == nil {
				// function call NO
				csvFields[responseFunctionCall] = "false"
			}
		}
	}

	// sanitize response message: all in a single line, remove extra quotes
	s := resp.Candidates[0].Content.Parts[0].Text
	s = strings.ReplaceAll(s, "\n", "\\n")
	s = strings.ReplaceAll(s, "\"", "")
	csvFields[responseContent] = "\"" + s + "\""
}

// embeddingsCSV write embeddings response to file
func embeddingsCSV(re *responseEmbed, reb *responseEmbedBatch) error {
	// function name to return if any error occur
	var funcName string = "embeddingsCSV"

	// last row id of existing csv file - used for append data
	var embedCsvFileOffset int = 0

	// use custom csv file
	if flagCsv != "" {
		embedCsvFile = flagCsv
	}

	// create or open file (append)
	f, err := os.OpenFile(embedCsvFile, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0600)
	if err != nil {
		return fmt.Errorf("func %q - error creating embeddings csv file: %w\n", funcName, err)
	}
	defer f.Close()

	// check file size
	fileInfo, err := f.Stat()
	if err != nil {
		return fmt.Errorf("func %q - error checking csv file size: %w\n", funcName, err)
	}

	// Create a CSV writer
	csvWriter := csv.NewWriter(f)

	// Create a CSV reader
	csvReader := csv.NewReader(f)

	// if new file: write headers
	// else: check last row id
	switch {
	case fileInfo.Size() == 0:
		// if csv file does not exist, or is empty, write headers
		if err := csvWriter.Write(embedCsvHeader); err != nil {
			return fmt.Errorf("func %q - error writing headers to csv file %s: %w\n", funcName, embedCsvFile, err)
		}
	case fileInfo.Size() != 0:
		var records [][]string
		for {
			record, err := csvReader.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				return fmt.Errorf("func %q - error reading csv file %s: %w\n", funcName, embedCsvFile, err)
			}

			records = append(records, record)
		}
		if offset, err := strconv.ParseInt(records[len(records)-1][0], 10, 64); err != nil {
			return fmt.Errorf("func %q - error reading last line of csv file %s: %w\n", funcName, embedCsvFile, err)
		} else {
			embedCsvFileOffset = int(offset) + 1
		}
	}

	switch {
	case len(inputs) > 1:
		// batch
		for i, e := range reb.Embeddings {
			id := fmt.Sprintf("%d", embedCsvFileOffset)
			text := inputs[i]
			s, err := embeddingString(e.Values)
			if err != nil {
				return err
			}
			// Write the data record to the CSV file.
			if err := csvWriter.Write([]string{id, text, s}); err != nil {
				return fmt.Errorf("func %q - error writing data to CSV file %s: %w\n", funcName, embedCsvFile, err)
			}
			embedCsvFileOffset++
		}
	default:
		// single prompt
		id := fmt.Sprintf("%d", embedCsvFileOffset)
		text := inputs[0]
		s, err := embeddingString(re.Embedding.Values)
		if err != nil {
			return err
		}
		// Write the data record to the CSV file.
		if err := csvWriter.Write([]string{id, text, s}); err != nil {
			return fmt.Errorf("func %q - error writing data to CSV file %s: %w\n", funcName, embedCsvFile, err)
		}
	}

	// Flush the CSV writer.
	csvWriter.Flush()

	fmt.Printf("\nembeddings data successfully written to file %q\n\n", embedCsvFile)

	return nil
}

func embeddingString(embeddingValues []float64) (embeddingValuesToString string, err error) {
	// function name to return if any error occur
	var funcName string = "embeddingString"

	var embedding strings.Builder
	if _, err := embedding.WriteString("["); err != nil {
		return "", fmt.Errorf("func %q - error creating embeddings CSV row: %w\n", funcName, err)
	}
	for i, e := range embeddingValues {
		f := strconv.FormatFloat(e, 'f', -1, 64)
		trimmed := strings.TrimRight(strings.TrimRight(f, "0"), ".")
		if _, err := embedding.WriteString(trimmed); err != nil {
			return "", fmt.Errorf("func %q - error creating embeddings CSV field: %w\n", funcName, err)
		}
		if i < len(embeddingValues)-1 {
			if _, err := embedding.WriteRune(','); err != nil {
				return "", fmt.Errorf("func %q - error creating embeddings CSV field: %w\n", funcName, err)
			}
		}
	}
	if _, err := embedding.WriteRune(']'); err != nil {
		return "", fmt.Errorf("func %q - error creating embeddings CSV row: %w\n", funcName, err)
	}

	return embedding.String(), nil
}

func finalResult(bodyString string, r *response, re *responseEmbed, reb *responseEmbedBatch) (rc int, err error) {
	// --- parse response --- //
	switch {
	case flagRaw || flagJson:
		// print raw or json response (in single response mode raw response is already json formatted)
		ppJSON, err := prettyPrintJsonString(bodyString)
		if err != nil {
			return 1, err
		}
		if !flagNoConsole {
			fmt.Printf("%s\n", ppJSON)
		}
		if flagOutput != "" {
			if err := formatTextFileOutput(ppJSON); err != nil {
				return 1, err
			}
			fmt.Printf("\nsaved response/chat file %q\n", flagOutput)
		}
		return 0, nil
	case responseFormat == responseMimeTypeJSON:
		// check if and why the model stopped generating tokens
		if r.Candidates[0].Content == nil || len(r.Candidates[0].Content.Parts) == 0 {
			if err = finishReasonCheck(&r.Candidates[0]); err != nil {
				return 1, err
			}
			return 0, nil
		}
		s := r.Candidates[0].Content.Parts[0].Text
		s = strings.TrimSuffix(s, "\n")
		ppJSON, err := prettyPrintJsonString(s)
		if err != nil {
			return 1, err
		}
		if !flagNoConsole {
			fmt.Printf("%s\n", ppJSON)
		}
		if flagOutput != "" {
			if err := formatTextFileOutput(ppJSON); err != nil {
				return 1, err
			}
			fmt.Printf("\nsaved response/chat file %q\n", flagOutput)
		}
		return 0, nil
	case flagEmbed:
		// - embedding - //
		if len(inputs) > 1 {
			if err := embeddingsCSV(nil, reb); err != nil {
				return 0, err
			}
			return 0, nil
		}
		if err := embeddingsCSV(re, nil); err != nil {
			return 0, err
		}
		return 0, nil
	default:
		// - NOT stream mode - //
		// print results
		var citations []citationSources
		if len(r.Candidates) > 0 {
			for _, c := range r.Candidates {
				// function call
				if flagFunction != "" && flagFunctionResponse == "" {
					var b strings.Builder
					if len(c.Content.Parts) > 0 {
						for i, p := range c.Content.Parts {
							if p.FunctionCall != nil {
								// function call YES
								if i == 0 {
									if _, err := b.WriteString("function call: yes\n\n"); err != nil {
										return 1, err
									}
									if !flagNoConsole {
										fmt.Printf("function call: yes\n\n")
									}
								}
								if _, err := b.WriteString("function name: " + p.FunctionCall.Name + "\n"); err != nil {
									return 1, err
								}
								if !flagNoConsole {
									fmt.Printf("function name: %s\n", p.FunctionCall.Name)
								}
								if p.FunctionCall.Args != nil {
									args, err := json.Marshal(p.FunctionCall.Args)
									if err != nil {
										return 1, err
									}
									s := string(args)
									s = strings.ReplaceAll(s, "\\\\n", "\\n")
									s = strings.ReplaceAll(s, "\\\\'", "'")
									s = strings.ReplaceAll(s, "\\\\`", "`")
									if _, err := b.WriteString("function args: " + s + "\n"); err != nil {
										return 1, err
									}
									if !flagNoConsole {
										fmt.Printf("function args: ")
										fmt.Printf("%s\n", s)
									}
									if i < len(c.Content.Parts)-1 {
										if _, err := b.WriteString("\n"); err != nil {
											return 1, err
										}
										if !flagNoConsole {
											fmt.Println()
										}
									}
								}
							}
							if p.FunctionCall == nil {
								// function call NO
								if _, err := b.WriteString("function call: no\n\n" + p.Text + "\n"); err != nil {
									return 1, err
								}
								if !flagNoConsole {
									fmt.Printf("function call: no\n\n")
									fmt.Printf("%s\n", p.Text)
								}
							}
						}
						if flagOutput != "" {
							if err := formatTextFileOutput(b.String()); err != nil {
								return 1, err
							}
							fmt.Printf("\nsaved response/chat file %q\n", flagOutput)
						}
						continue
					}
				}
				// check if and why the model did not generate tokens
				if c.Content == nil || len(c.Content.Parts) == 0 {
					if err = finishReasonCheck(&c); err != nil {
						return 1, err
					}
				}
				// check if model generate tokens
				if c.Content != nil && len(c.Content.Parts) > 0 {
					var t string // used to write response to file
					// print response
					if logProbs {
						fmt.Println("[RESPONSE]:")
					}
					// code-exec task
					if flagCodeExecution {
						for i, p := range c.Content.Parts {
							switch {
							case i == 0 || i == 3:
								t = t + p.Text
							case i == 1:
								if p.ExecutableCode != nil {
									t = t + fmt.Sprintf("```%s%s```\n\n", strings.ToLower(p.ExecutableCode.Language), strings.TrimSuffix(p.ExecutableCode.Code, "\n"))
								}
							case i == 2:
								if p.CodeExecutionResult != nil {
									t = t + fmt.Sprintf("RESULT: %s\n%s\n", p.CodeExecutionResult.Outcome, p.CodeExecutionResult.Output)
								}
							}
						}
						if !flagNoConsole {
							fmt.Printf("%s", t)
						}
					}
					if !flagCodeExecution {
						for _, p := range c.Content.Parts {
							/*
									// "The Gemini API doesn't return thoughts in the response."
									// ref: https://ai.google.dev/gemini-api/docs/thinking#use-thinking-models
								for i, p := range c.Content.Parts {
									// thinking model: thinking not printed
									if strings.Contains(r.ModelVersion, modelThinking) && (i == 0 && flagNoThinking) {
										continue
									}
									// thinking model: thinking header
									if strings.Contains(r.ModelVersion, modelThinking) && (i == 0 && !flagNoThinking) {
										t = t + "[THINKING]\n"
									}
									// thinking model: answer header
									if strings.Contains(r.ModelVersion, modelThinking) && (i == 1 && !flagNoThinking) {
										t = t + "\n\n---\n\n[RESPONSE]\n"
									}
							*/
							// all models: generated text
							if flagPreset == "presentationcode" { // remove markdown formatting from python code
								p.Text = strings.TrimPrefix(p.Text, "```python")
								p.Text = strings.TrimSuffix(p.Text, "```")
							}
							t = t + p.Text
							if !flagNoConsole {
								fmt.Printf("%s", t)
							}
							textCitations = textCitations + p.Text
						}
					}
					// save response to file
					if flagOutput != "" {
						if err := formatTextFileOutput(t); err != nil {
							return 1, err
						}
						fmt.Printf("\nsaved response/chat file %q\n", flagOutput)
					}
					// check if and why the model stopped generating tokens
					if err := finishReasonCheck(&c); err != nil {
						return 1, err
					}
					// check citations
					if c.CitationMetadata != nil {
						if len(c.CitationMetadata.CitationSource) > 0 {
							for _, cs := range c.CitationMetadata.CitationSource {
								citations = append(citations, cs)
							}
						}
					}
				}
				// finish reason requested
				if flagFinishReason {
					if err := finishReasonCheck(&c); err != nil {
						return 1, err
					}
				}
			}
			if flagFunction == "" {
				fmt.Println()
			}
			// print citations (if any)
			if flagCitations {
				fmt.Printf("\n\nCITATIONS:")
				if len(citations) > 0 {
					c, err := json.Marshal(citations)
					if err != nil {
						return 1, fmt.Errorf("error creating 'citations' JSON: %w", err)
					}
					ppJSON, err := prettyPrintJsonString(string(c))
					if err != nil {
						return 1, fmt.Errorf("error printing 'citations': %w", err)
					}
					fmt.Printf("\n%s\n\n", ppJSON)
					// print citations text
					for i, c := range citations {
						b := []byte(textCitations)
						s := b[c.StartIndex:c.EndIndex]
						fmt.Printf("[CITATION TEXT %d]\n%s\n\n", i, s)
					}
				} else {
					fmt.Printf(" NONE\n")
				}
			}
		}
	}
	return 0, nil
}

func printLogprobs(bodyString string, response *response) error {
	if len(response.Candidates) > 0 && response.Candidates[0].LogprobsResult != nil {
		// FIX: Gemini 2.0 returning string "Infinite"
		var avgFloat float64
		var okFloat bool
		// check if returned value is float
		if avgFloat, okFloat = response.Candidates[0].AvgLogprobs.(float64); !okFloat {
			fmt.Printf("logprobs not available\n")
			return nil
		}
		// print average logprobs
		//avg := strconv.FormatFloat(response.Candidates[0].AvgLogprobs, 'f', -1, 64) // original code
		avg := strconv.FormatFloat(avgFloat, 'f', -1, 64) // temporary fix
		// END_FIX
		fmt.Printf("[AVERAGE LOGPROBS]:\n%s\n", avg)
		// choosen logprobs
		if len(response.Candidates[0].LogprobsResult.ChosenCandidates) > 0 {
			// prints header
			formatLogprobs("[CHOSEN LOGPROBS CANDIDATES]:")
			for i := range response.Candidates[0].LogprobsResult.ChosenCandidates {
				// extract and format data
				s := strconv.FormatFloat(response.Candidates[0].LogprobsResult.ChosenCandidates[i].LogProbability, 'g', -1, 64)
				t := response.Candidates[0].LogprobsResult.ChosenCandidates[i].Token
				percent := math.Exp(response.Candidates[0].LogprobsResult.ChosenCandidates[i].LogProbability) * 100
				// prints data
				fmt.Printf("%-30q", t)
				fmt.Printf("%-30s", s)
				fmt.Printf("%14.8f\t\t", percent)
				fmt.Println()
			}
			// prints line separator
			for i := 0; i < logProbsRowLength; i++ {
				fmt.Printf("-")
			}
			fmt.Println()
		}
		// top logprobs candidates
		if topLogProbs != 0 {
			if len(response.Candidates[0].LogprobsResult.TopCandidates) > 0 {
				// prints header
				formatLogprobs("[CANDIDATES WITH TOP LOG PROBABILITIES]:")
				for tc := range response.Candidates[0].LogprobsResult.TopCandidates {
					if len(response.Candidates[0].LogprobsResult.TopCandidates[tc].Candidates) > 0 {
						for c := range response.Candidates[0].LogprobsResult.TopCandidates[tc].Candidates {
							// extract and format data
							s := strconv.FormatFloat(response.Candidates[0].LogprobsResult.TopCandidates[tc].Candidates[c].LogProbability, 'g', -1, 64)
							t := response.Candidates[0].LogprobsResult.TopCandidates[tc].Candidates[c].Token
							percent := math.Exp(response.Candidates[0].LogprobsResult.TopCandidates[tc].Candidates[c].LogProbability) * 100
							// prints data
							fmt.Printf("%-30q", t)
							fmt.Printf("%-30s", s)
							fmt.Printf("%14.8f\t\t", percent)
							fmt.Println()
						}
					}
					// prints line separator
					for i := 0; i < logProbsRowLength; i++ {
						fmt.Printf("-")
					}
					fmt.Println()
				}
			}
		}
		fmt.Println()
		return nil
	}
	// logprobs not available
	return fmt.Errorf("%q or %q informations not available\n", "logprobs", "top_logprobs")
}

// formatLogprobs prints logprobs/top-logprobs headers
func formatLogprobs(header string) {
	fmt.Println()
	fmt.Println(header)
	for i := 0; i < logProbsRowLength; i++ {
		fmt.Printf("-")
	}
	fmt.Println()
	fmt.Printf("%-30s%-30s%14s\n", "token", "logprob", "%")
	for i := 0; i < logProbsRowLength; i++ {
		fmt.Printf("-")
	}
	fmt.Println()
}

func finishReasonCheck(c *candidate) error {
	switch {
	case c.FinishReason != "" && (c.FinishReason != "STOP" && c.FinishReason != "MAX_TOKENS"):
		fmt.Printf("\n\n-- WARNING: token generation was stopped --\n\n")
		fmt.Printf("DETAILS:\n")
		if err := printFinishReason(c); err != nil {
			return err
		}
	case c.FinishReason != "" && flagFinishReason:
		fmt.Printf("\n\nFINISH REASON: %s\n\n", c.FinishReason)
		fmt.Printf("DETAILS:\n")
		if err := printFinishReason(c); err != nil {
			return err
		}
	}
	return nil
}

func printFinishReason(c *candidate) error {
	// function name to return if any error occur
	var funcName string = "printFinishReason"

	// convert and print finishReason json formatted
	r, err := json.Marshal(c)
	if err != nil {
		return fmt.Errorf("func %q - error reading %q JSON data: %w\n", funcName, "finish_reason", err)
	}
	ppJSON, err := prettyPrintJsonString(string(r))
	if err != nil {
		return fmt.Errorf("func %q - error formatting %q JSON data: %w\n", funcName, "finish_reason", err)
	}
	fmt.Printf("%s\n\n", ppJSON)
	if c.FinishReason != "STOP" && c.FinishReason != "MAX_TOKENS" {
		var category string
		if len(c.SafetyRatings) > 0 {
			for _, s := range c.SafetyRatings {
				if s.Blocked {
					category = s.Category
				}
			}
		}
		return fmt.Errorf("func %q - token generation was stopped - finish reason: %s %q", funcName, c.FinishReason, category)
	}
	return nil
}

func validateMediaLocal() error {
	// function name to return if any error occur
	var funcName string = "validateMedia"

	if len(mediaArgsLocal) > 0 {
		for _, v := range mediaArgsLocal {
			var data inlineData
			mime, err := checkMIME(v)
			if err != nil {
				return err
			}
			f, err := os.ReadFile(filepath.Clean(v))
			if err != nil {
				return fmt.Errorf("func %q - file %s: %w\n", funcName, v, err)
			}
			imageFileBase64Encoded := base64.StdEncoding.EncodeToString(f)
			data.Data = imageFileBase64Encoded
			data.MimeType = strings.TrimSuffix(mime, "; charset=utf-8")
			mediaPayload[v] = data
		}
	}
	return nil
}

// check input file MIME type for media tasks
func checkMIME(filename string) (string, error) {
	// function name to return if any error occur
	var funcName string = "checkMIME"

	file, err := os.Open(filepath.Clean(filename))
	if err != nil {
		return "", fmt.Errorf("func %q: %w\n", funcName, err)
	}
	defer file.Close()

	mtype, err := mimetype.DetectReader(file)
	if err != nil {
		return "", fmt.Errorf("func %q: %w\n", funcName, err)
	}

	return mtype.String(), nil
}

// write response to file
func writeFileOutput(response string) error {
	// function name to return if any error occur
	var funcName string = "writeFileOutput"

	// create or open file (append)
	f, err := os.OpenFile(flagOutput, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		return fmt.Errorf("func %q - file %s: %w\n", funcName, flagOutput, err)
	}
	defer f.Close()

	// write data to csv file
	if _, err := f.Write([]byte(response)); err != nil {
		return fmt.Errorf("func %q - file %s: %w\n", funcName, flagOutput, err)
	}

	return nil
}

// formatTextFileOutput save response to file
func formatTextFileOutput(response string) error {
	var b strings.Builder

	// file already exist: add separator
	if _, errFileNotExist := os.Open(flagOutput); !errors.Is(errFileNotExist, fs.ErrNotExist) {
		if _, err := b.WriteString(outputToFileSep); err != nil {
			return err
		}
	}

	if flagOutputFull {
		if len(systemContent.Parts) > 0 {
			// system HEADER
			if _, err := b.WriteString("**SYSTEM**: "); err != nil {
				return err
			}
			// system message
			for _, p := range systemContent.Parts {
				if _, err := b.WriteString(p.Text); err != nil {
					return err
				}
			}
			s := strings.TrimSuffix(b.String(), "\n")
			b.Reset()
			if _, err := b.WriteString(s); err != nil {
				return err
			}
			if _, err := b.WriteString("\n\n"); err != nil {
				return err
			}
		}
		// user HEADER + user PROMPT + assistant HEADER
		prompt = strings.TrimSuffix(prompt, "\n")
		if _, err := b.WriteString("**USER**: " + prompt + "\n\n**ASSISTANT**: "); err != nil {
			return err
		}
	}

	// write assistant response
	if _, err := b.WriteString(response); err != nil {
		return err
	}

	// write completion session to file
	if err := writeFileOutput(b.String()); err != nil {
		return err
	}

	return nil
}
