/*  'geminicli' Terminal Chat Completion client for Google's Gemini AI models
    Copyright (C) 2024 gimlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"gitlab.com/sysUtils/go-libraries/libhelp"
	"gitlab.com/sysUtils/go-libraries/libopt"

	"github.com/pborman/getopt/v2"
)

func main() {
	var (
		logger                 *log.Logger = log.New(os.Stderr, logPrefix, log.Lshortfile)
		responseBody           response
		responseBodyEmbed      responseEmbed
		responseBodyEmbedBatch responseEmbedBatch
		bodyString             string
	)

	// --- check user input chars --- //
	// command name
	if err := libopt.ChkChars(chrLowCase+chrUpCase+chrNums+chrCmdName, os.Args[:1]); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// check short/long options for single/double dash
	if len(os.Args) > 1 && os.Args[1] != "" && os.Args[len(os.Args)-1] != "" {
		if err := checkLongOptionDashes(os.Args[1:]); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
	}

	// --- check cmdline options --- //
	// any error during parse
	if err := getopt.Getopt(nil); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}
	// check Mutually Exclusive Options
	if err := mutuallyExclusiveOptions(); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}
	// check for flag dependencies
	if err := checkFlagDependencies(); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}
	// check if string arguments was provided
	if err := argProvided(); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// informative flags
	switch {
	case flagHelp:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagHelpLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		libhelp.PrintHelp(helpMsgUsage, helpMsgBody, helpMsgTips, true)
		os.Exit(0)
	case flagVersion:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagVersionLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		fmt.Printf("%s %s %s\n%s\n", cmdName, author, version, license)
		os.Exit(0)
	case flagDefaults:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagDefaultsLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		fmt.Println(defaultValues)
		os.Exit(0)
	case flagListModels:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagListModelsLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		// endpoint
		if err := composeEndpoint(); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		// query
		if err := listModels(); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		os.Exit(0)
	case flagModelInfo:
		if len(os.Args) > 4 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagModelInfoLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		if flagModel != "" {
			model = flagModel
		}
		// compose endpoint
		if err := composeEndpoint(); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		// query
		if err := modelInfo(); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		os.Exit(0)
	case flagFunctionExamples:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagFunctionExamplesLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		fmt.Printf("%s\n", jsonPayloadExamples)
		os.Exit(0)
	case flagResponseFormatSchemaExamples:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagResponseFormatSchemaExamplesLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		fmt.Printf("%s\n", jsonSchemaExample)
		os.Exit(0)
	case flagListPresets:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagListPresetsLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		var p presetSettings
		p.printList()
		os.Exit(0)
	case flagPresetSystem && !flagPreview:
		if len(os.Args) > 5 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagPresetSystemLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		var p presetSettings
		if err := p.set(flagPreset); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		p.printSysMsg(flagPreset)
		os.Exit(0)
	case flagWebSystem:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagWebSystemLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		fmt.Println(systemMessageWeb)
		os.Exit(0)
	case flagPdfSystem:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagPdfSystemLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		fmt.Println(systemMessagePdf)
		os.Exit(0)
	}

	// check format of 'string' type flags
	if err := checkFlagTypeString(); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// - check non-options arguments: prompt message - //
	nonOptArgs := getopt.Args()
	switch {
	case flagPreset != "" && (flagWeb != "" || flagPdf != ""):
		// when used with presets, web or pdf content become the prompt
		prompt = ""
	case (len(nonOptArgs) == 0 || (len(nonOptArgs) > 0 && nonOptArgs[0] == "")) && checkNoPrompt():
		// passed an argument that does not need PROMPT
		prompt = ""
	case (len(nonOptArgs) == 0 || (len(nonOptArgs) > 0 && nonOptArgs[0] == "")) && (flagPreset != "" && presets[flagPreset].prompt != ""):
		// given preset has an hardcoded PROMPT
		prompt = presets[flagPreset].prompt
	case len(nonOptArgs) == 0 || (len(nonOptArgs) > 0 && nonOptArgs[0] == ""):
		// no PROMPT given
		logger.Fatal(fmt.Errorf("\a%s\n%s", "missing user PROMPT", tryMsg))
	case len(nonOptArgs) > 1:
		// to many PROMPTs
		var s string
		for i, v := range nonOptArgs {
			if i == 0 {
				s += fmt.Sprintf("[PROMPT %d]\n", i+1)
				s += fmt.Sprintf("%s <- PROBABLY THE SOURCE OF THE PROBLEM\n\n", v)
				continue
			} else {
				s += fmt.Sprintf("[PROMPT %d]\n", i+1)
				s += fmt.Sprintf("%s\n\n", v)
			}
		}
		logger.Fatal(fmt.Errorf("\a%s:\n\n%s\n%s", "too many user PROMPTs or unknown option", s, tryMsg))
	default:
		prompt = nonOptArgs[0]
		// sanitize prompt
		prompt = strings.ReplaceAll(prompt, "\\n", "\n")
	}

	// --- config program --- //
	// presets
	if flagPreset != "" {
		var p presetSettings
		if err := p.set(flagPreset); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
	}
	// load config from TOML file
	if flagCfg != "" {
		if err := configFromFile(flagCfg); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
	}
	// load config from command line options
	if err := configFlags(); err != nil {
		switch {
		case err.Error() == "EXIT":
			os.Exit(-1)
		default:
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
	}
	// endpoint
	if err := composeEndpoint(); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}
	// load JSON payload from file
	if flagPayloadRun != "" {
		f, err := os.Open(flagPayloadRun)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		defer f.Close()
		payloadJSON, err := io.ReadAll(f)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		if flagPreview {
			// '--preview' and '--jrequest-load', with '--count', start background counting of input tokens
			if strings.Contains(flagCount, tokenInput) {
				go countEstimatedInputTokensPayload(payloadJSON)
			}
			fmt.Println(string(payloadJSON))
			if flagCount != "" {
				if _, _, ok := strings.Cut(endpoint, methodCountTokens); ok {
					<-countChannel // wait until countEstimatedInputTokens() make http request
					endpoint, _, _ = strings.Cut(endpoint, methodCountTokens)
					switch {
					case flagStream:
						endpoint = endpoint + methodGenerateStream + keyStream + "&" + keyKey
					default:
						endpoint = endpoint + methodGenerate + keyKey
					}
				}
				// print input token usage
				printTokensUsage(nil)
			}
			fmt.Println()
			fmt.Printf("ENDPOINT: %s%s\n", endpoint, fakeApiKey)
			os.Exit(0)
		}
		if bodyString, err = completionTask(payloadJSON); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		// if requested, print HTTP test
		if flagTestPayload != "" || flagTestPayloadHttpbin {
			fmt.Printf("\n%s\n", bodyString)
			os.Exit(0)
		}
	}

	if flagPayloadRun == "" {
		// user PROMPT exceptions, if any
		if flagTestPayload == "" && !flagTestPayloadHttpbin {
			if err := userPromptExceptions(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
		}
		// config roles
		if err := configRoles(); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
	}

	// '--preview', '--web-test' or '--pdf-test', with '--count', start background counting of input tokens
	if flagPayloadRun == "" && ((flagPreview || flagWebTest || flagPdfTest) && strings.Contains(flagCount, tokenInput)) {
		go countEstimatedInputTokens()
	}

	// if requested, print raw web page, stripped from tags, or pdf raw pages, and exit
	if flagWebTest || flagPdfTest {
		switch {
		case flagWebTest:
			// print web page
			fmt.Printf("%s\n", webPageContent)
		case flagPdfTest:
			// print pdf pages
			fmt.Printf("%s\n", pdfContent)
		}
		if flagCount != "" {
			// print input token usage
			<-countChannel // wait until countEstimatedInputTokens() make http request
			printTokensUsage(nil)
		}
		os.Exit(0)
	}

	// File API: get file metadata, list uploaded files or delete upload file and exit
	if flagFileUploadInfo != "" || flagFileUploadList || flagFileUploadDelete != "" {
		if flagPreview {
			fmt.Println(endpoint + fakeApiKey)
			os.Exit(0)
		}
		httpMethod := "GET"
		if flagFileUploadDelete != "" {
			httpMethod = "DELETE"
		}
		resp, err := httpRequest(httpMethod, true)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			return
		}
		// if requested, print HTTP test
		if flagTestPayload != "" || flagTestPayloadHttpbin {
			fmt.Printf("\n%s\n", resp)
			os.Exit(0)
		}
		if flagFileUploadDelete != "" {
			fmt.Printf("%s: DELETED ", flagFileUploadDelete)
		}
		fmt.Printf(resp)
		os.Exit(0)
	}

	// Cache API: list cached contents, cache information, delete cache
	if flagCacheList || flagCacheInfo != "" || flagCacheDelete != "" {
		if flagPreview {
			fmt.Println(endpoint + fakeApiKey)
			os.Exit(0)
		}
		httpMethod := "GET"
		if flagCacheDelete != "" {
			httpMethod = "DELETE"
		}
		resp, err := httpRequest(httpMethod, true)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			return
		}
		// if requested, print HTTP test
		if flagTestPayload != "" || flagTestPayloadHttpbin {
			fmt.Printf("\n%s\n", resp)
			os.Exit(0)
		}
		if flagCacheDelete != "" {
			fmt.Printf("%s: DELETED ", flagCacheDelete)
		}
		fmt.Printf(resp)
		os.Exit(0)
	}

	// create JSON payload
	var payloadJSON []byte
	var err error
	if flagPayloadRun == "" {
		payloadJSON, err = createPayloadJSON()
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
	}

	// save JSON payload, if requested
	if flagPayloadSave != "" {
		f, err := os.OpenFile(flagPayloadSave, os.O_RDWR|os.O_TRUNC|os.O_CREATE, 0600)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		defer f.Close()
		s, err := prettyPrintJsonString(string(payloadJSON))
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		s = s + "\n"
		_, err = f.WriteString(s)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		fmt.Printf("request payload file %q saved\n", flagPayloadSave)
		fmt.Println()
	}

	// save TOML config file, if requested
	if flagCfgSave != "" {
		if err := configToFile(flagCfgSave); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		if !flagPreview {
			fmt.Printf("saved config file %q\n", flagCfgSave)
			os.Exit(0)
		}
	}

	// preview and exit
	if flagPreview {
		ppJSON, err := prettyPrintJsonString(string(payloadJSON))
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		if flagPreset != "" && flagPresetSystem {
			fmt.Printf("// --- JSON payload --- //\n")
		}
		fmt.Printf("%s\n\n", ppJSON)
		if flagPreset != "" && flagPresetSystem {
			var p presetSettings
			if err := p.set(flagPreset); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Printf("// --- preset %q SYSTEM MESSAGE --- //\n", flagPreset)
			p.printSysMsg(flagPreset)
			fmt.Println()
		}
		if _, _, ok := strings.Cut(endpoint, methodCountTokens); ok {
			<-countChannel // wait until countEstimatedInputTokens() make http request
			endpoint, _, _ = strings.Cut(endpoint, methodCountTokens)
			switch {
			case flagStream:
				endpoint = endpoint + methodGenerateStream + keyStream + "&" + keyKey
			default:
				endpoint = endpoint + methodGenerate + keyKey
			}
		}
		fmt.Printf("ENDPOINT: %s%s\n", endpoint, fakeApiKey)
		if flagCount != "" && strings.Contains(flagCount, tokenInput) {
			printTokensUsage(nil)
		}
		if flagCfgSave != "" {
			fmt.Printf("\nsaved config file %q\n", flagCfgSave)
		}
		os.Exit(0)
	}

	// File API: upload file
	if flagFileUpload != "" {
		uploadUrl, err := initiateUpload(payloadJSON)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		// if requested, print HTTP test
		if flagTestPayload != "" || flagTestPayloadHttpbin {
			fmt.Printf("\n%s\n", uploadUrl)
			os.Exit(0)
		}
		err = uploadChunks(uploadUrl, mediaUploadData)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		endpoint = urlMediaMetadata + "/" + mediaUploadName + "?" + keyKey
		resp, err := httpRequest("GET", true)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		fmt.Printf(resp)
		os.Exit(0)
	}

	// --- execute request and print result --- //
	// make HTTP request (single response mode or stream mode)
	if flagPayloadRun == "" {
		if bodyString, err = completionTask(payloadJSON); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
	}

	// if requested, print HTTP test
	if flagTestPayload != "" || flagTestPayloadHttpbin {
		fmt.Printf("\n%s\n", bodyString)
		os.Exit(0)
	}

	// Cache API: print 'create/update cache' response
	if flagCacheCreate || flagCacheUpdate != "" {
		fmt.Printf(bodyString)
		os.Exit(0)
	}

	if (flagJson || flagRaw) && !flagStream {
		// prints json/raw response
		_, err := finalResult(bodyString, &responseBody, nil, nil)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		os.Exit(0)
	}

	// decode JSON response data
	if !flagStream && !flagEmbed {
		if err := json.Unmarshal([]byte(bodyString), &responseBody); err != nil {
			logger.Fatal(fmt.Errorf("\aerror decoding JSON data: %v\n%s", err, tryMsg))
		}
		// count words in normal mode (not stream)
		if strings.Contains(flagCount, outputWords) {
			if len(responseBody.Candidates) > 0 {
				for _, r := range responseBody.Candidates {
					if len(r.Content.Parts) > 0 {
						for _, c := range r.Content.Parts {
							if len(c.Text) > 0 {
								for _, p := range strings.Split(c.Text, " ") {
									outputWordsCount += len(strings.Fields(p))
								}
							}
						}
					}
				}
			}

		}
	}

	// decode JSON embedding data
	if flagEmbed {
		switch {
		case len(inputs) > 1:
			if err := json.Unmarshal([]byte(bodyString), &responseBodyEmbedBatch); err != nil {
				logger.Fatal(fmt.Errorf("\aerror decoding JSON data: %v\n%s", err, tryMsg))
			}
		default:
			if err := json.Unmarshal([]byte(bodyString), &responseBodyEmbed); err != nil {
				logger.Fatal(fmt.Errorf("\aerror decoding JSON data: %v\n%s", err, tryMsg))
			}
		}
	}

	// encode response data to csv and write all data to csv file
	if flagCsv != "" && !flagEmbed {
		responseCSV(responseBody)
		if err := writeFileCSV(newFileCSV); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
	}

	// --- parse response --- //
	switch {
	case flagStream:
		// dunno - response already printed
	case flagEmbed:
		// save embeddings
		_, err := finalResult(bodyString, nil, &responseBodyEmbed, &responseBodyEmbedBatch)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
	default:
		// prints completion
		_, err := finalResult(bodyString, &responseBody, nil, nil)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		// prints logprobs
		if logProbs && (!flagJson && !flagRaw) {
			if err := printLogprobs(bodyString, &responseBody); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
		}
		// print model name
		if flagModelUsed {
			fmt.Printf("MODEL: %s\n", responseBody.ModelVersion)
		}
	}

	// prints how many tokens and/or words have been used
	if flagCount != "" {
		fmt.Println()
		switch {
		case flagStream:
			var u usageMetadata
			u.PromptTokenCount = streamTokens[inputTokens]
			u.CachedContentTokenCount = streamTokens[cachedTokens]
			u.CandidatesTokenCount = streamTokens[outputTokens]
			u.TotalTokenCount = streamTokens[totalTokens]
			printTokensUsage(&u)
		default:
			printTokensUsage(responseBody.UsageMetadata)
		}
	}
}
