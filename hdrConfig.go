/*  'geminicli' Terminal Chat Completion client for Google's Gemini AI models
    Copyright (C) 2024 gimlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

type tomlConfig struct {
	Model            string  `toml:"model"`
	Format           string  `toml:"format"`
	SafetyAll        string  `toml:"safety_all_categories"`
	Stop             string  `toml:"stop"`
	DashStop         string  `toml:"dash_stop"` // used to comment out config line
	System           string  `toml:"system"`
	DashSystem       string  `toml:"dash_system"` // used to comment out config line
	FunctionCall     string  `toml:"function_call_mode"`
	DashFCall        string  `toml:"dash_fcall"` // used to comment out config line
	OutputFile       string  `toml:"output_file"`
	DashOutFile      string  `toml:"dash_out_file"`   // used to comment out config line
	DashNoConsole    string  `toml:"dash_no_console"` // used to comment out config line
	Temperature      float64 `toml:"temperature"`
	TopP             float64 `toml:"top_P"`
	PresencePenalty  float64 `toml:"presence_penalty"`
	FrequencyPenalty float64 `toml:"frequency_penalty"`
	ResponseTokens   int     `toml:"response_tokens"`
	TopK             int     `toml:"top_K"`
	Retries          int     `toml:"retries"`
	RetriesWait      int     `toml:"retries_wait"`
	Timeout          int     `toml:"timeout"`
	TimeoutChunk     int     `toml:"timeout_chunk"`
	Citations        bool    `toml:"citations"`
	FinishReason     bool    `toml:"finishReason"`
	Stream           bool    `toml:"stream"`
	NoConsole        bool    `toml:"no_console"`
}

const tomlKeys string = "model,stream,retries,retries_wait,timeout,timeout_chunk,citations,finishReason,format,function_call_mode,response_tokens,safety_all_categories,stop,system,temperature,top_P,top_K,presence_penalty,frequency_penalty,output_file,no_console"

var tomlMap map[string]any = make(map[string]any)
var cfgMap map[string]any = make(map[string]any)

const configTemplate = `# This is the 'geminicli' configuration file.

# Allows to perform a basic configuration.

# If an option is commented out, the default value is loaded.
# For example:
#     model = MODEL_NAME (set 'model' to MODEL_NAME)
#     #model = MODEL_NAME (use default model)

# global
model = "{{.Model}}" # string
stream = {{.Stream}} # bool (true/false)

# network
retries = {{.Retries}} # integer
retries_wait = {{.RetriesWait}} # integer
timeout = {{.Timeout}} # integer (seconds)
timeout_chunk = {{.TimeoutChunk}} # integer (seconds)

# completion
citations = {{.Citations}} # bool (true/false)
finishReason = {{.FinishReason}} # bool (true/false)
format = "{{.Format}}" # string ("text" or "json")
{{.DashFCall}}function_call_mode = "{{.FunctionCall}}" # string ("AUTO" or "ANY" or "NONE")
response_tokens = {{.ResponseTokens}} # integer
safety_all_categories = "{{.SafetyAll}}" # string ("BLOCK_NONE" or "BLOCK_LOW_AND_ABOVE" or "BLOCK_MEDIUM_AND_ABOVE" or "BLOCK_ONLY_HIGH")
{{.DashStop}}stop = "{{.Stop}}" # string (comma separated strings, such as: "STRING_1,STRING_2,STRING_3" - max 5 items)
{{.DashSystem}}system = '''{{.System}}''' # string
temperature = {{.Temperature}} # float
top_P = {{.TopP}} # float
top_K = {{.TopK}} # integer
presence_penalty = {{.PresencePenalty}} # float
frequency_penalty = {{.FrequencyPenalty}} # float
{{.DashOutFile}}output_file = "{{.OutputFile}}" # string (where to save response from model)
{{.DashNoConsole}}no_console = {{.NoConsole}} # bool (true/false)
`
