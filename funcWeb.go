/*  'geminicli' Terminal Chat Completion client for Google's Gemini AI models
    Copyright (C) 2024 gimlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/joho/godotenv"
)

// create and send HTTP request
// exctract response body (no stream mode) or call stream function 'httpReceiveStream()'
func httpRequestPOST(payloadJSON []byte) (bodyString string, err error) {
	// function name to return if any error occur
	var funcName string = "httpRequestPOST"

	// set method
	var httpMethod string = "POST"
	if flagCacheUpdate != "" {
		httpMethod = "PATCH"
	}

	// load env configs
	key, err := loadEnv(apikeyName)
	if err != nil {
		return "", err
	}

	// create and configure HTTP request
	switch {
	case flagTestPayload != "" || flagTestPayloadHttpbin:
		// dump server endpoint
		key = fakeApiKey
		endpoint = endpoint + key
		if err := testEndpoint(httpMethod); err != nil {
			return "", err
		}
	default:
		// production endpoint
		endpoint = endpoint + key
	}

	req, err := http.NewRequest(httpMethod, endpoint, bytes.NewBuffer(payloadJSON))
	if err != nil {
		return "", fmt.Errorf("func %q - error creating HTTP request: %w\n", funcName, err)
	}

	// set HTTP User-Agent Header
	ua, err := createUserAgent(req)
	if err != nil {
		return "", fmt.Errorf("func %q - %w\n", funcName, err)
	}

	// add the necessary HTTP headers
	req.Header.Add("Content-Type", "application/json; charset=UTF-8")
	req.Header.Add("User-Agent", ua)

	// create HTTP client
	client := &http.Client{
		Timeout: timeout,
	}

	// make HTTP request
	startTime := time.Now()
	resp, errResp := client.Do(req)
	endTime := time.Now()
	if errResp != nil {
		e := strings.ReplaceAll(errResp.Error(), "\"", "'")
		return "", fmt.Errorf("func '%s' - error during POST request: %s\n", funcName, e)
	}
	defer resp.Body.Close()

	// encode elapsed time to csv
	elapsed := endTime.Sub(startTime)
	csvFields[requestDateTime] = startTime.Format(time.DateTime)
	csvFields[responseElapsedTime] = fmt.Sprintf("%.9f", float64(elapsed)/nanosec)

	// check HTTP response status
	if resp.StatusCode != http.StatusOK {
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return "", fmt.Errorf("func %q - error reading HTTP response body: %w\n", funcName, err)
		}
		errDetails := fmt.Sprintf("// ------ ERROR DETAILS ------ //\n%s", body)
		return "", fmt.Errorf("func %q - there was an error during HTTP transaction\n\n%s", funcName, errDetails)
	}

	if flagStream && (!flagPreview && flagTestPayload == "" && !flagTestPayloadHttpbin) {
		reader := bufio.NewReader(resp.Body)
		if err := httpReceiveStream(reader); err != nil {
			return "", err
		}
		return "", nil
	}

	// parse HTTP response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("func %q - error reading HTTP response body: %w\n", funcName, err)
	}

	return string(body), nil
}

func httpRequest(httpMethod string, requiretAuth bool) (responseBody string, err error) {
	// function name to return if any error occur
	var funcName string = "httpRequest"

	// load env configs
	key, err := loadEnv(apikeyName)
	if err != nil {
		return "", err
	}

	// create and configure HTTP request
	switch {
	case flagTestPayload != "" || flagTestPayloadHttpbin:
		// dump server endpoint
		if requiretAuth {
			key = fakeApiKey
			endpoint = endpoint + key
		}
		if err := testEndpoint(httpMethod); err != nil {
			return "", err
		}
	default:
		// production endpoint
		if requiretAuth {
			endpoint = endpoint + key
		}
	}

	// create HTTP client
	client := &http.Client{
		Timeout: timeout,
	}

	// configure HTTP client
	req, err := http.NewRequest(httpMethod, endpoint, nil)
	if err != nil {
		return "", fmt.Errorf("func %q - error creating HTTP request: %w\n", funcName, err)
	}

	// set HTTP User-Agent Header
	ua, err := createUserAgent(req)
	if err != nil {
		return "", fmt.Errorf("func %q - %w\n", funcName, err)
	}
	req.Header.Add("User-Agent", ua)

	// make HTTP request
	resp, err := client.Do(req)
	if err != nil {
		return "", fmt.Errorf("func %q - error during HTTP transaction: %w\n", funcName, err)
	}
	defer resp.Body.Close()

	// extract body from response
	bodyString, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("func %q - error reading HTML body: %w\n", funcName, err)
	}

	return string(bodyString), nil
}

// parse 'STREAM mode' response
func httpReceiveStream(reader *bufio.Reader) error {
	// function name to return if any error occur
	var funcName string = "httpReceiveStream"

	var checkModelUsed bool = true
	var m string          // hold used model name
	var b strings.Builder // build stream response for stream words conuting purpose
	var citations []citationSources
	var counter int = -1

	for {
		counter++
		// Set a read timeout for each iteration
		ctx, cancel := context.WithTimeout(context.Background(), timeoutChunk)
		defer cancel()

		select {
		case <-ctx.Done():
			return fmt.Errorf("func %q - timeout reading response body\n", funcName)
		default:
			// parse io reader
			line, err := reader.ReadBytes('\n')
			if err != nil {
				// check end of stream
				if errors.Is(err, io.EOF) {
					fmt.Println()
					outputWordsCount = len(strings.Fields(b.String())) // count output words
					// print citations (if any)
					if flagCitations {
						for _, c := range "\n\nCITATIONS:" {
							fmt.Printf("%c", c)
							time.Sleep(streamCharsPause)
						}
						if len(citations) > 0 {
							c, err := json.Marshal(citations)
							if err != nil {
								return fmt.Errorf("error creating 'citations' JSON: %w", err)
							}
							ppJSON, err := prettyPrintJsonString(string(c))
							if err != nil {
								return fmt.Errorf("error printing 'citations': %w", err)
							}
							s := fmt.Sprintf("\n%s\n", ppJSON)
							for _, c := range s {
								fmt.Printf("%c", c)
								time.Sleep(streamCharsPause)
							}
							for i, citation := range citations {
								b := []byte(b.String())
								s := b[citation.StartIndex:citation.EndIndex]
								fmt.Printf("[CITATION TEXT %d]\n", i)
								for _, c := range s {
									fmt.Printf("%c", c)
									time.Sleep(streamCharsPause)
								}
								fmt.Printf("\n\n")
							}
						}
						if len(citations) == 0 {
							for _, c := range " NONE\n" {
								fmt.Printf("%c", c)
								time.Sleep(streamCharsPause)
							}
						}
					}
					if flagModelUsed {
						fmt.Printf("\nMODEL: %s\n", m)
					}
					if flagOutput != "" {
						if err := formatTextFileOutput(b.String()); err != nil {
							return fmt.Errorf("func %q - error saving file: %w\n", funcName, err)
						}
						fmt.Printf("\nsaved response/chat file %q\n", flagOutput)
					}
					return nil
				}
				return fmt.Errorf("func %q - error reading HTTP response body: %w\n", funcName, err)
			}

			// print raw response (if '--response-raw' option was given)
			if flagRaw {
				fmt.Printf("%s", string(line))
				continue
			}

			// sanitize response
			data := bytes.TrimLeft(line, "data: ")
			data = bytes.TrimRight(data, "\n")

			// check if 'data' contain any data
			if len(data) <= 1 {
				continue
			}

			// print response chunk json formatted
			if flagJson {
				j, err := prettyPrintJsonString(string(data))
				if err != nil {
					return err
				}
				fmt.Printf("%s\n", j)
				continue
			}

			// --- parse response
			var responseStream response
			err = json.Unmarshal(data, &responseStream)
			if err != nil {
				return fmt.Errorf("func %q - error extracting JSON response: %w\n", funcName, err)
			}
			if checkModelUsed {
				m = responseStream.ModelVersion
				checkModelUsed = false
			}
			if len(responseStream.Candidates) > 0 {
				if responseStream.Candidates[0].Content != nil {
					if responseStream.Candidates[0].FinishReason != "" {
						if err := finishReasonCheck(&responseStream.Candidates[0]); err != nil {
							return err
						}
					}
					if len(responseStream.Candidates[0].Content.Parts) > 0 {
						//for i, p := range responseStream.Candidates[0].Content.Parts { // ONLY FOR THINKING MODELS
						for _, p := range responseStream.Candidates[0].Content.Parts {
							var t string = ""
							switch {
							case flagCodeExecution && p.ExecutableCode != nil:
								t = fmt.Sprintf("```%s%s```\n\n", strings.ToLower(p.ExecutableCode.Language), strings.TrimSuffix(p.ExecutableCode.Code, "\n"))
							case flagCodeExecution && p.CodeExecutionResult != nil:
								t = fmt.Sprintf("RESULT: %s\n%s\n", p.CodeExecutionResult.Outcome, p.CodeExecutionResult.Output)
							default:
								/*
										// "The Gemini API doesn't return thoughts in the response."
										// ref: https://ai.google.dev/gemini-api/docs/thinking#use-thinking-models
									if strings.Contains(responseStream.ModelVersion, modelThinking) && (i == 0 && flagNoThinking) {
										continue
									}
									if strings.Contains(responseStream.ModelVersion, modelThinking) && (counter == 0 && !flagNoThinking) {
										t = t + "[THINKING]\n"
									}
									if strings.Contains(responseStream.ModelVersion, modelThinking) && (i == 1 && !flagNoThinking) {
										t = t + "\n\n---\n\n[RESPONSE]\n"
									}
								*/
								t = t + p.Text
							}
							// build response string for words counting purpose
							if _, err := b.WriteString(t); err != nil {
								return fmt.Errorf("func %q - error building stream response\n", funcName)
							}
							// output char by char, for smooth streaming
							for _, c := range t {
								fmt.Printf("%c", c)
								time.Sleep(streamCharsPause)
							}
						}
						// token count
						if responseStream.Candidates[0].FinishReason != "" && responseStream.Candidates[0].FinishReason == "STOP" {
							streamTokens[inputTokens] = responseStream.UsageMetadata.PromptTokenCount
							streamTokens[cachedTokens] = responseStream.UsageMetadata.CachedContentTokenCount
							streamTokens[outputTokens] = responseStream.UsageMetadata.CandidatesTokenCount
							streamTokens[totalTokens] = responseStream.UsageMetadata.TotalTokenCount
						}
					}
				}
				if responseStream.Candidates[0].CitationMetadata != nil {
					if len(responseStream.Candidates[0].CitationMetadata.CitationSource) > 0 {
						for _, cm := range responseStream.Candidates[0].CitationMetadata.CitationSource {
							citations = append(citations, cm)
						}
					}
				}
				continue
			}
		}
		cancel()
	}
}

// load API key
func loadEnv(label string) (secret string, err error) {
	// function name to return if any error occur
	var funcName string = "loadEnv"

	// option flagFile is DEPRECATED
	if flagFile != "" {
		flagKeyFile = flagFile
	}

	// get env
	switch {
	case flagKeyFile != "":
		// from supplied .env file
		err := godotenv.Load(flagKeyFile)
		if err != nil {
			return "", fmt.Errorf("func %q - error loading env file %s", funcName, flagKeyFile)
		}
	default:
		// from os environment
		envVar := os.Getenv(label)

		// from default .env file
		cfgEnv := os.Getenv(cfgRootDir) + cfgDir + cfgFile
		errFile := godotenv.Load(cfgEnv)

		// check result
		if errFile != nil && envVar == "" {
			return "", fmt.Errorf("func %q - error loading default env file %q or missing env var %q: %w", funcName, cfgRootDir+cfgDir+cfgFile, label, err)
		}
	}

	// load env configs
	secret, ok := os.LookupEnv(label)
	if !ok || secret == "" {
		return "", fmt.Errorf("func %q - API KEY %q not given", funcName, label)
	}
	return secret, nil
}

// createUserAgent return a custom 'User-Agent'
func createUserAgent(req *http.Request) (ua string, err error) {
	// function name to return if any error occur
	var funcName string = "createUserAgent"

	// load default Go User-Agent
	dump, err := httputil.DumpRequestOut(req, true)
	if err != nil {
		return "", fmt.Errorf("func %q - error retrieving Go default User-Agent: %w\n", funcName, err)
	}
	_, d, _ := strings.Cut(string(dump), "User-Agent: ")
	d, _, _ = strings.Cut(d, "\r\n")

	// create custom UA
	ua = fmt.Sprintf("%s (%s)", customUA, d)

	return ua, nil
}

func initiateUpload(payloadJSON []byte) (location string, err error) {
	// function name to return if any error occur
	var funcName string = "initiateUpload"

	// load env configs
	key, err := loadEnv(apikeyName)
	if err != nil {
		return "", err
	}

	mimeType := http.DetectContentType(mediaUploadData)
	numBytes := len(mediaUploadData)

	// create and configure HTTP request
	switch {
	case flagTestPayload != "" || flagTestPayloadHttpbin:
		// dump server endpoint
		key = fakeApiKey
		endpoint = endpoint + key
		if err := testEndpoint(http.MethodPost); err != nil {
			return "", err
		}
	default:
		// production endpoint
		endpoint = endpoint + key
	}

	payload := bytes.NewReader(payloadJSON)

	req, err := http.NewRequest(http.MethodPost, endpoint, payload)
	if err != nil {
		return "", err
	}

	req.Header.Set("X-Goog-Upload-Protocol", "resumable")
	req.Header.Set("X-Goog-Upload-Command", "start")
	req.Header.Set("X-Goog-Upload-Header-Content-Length", fmt.Sprintf("%d", numBytes))
	req.Header.Set("X-Goog-Upload-Header-Content-Type", mimeType)
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	// parse HTTP response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("func %q - error reading HTTP response body: %w\n", funcName, err)
	}

	// error check
	if resp.StatusCode != http.StatusCreated && resp.StatusCode != http.StatusOK {
		fmt.Println(string(body))
		return "", fmt.Errorf("func %q - unexpected status code: %d", funcName, resp.StatusCode)
	}

	// return HTTP dump
	if flagTestPayload != "" || flagTestPayloadHttpbin {
		return string(body), nil
	}

	// return file upload URL
	location = resp.Header.Get("X-Goog-Upload-URL")
	return location, nil
}

func uploadChunks(uploadUrl string, data []byte) error {
	numChunks := (len(data) + chunkSize - 1) / chunkSize

	for i := 1; i <= numChunks; i++ {
		offset := (i - 1) * chunkSize
		end := offset + chunkSize
		if end > len(data) {
			end = len(data)
		}
		chunk := data[offset:end]

		uploadCommand := "upload"
		if i == numChunks {
			uploadCommand = "upload, finalize"
		}

		err := uploadChunk(uploadUrl, chunk, offset, uploadCommand)
		if err != nil {
			return err
		}
	}

	return nil
}

func uploadChunk(uploadUrl string, chunk []byte, offset int, uploadCommand string) error {
	req, err := http.NewRequest(http.MethodPut, uploadUrl, bytes.NewReader(chunk))
	if err != nil {
		return err
	}

	req.Header.Set("Content-Length", fmt.Sprintf("%d", len(chunk)))
	req.Header.Set("X-Goog-Upload-Offset", fmt.Sprintf("%d", offset))
	req.Header.Set("X-Goog-Upload-Command", uploadCommand)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

	return nil
}

func completionTask(payloadJSON []byte) (resp string, err error) {
	for i := -1; i < retries; i++ {
		resp, err = httpRequestPOST(payloadJSON)
		if err != nil {
			switch {
			case strings.HasSuffix(err.Error(), timeoutErr) && i < (retries-1):
				// manage timeout error and retries
				fmt.Printf("connection timeout (%.0f sec): retry in %d sec. ... attempt %d of %d\n", timeout.Seconds(), retriesWait, i+2, retries)
				time.Sleep(time.Second * time.Duration(retriesWait))
				endpoint, _, _ = strings.Cut(endpoint, keyKey)
				endpoint = endpoint + keyKey
				continue
			default:
				// manage all errors, including 'last timeout retry'
				if i > -1 {
					// print an empty line after all timeout messages, if any
					fmt.Println()
				}
				return "", err
			}
		}
		// no errors, no timeout: read body, break loop, continue execution
		if i > -1 {
			// print an empty line after all timeout messages, if any
			fmt.Println()
		}
		break
	}
	return resp, nil
}

// testEndpoint compose endpoint for tests with dump server
func testEndpoint(httpMethod string) error {
	// function name to return if any error occur
	var funcName string = "testEndpoint"

	// extract URL parts
	e, err := url.Parse(endpoint)
	if err != nil {
		return fmt.Errorf("func %q - error creating test URL: %w\n", funcName, err)
	}

	// select dump server
	switch {
	case flagTestPayload != "":
		// owned dump server
		url := strings.TrimSuffix(flagTestPayload, "/")
		endpoint = url + e.Path + "?" + e.RawQuery
	case flagTestPayloadHttpbin:
		// httpbin.org
		endpoint = testUrlHttpbin + strings.ToLower(httpMethod) + "?" + e.RawQuery
	}

	return nil
}
