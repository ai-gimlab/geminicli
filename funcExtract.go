/*  'geminicli' Terminal Chat Completion client for Google's Gemini AI models
    Copyright (C) 2024 gimlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"fmt"
	"slices"
	"strconv"
	"strings"

	"github.com/gen2brain/go-fitz"
)

// --- data types --- //
type Extractor interface {
	ToText(string) (string, error)
}

type Pdf struct{}

// --- constants --- //
const (
	// supported files
	PDF string = "application/pdf"
)

// --- methods --- //
// ToText extract text from 'filename' pdf file, return extracted text.
// Pdf type satisfy Extractor interface.
func (p Pdf) ToText(filename string) (string, error) {
	// function name to return if any error occur
	var funcName string = "PdfToText"

	var b strings.Builder
	var pages []int

	// init fitz
	doc, err := fitz.New(filename)
	if err != nil {
		return "", fmt.Errorf("func %q - error inizializing pdf parser: %w", funcName, err)
	}
	defer doc.Close()

	// single pages requested
	if flagPdfPages != "" {
		// convert flagPdfPages's argument in a slice of pages number
		p, err := pdfPages(flagPdfPages)
		if err != nil {
			return "", err
		}
		// check if requested pages are valid
		for _, v := range p {
			if v > doc.NumPage() || v < 1 {
				return "", fmt.Errorf("\ainvalid PDF page number requested: %d (valid range 1 to %d)", v, doc.NumPage())
			}
			pages = append(pages, v)
		}
		// sort pages
		slices.Sort(pages)
		// extract text
		for _, n := range pages {
			text, err := doc.Text(n - 1)
			if err != nil {
				return "", err
			}
			// write extracted text to buffer
			if _, err := b.WriteString(text); err != nil {
				return "", err
			}
			// page numbering
			if flagPdfPageNumber {
				if _, err := b.WriteString(fmt.Sprintf("|| --- end PAGE %d --- ||\n\n---\n\n", n)); err != nil {
					return "", err
				}
			}
		}
		return b.String(), nil
	}

	// extract text from whole document
	for n := 0; n < doc.NumPage(); n++ {
		text, err := doc.Text(n)
		if err != nil {
			return "", err
		}
		// write extracted text to buffer
		if _, err := b.WriteString(text); err != nil {
			return "", err
		}
		// page numbering
		if flagPdfPageNumber {
			if _, err := b.WriteString(fmt.Sprintf("|| --- end PAGE %d --- ||\n\n---\n\n", n+1)); err != nil {
				return "", err
			}
		}
	}

	return b.String(), nil
}

// pdfPages check format of 'flagPdfPages' option argument - return a slice with all page numbers requested
func pdfPages(pages string) (pageNumbers []int, err error) {
	var valuesString []string = strings.Split(pages, ",")
	for _, v := range valuesString {
		if strings.Contains(v, "-") {
			var valuesRange []string = strings.Split(v, "-")
			startRange, err := strconv.Atoi(valuesRange[0])
			if err != nil {
				return nil, err
			}
			endRange, err := strconv.Atoi(valuesRange[1])
			if err != nil {
				return nil, err
			}
			for i := startRange; i <= endRange; i++ {
				pageNumbers = append(pageNumbers, i)
			}
			continue
		}
		page, err := strconv.Atoi(v)
		if err != nil {
			return nil, err
		}
		pageNumbers = append(pageNumbers, page)
	}
	return pageNumbers, nil
}
