# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

### Added

- default option to help/defaults messages

### Changed

- version from commits
- modified system message of preset *pdfmarkdown*

### Removed

- thinking message (removed from the official API)

### Fixed

- preset *presentationcode*: removed markdown formatting from code (**no stream mode only**)

## [1.4.0](https://gitlab.com/ai-gimlab/geminicli/-/releases/v1.4.0) - 2025-01-06

### Added

- Gemini 2.0 initial support
- *thinking* models support
- option `--no-thinking`: disables the display of the *thinking* message when a *thinking* model is being used
- preset *pdfmarkdown*: convert PDF document to Markdown format (text only)

### Fixed

- options `--response-raw` and `--response-json` not working as expected in stream mode
- *citations* are now excluded from *stream*, *csv* and *function* modes
- *code execution* response miss code and result

## [1.3.0](https://gitlab.com/ai-gimlab/geminicli/-/releases/v1.3.0) - 2024-11-14

### Added

- option `--model-used`: prints which model was used
- options `--logprobs` and `top-logprobs=NUMBER`: log-likelihood scores for the response tokens and top tokens
- option `--pp=NUMBER`: [presence penalty](https://ai.google.dev/api/generate-content#generationconfig)
- option `--fp=NUMBER`: [frequency penalty](https://ai.google.dev/api/generate-content#generationconfig)

### Changed

- moved stop sequences config from payload config to program config

### Fixed

- when using presets by loading/saving configuration from/to file, the options load order does not behave as expected

## [1.2.0](https://gitlab.com/ai-gimlab/geminicli/-/releases/v1.2.0) - 2024-10-23

### Added

- preset *functionJSchema*: create JSON object for function call definition from natural language PROMPT
- preset *formatJSchema*: create JSON schema for JSON formatted response from natural language PROMPT
- preset *manpage*: create command man page using command help output as PROMPT
- preset *mantranslate*: translate a given man page content (PROMPT is content of a manFile.1)
- preset *role*: find a Role for a given [system|user] PROMPT
  - useful for *act as* or *you are* LLMs role attribution
- support for web dump server, such as [httpbin.org](https://httpbin.org), or an owned one, such as [webdebug](https://gitlab.com/web281/webdebug#webdebug)
- api-key name can be customized with the new option `--env=NAME`
  - eg.: 'GOOGLE_API_KEY' or 'GOOGLE_SEARCH_API_KEY', or 'PROJECT_1269_API_KEY', etc...
  - useful to manage multiple api-keys
- `-k, --key-file=FILENAME` option (replace `-f, --file=APIKEY_FILE` option)
- `--model-used` option: which model was used at endpoint side

### Fixed

- output file formatting (options `--output-full` + `--output=FILENAME`)
- option `--output=FILENAME`: check if FILENAME directory exist was made after completion
- option `--no-console` not working as expected
- `nil pointer dereference` when response format is set to JSON and `finishReason` is *SAFETY*
- option `--cache-create` dependency from option `--media=FILENAME` not working as expected
- missing counting of cached tokens

### Deprecated

- option `-f, --file=APIKEY_FILE` (replaced by `-k, --key-file=FILENAME` option)

### Removed

- check model name: checking is done by the endpoint. This extra check only adds latency
- duplicate code for JSON output

## [1.1.0](https://gitlab.com/ai-gimlab/geminicli/-/releases/v1.1.0) - 2024-08-22

### Added

- Context caching
- Can save response text to file (option `--output=FILENAME`)
- Can save full chat text to file (options `--output-full` + `--output=FILENAME`)
- Can suppress console output (options `--no-console` +  `--output=FILENAME`)
- *save response to file* and *suppress console output* can be saved as options in *TOML* config file

### Fixed

- Option `--format=FORMAT` no more mandatory for option `--format-jschema={SCHEMA}`

## [1.0.0](https://gitlab.com/ai-gimlab/geminicli/-/releases/v1.0.0) - 2024-08-11

### First stable release

---

[Back to project main page](https://gitlab.com/ai-gimlab/geminicli#geminicli---overview)

---
